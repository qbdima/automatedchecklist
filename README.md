# **AutomatedChecklist** #

AutomatedChecklist is an application for automation testing new QB servers.
### Application consists of 3 parts: ###
1. First part respond for create new QB account on server and for create test application with test content.
2. Second part respond for testing REST API, chat, bosh and web sockets.
3. Third pard respond for UI testing admin panel using selenium web driver.

## Download ##
For download source files execute:

```
#!bash

git clone https://qbdima@bitbucket.org/qbdima/automatedchecklist.git 
```


### Requirements for software to run application ###
1. Firefox version 42.0 or higher.
2. Node.js version 5.6.0 or higher.
3. Allure command line version 1.4.22 or higher.
4. JDK 8.

For install Node.js and Allure command line execute command:

```
#!bash

bash install_soft.sh
```

### **Attention!** ###
Before first using new email address, in security settings you should allow apps to access your account. Without it program can not get mails from mailbox. [https://support.google.com/accounts/answer/6010255?hl=en](Link URL)

# **RUN** #
1. Create configurations.json file in /src/test/resources folder. Put configuration information in this file using example configurations.json.example.
2. In gradle.properties file you can set what kind of test you need to run using mode parameter. If mode=1 - will run only first part of program. If mode=2 - will run first and second part of program. mode=3 will run all parts. 
3. In command line, from directory with sources execute:

```
#!bash

bash ./gradlew clean test
```
When test are finished, results will be displayed in browser.