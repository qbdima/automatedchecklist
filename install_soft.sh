DIR="Downloads"

if ! grep -rn allure $HOME/.bash_profile
then
curl -L -o $HOME/$DIR/allure-commandline.tar.gz 'https://github.com/allure-framework/allure-core/releases/download/allure-core-1.4.22/allure-commandline.tar.gz'
mkdir -p $HOME/$DIR/allure-commandline
tar -xvf $HOME/$DIR/allure-commandline.tar.gz -C $HOME/$DIR/allure-commandline
echo "export PATH=$PATH:$HOME/$DIR/allure-commandline/bin" >> $HOME/.bash_profile
source $HOME/.bash_profile
fi

if ! which node
then
curl -L -o $HOME/$DIR/node-v5.6.0.pkg 'https://nodejs.org/dist/v5.6.0/node-v5.6.0.pkg'
open $HOME/$DIR/node-v5.6.0.pkg
fi


