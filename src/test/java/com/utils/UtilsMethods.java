package com.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileReader;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Optional;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class UtilsMethods {

    static GlobalParams params = GlobalParams.INSTANCE;
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    public static String calculateHMAC_SHA(String data, String key) throws SignatureException {
        String result = null;
        try {

            // get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);

            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);

            byte[] digest = mac.doFinal(data.getBytes());

            StringBuilder sb = new StringBuilder(digest.length * 2);
            String s;
            for (byte b : digest) {
                s = Integer.toHexString(0xFF & b);
                if (s.length() == 1) {
                    sb.append('0');
                }

                sb.append(s);
            }

            result = sb.toString();

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }

        return result;
    }


    public static HashMap<String,String> parseBlobObject(String str){
        String[] tmpStr = str.split("\\?");
        String[] parts = tmpStr[1].split("&");
        String[] tmp;
        HashMap<String,String> arr = new HashMap<String,String>();

        for(int i = 0; i < parts.length; i++){
            tmp = parts[i].split("=");
            arr.put(tmp[0],tmp[1]);
        }
        return arr;
    }

    public static String getBucketUrl(String str){
        String[] parts = str.split("\\?");

        return parts[0];
    }

    public static String createUserJID(Long userID){
        final StringBuffer userJID = new StringBuffer(20);

        class UserJID {
            public Optional<String> appID;
            public Optional<String> chatDomain;
        }

        UserJID jid = new UserJID();
        jid.appID = Optional.of((String)params.get("app_id"));
        jid.chatDomain = Optional.of((String)params.get("chat_domain"));


        jid.appID.ifPresent(appIDStr -> {
            jid.chatDomain.ifPresent(chatDomainStr -> {
                userJID.append(userID)
                        .append("-")
                        .append(appIDStr)
                        .append("@")
                        .append(chatDomainStr);
            });
        });


        return userJID.toString();
    }

    public static String createRoomJID(String dialogID){

        final StringBuffer groupChatRoom = new StringBuffer(20);

        class RoomJID {
            public Optional<String> appID;
            public Optional<String> chatDomain;
        }

        RoomJID jid = new RoomJID();
        jid.appID = Optional.of((String)params.get("app_id"));
        jid.chatDomain = Optional.of((String)params.get("chat_domain"));


        jid.appID.ifPresent(appIDStr -> {
            jid.chatDomain.ifPresent(chatDomainStr -> {
                groupChatRoom.append(appIDStr)
                        .append("_")
                        .append(dialogID)
                        .append("@muc.")
                        .append(chatDomainStr);
            });
        });

        return groupChatRoom.toString();
    }
    public static void printConfig(){
        System.out.println("\"" + (String)params.get("clientName") + "\"" + ": {" );
        System.out.println("\"" + "app_id" + "\"" + ": " + (String)params.get("app_id") + ",");
        System.out.println("\"" + "auth_key" + "\"" + ": " + "\"" + (String)params.get("auth_key") + "\"" + ",");
        System.out.println("\"" + "auth_secret" + "\"" + ": " + "\"" + (String)params.get("auth_secret") + "\"" + ",");
        System.out.println("\"" + "api_domain" + "\"" + ": " + "\"" + (String)params.get("api_domain") + "\"" + ",");
        System.out.println("\"" + "chat_domain" + "\"" + ": " + "\"" + (String)params.get("chat_domain") + "\"" + ",");
        System.out.println("\"" + "bucket_name" + "\"" + ": " + "\"" + (String)params.get("bucket_name") + "\"" + ",");
        System.out.println("\"" + "test_user_id1" + "\"" + ": " + (String)params.get("test_user_id1") + ",");
        System.out.println("\"" + "test_user_login1" + "\"" + ": " + "\"" + (String)params.get("test_user_login1") + "\"" + ",");
        System.out.println("\"" + "test_user_password1" + "\"" + ": " + "\"" + (String)params.get("test_user_password1") + "\"" + ",");
        System.out.println("\"" + "test_user_id2" + "\"" + ": " + (String)params.get("test_user_id2") + ",");
        System.out.println("\"" + "test_user_login2" + "\"" + ": " + "\"" + (String)params.get("test_user_login2") + "\"" + ",");
        System.out.println("\"" + "test_user_password2" + "\"" + ": " + "\"" + (String)params.get("test_user_password2") + "\"" + ",");
        System.out.println("\"" + "dialog_id" + "\"" + ": " + "\"" + " " + "\"" + ",");
        System.out.println("\"" + "dialog_users_ids_to_update" + "\"" + ": " + "[" + (String)params.get("test_user_id2") + "]" + ",");
        System.out.println("\"" + "dialog_operation_to_update" + "\"" + ": " + "\"" + "push" + "\"");
        System.out.println("}," );
    }

//    public static JsonObject getEmailCredentials() throws Exception {
//
//        String filePath = "./src/test/resources/configurations.json";
//        JsonObject  credentials = null;
//
//        if ((new File(filePath)).exists()) {
//            try{
//                JsonElement jelement = new JsonParser().parse(new FileReader(filePath));
//                credentials = jelement.getAsJsonObject();
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }else {
//            throw new Exception("Please create config file and put here configurations properties");
//        }
//
//        return credentials;
//    }



}
