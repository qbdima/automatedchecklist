package com.utils;

public class Settings {

    static GlobalParams params = GlobalParams.INSTANCE;

    public static void setUp() throws Exception {
        String adminLogin = ConfigParser.getQBLogin();
        String adminPassword = ConfigParser.getQBPassword();

        if (adminLogin == null || adminPassword == null || adminLogin.isEmpty() || adminPassword.isEmpty()){
            throw new Exception("Please check your qb_login and qb_password parameters. You should set both parameters " +
                    "or delete that fields from config for create default test account");
        }
    }

}
