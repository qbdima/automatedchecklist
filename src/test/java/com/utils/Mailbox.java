package com.utils;

import javax.mail.*;
import javax.mail.search.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Mailbox {

    private static Store store;
    private static Folder folder;

    public static void connect(String email, String password) {
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");

        try {
            Session session = Session.getDefaultInstance(props, null);
            store = session.getStore("imaps");
            store.connect("imap.gmail.com", email, password);
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    public static void setFolder(String folderName) {
        try {
            folder = store.getFolder(folderName);
            folder.open(Folder.READ_WRITE);
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    public static Message getMessageByDateAndSubject(String subject, boolean isSES) {
        try {
            FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DATE, -1);
            Date d = c.getTime();

            SearchTerm st = new ReceivedDateTerm(ComparisonTerm.GT, d);

            SearchTerm andTerm = new AndTerm(ft, st);

            Message messages2[] = null;

            for (int i = 0; i < 5; i++) {
                messages2 = folder.search(andTerm);
                for (Message tmp : messages2) {
                    if (tmp.getSubject().contains(subject)) {
                        return tmp;
                    }
                }

                folder.close(true);

                if(isSES){
                    return null;
                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                folder.open(Folder.READ_WRITE);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }

        return null;

    }

    public static StringBuffer readMessage(Message message) {
        BufferedReader reader;
        String line;
        StringBuffer buffer = new StringBuffer();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(message
                            .getInputStream()));
            while ((line = reader.readLine()) != null) {
                buffer.append(" " + line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }
        return buffer;
    }

    public static String getUrlByRegexp(StringBuffer message, String regexp) {

        Pattern p = Pattern
                .compile(regexp);
        Matcher m = p.matcher(message.toString());

        String registrationURL = null;

        if (m.find()) {
            registrationURL = m.group(0);

        }
        return registrationURL;
    }

    public static boolean followTheLink(String link) {
        URL url;
        boolean isFollowed = false;

        try {
            // get URL content
            url = new URL(link);
            URLConnection conn = url.openConnection();

            boolean redirect = false;

            // normally, 3xx is redirect
            int status = ((HttpURLConnection) conn).getResponseCode();
            ((HttpURLConnection) conn).setInstanceFollowRedirects(false);
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER)
                    redirect = true;
            }

            if (redirect) {

                // get redirect url from "location" header field
                String newUrl = conn.getHeaderField("Location");

                // open the new connnection again
                conn = (HttpURLConnection) new URL(newUrl).openConnection();

            }

            if(((HttpURLConnection) conn).getResponseCode() == 200){
                isFollowed = true;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isFollowed;
    }

}

