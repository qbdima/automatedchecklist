package com.utils;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.SignatureException;
import java.util.Map;

public class StatusChecker {

    static GlobalParams params = GlobalParams.INSTANCE;
    final static Logger logger = Logger.getLogger(StatusChecker.class);

    private static final String checkerURL = "https://statusdev.quickblox.com/";
    private static final String path = "admin/instance/create";


    public static WebTarget getTarget(Client client, String api, String path) {
        WebTarget target = client
                .target(api).path(path);
        return target;
    }

    public static boolean create(JSONObject sessionParams) throws JSONException, SignatureException {

        boolean isCreate = false;

        Response response = createCheckerRequest(getTarget(params.getClient(), checkerURL, path), MediaType.APPLICATION_JSON_TYPE,
                null, Entity.entity(sessionParams.toString(), MediaType.APPLICATION_JSON));
        if(response.getStatus() == 200){
            isCreate = true;
        }

        return isCreate;
    }

    private static Response createCheckerRequest(WebTarget target, MediaType type, Map<String, Object> queryParams, Entity<?> postBody){

        if (queryParams != null && queryParams.keySet().size() != 0) {
            for (String key : queryParams.keySet()) {
                target = target.queryParam(key, new Object[]{queryParams.get(key)});
            }
        }

        Invocation.Builder builder = target.request(type);

        Response tempResponse = builder.post(postBody, Response.class);

        try {
            if(logger.isInfoEnabled()){
                logger.info("Method: " + "POST" + " Request: " + target.getUriBuilder().toString());
                if(target.getUri().getQuery() != null ) {
                    logger.info("Params: " + URLDecoder.decode(target.getUri().getQuery(), "UTF-8"));
                }

                tempResponse.bufferEntity();
                String responseBody = tempResponse.readEntity(String.class);
                if(responseBody.equals(" ")){
                    logger.info("Response code: " + tempResponse.getStatus() + "\n");
                }else {
                    logger.info("Response code: " + tempResponse.getStatus() + " Response body: " + responseBody + "\n");
                }

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return tempResponse;
    }

}
