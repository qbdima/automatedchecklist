package com.utils;

import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.util.concurrent.atomic.AtomicInteger;


public class Retry implements IRetryAnalyzer {
////
////
//// private int retryCount = 0;
////    private int maxRetryCount = 5;
////
////    public boolean retry(ITestResult result) {
////
////        if (retryCount < maxRetryCount) {
////            retryCount++;
////            return true;
////        }
////        return false;
////    }
//
//    private int retryCount = 0;
//    private int maxRetryCount = 10;
//    public int ctr1 = 0;
//    public int ctr2 = 0;
//
//    @Override
//    public boolean retry(ITestResult result) {
//        if (!result.isSuccess()) {
//            if (retryCount < maxRetryCount) {
//                retryCount++;
//                result.setStatus(ITestResult.SUCCESS);
//                String message = Thread.currentThread().getName() + ": Error in " + result.getName() + " Retrying "
//                        + (maxRetryCount + 1 - retryCount) + " more times";
//                System.out.println(message);
//                Reporter.log("message");
//                return true;
//            } else {
//                result.setStatus(ITestResult.FAILURE);
//            }
//        }
//        return false;
//    }

    final static Logger logger = Logger.getLogger(Retry.class);

    private static int MAX_RETRY_COUNT = 3;

    Integer count = new Integer(MAX_RETRY_COUNT);

    public boolean isRetryAvailable() {
        return (count.intValue() > 0);
    }

    @Override
    public boolean retry(ITestResult result) {
        boolean retry = false;
        if (isRetryAvailable()) {
            System.out.println("Going to retry test case: " + result.getMethod() + ", " + (MAX_RETRY_COUNT - count.intValue() + 1) + " out of " + MAX_RETRY_COUNT);
            logger.info("Going to retry test case: " + result.getMethod() + ", " + (MAX_RETRY_COUNT - count.intValue() + 1) + " out of " + MAX_RETRY_COUNT);
            retry = true;
            --count;
        }
        return retry;
    }

}