package com.utils;

import org.apache.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;


public class Request {

    final static Logger logger = Logger.getLogger(Request.class);

    static WebTarget target;

    public static WebTarget getTarget(Client client, String api, String path) {
        target = client
                .target(api).path(path);
        return target;
    }

    public static Response getResponse(WebTarget target, MediaType type, String qbToken, RESPONSE_TYPE responseType,
                                       Map<String, Object> queryParams, Entity<?> postBody) {

        if (queryParams != null && queryParams.keySet().size() != 0) {
            for (String key : queryParams.keySet()) {
                target = target.queryParam(key, new Object[]{queryParams.get(key)});
            }
        }

        Invocation.Builder builder = target.request(type)
                .header("QB-Token", qbToken);

        Response tempResponse = null;

        switch (responseType) {
            case GET:
                tempResponse = builder.get(Response.class);
                break;
            case PUT:
                tempResponse = builder.put(postBody, Response.class);
                break;
            case POST:
                tempResponse = builder.post(postBody, Response.class);
                break;
            case DELETE:
                tempResponse = builder.delete(Response.class);
                break;
            default:
                throw new RuntimeException("You should specify type of request");

        }

        try {
            if(logger.isInfoEnabled()){
                logger.info("Method: " + responseType.toString() + " Request: " + target.getUriBuilder().toString());
                if(target.getUri().getQuery() != null ) {
                    logger.info("Params: " + URLDecoder.decode(target.getUri().getQuery(), "UTF-8"));
                }

                tempResponse.bufferEntity();
                String responseBody = tempResponse.readEntity(String.class);
                if(responseBody.equals(" ")){
                    logger.info("Response code: " + tempResponse.getStatus() + "\n");
                }else {
                    logger.info("Response code: " + tempResponse.getStatus() + " Response body: " + responseBody + "\n");
                }

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return tempResponse;

    }


}


