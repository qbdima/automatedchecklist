package com.utils;

import org.codehaus.jettison.json.JSONException;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class Test {

    public static void main(String [ ] args) throws JSONException, IOException, SAXException, ParserConfigurationException {

        final TestNG testNG = new TestNG(true);
        final Parser parser = new Parser("./src/test/resources/suite4.xml");
        final List<XmlSuite> suites = parser.parseToList();
        testNG.setXmlSuites(suites);
        testNG.run();
    }

}
