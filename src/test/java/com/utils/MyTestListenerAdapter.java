//package com.utils;
//
//import org.apache.log4j.Logger;
//import org.testng.IRetryAnalyzer;
//import org.testng.ITestResult;
//import org.testng.Reporter;
//import org.testng.TestListenerAdapter;
//import org.testng.annotations.ITestAnnotation;
//import org.testng.internal.annotations.IAnnotationTransformer;
//
//import java.lang.reflect.Constructor;
//import java.lang.reflect.Method;
//
///**
// * Created by dima_solovyov on 3/29/16.
// */
//public class MyTestListenerAdapter extends TestListenerAdapter {
//
//    final static Logger logger = Logger.getLogger(MyTestListenerAdapter.class);
//    @Override
//    public void onTestFailure(ITestResult result) {
//        logger.info("i am in onTestFailure method");
//        if (result.getMethod().getRetryAnalyzer() != null) {
//            Retry retryAnalyzer = (Retry)result.getMethod().getRetryAnalyzer();
//
//            if(retryAnalyzer.isRetryAvailable()) {
//                logger.info("SKIP");
//                result.setStatus(ITestResult.SKIP);
//            } else {
//                logger.info("FAIL");
//                result.setStatus(ITestResult.FAILURE);
//            }
//            Reporter.setCurrentTestResult(result);
//        }
//    }
//}
////public class MyTestListenerAdapter implements IAnnotationTransformer {
////    @Override
////    public void transform(ITestAnnotation annotation, Class testClass,
////                          Constructor testConstructor, Method testMethod) {
////        IRetryAnalyzer retry = annotation.getRetryAnalyzer();
////        if (retry == null) {
////            annotation.setRetryAnalyzer(Retry.class);
////        }
////    }
////}