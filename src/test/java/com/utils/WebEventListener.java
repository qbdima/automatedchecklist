package com.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class WebEventListener implements WebDriverEventListener {

    private Logger logger = Logger.getLogger(this.getClass());

    public void beforeNavigateTo(String url, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//
//        System.out.println("Before navigating to: '" + url + "'");
        logger.info("Before navigating to: '" + url + "'");
    }

    public void afterNavigateTo(String url, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//
//        System.out.println("Navigated to:'" + url + "'");
        logger.info("Navigated to:'" + url + "'");
    }

    public void beforeChangeValueOf(WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Value of the:" + element.toString()
//                + " before any changes made");
        logger.info("Value of the:" + element.toString()
                + " before any changes made");
    }

    public void afterChangeValueOf(WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Element value changed to: " + element.toString());
        logger.info("Element value changed to: " + element.toString());
    }

    public void beforeClickOn(WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Trying to click on: " + element.toString());
        logger.info("Trying to click on: " + element.toString());
    }

    public void afterClickOn(WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Clicked on: " + element.toString());
        logger.info("Clicked on: " + element.toString());
    }

    public void beforeNavigateBack(WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Navigating back to previous page");
        logger.info("Navigating back to previous page");
    }

    public void afterNavigateBack(WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Navigated back to previous page");
        logger.info("Navigated back to previous page");
    }

    public void beforeNavigateForward(WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Navigating forward to next page");
        logger.info("Navigating forward to next page");
    }

    public void afterNavigateForward(WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Navigated forward to next page");
        logger.info("Navigated forward to next page");
    }

    public void onException(Throwable error, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Exception occured: " + error);
        if (error.getClass().equals(NoSuchElementException.class)){
            logger.error("WebDriver error: Element not found ");
        } else {
            logger.error("WebDriver error:", error);
        }
    }

    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Trying to find Element By : " + by.toString());
        logger.info("Trying to find Element By : " + by.toString());
    }

    public void afterFindBy(By by, WebElement element, WebDriver driver) {
//        System.out.println("Time:" + LocalDateTime.now());
//        System.out.println("Found Element By : " + by.toString());
        logger.info("Found Element By : " + by.toString());
    }

    /*
     * non overridden methods of WebListener class
     */
    public void beforeScript(String script, WebDriver driver) {
    }

    public void afterScript(String script, WebDriver driver) {
    }

}
