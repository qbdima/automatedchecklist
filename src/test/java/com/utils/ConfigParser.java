package com.utils;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfigParser {

    public static JsonObject getCredentials() throws Exception {

        String filePath = "./src/test/resources/configurations.json";
        JsonObject  credentials = null;

        if ((new File(filePath)).exists()) {
            try{
                JsonElement jelement = new JsonParser().parse(new FileReader(filePath));
                credentials = jelement.getAsJsonObject();
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            throw new Exception("Configuration file ./src/test/resources/configurations.json not found. " +
                    "Please create it and put here configurations properties. See configurations.json.example for details");
        }

        return credentials;
    }

    public static String getAdminURL(){

        String adminURL = null;
        try {
            adminURL = getCredentials().get("admin_panel_url").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return adminURL;
    }

    public static String getRegistrationCode(){

        String registrationCode = null;
        try {
            registrationCode = getCredentials().get("registration_code").getAsString();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        return registrationCode;
    }


    public static String getQBLogin(){

        String QBLogin = null;
        try {
            if(getCredentials().has("qb_login")){
                QBLogin = getCredentials().get("qb_login").getAsString();
                if(QBLogin.length() == 0){
                    QBLogin = Consts.ADMIN_LOGIN;
                }
            }else {
                QBLogin = Consts.ADMIN_LOGIN;
            }
        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }

        return QBLogin;
    }




    public static String getQBPassword(){
//        String adminResource = getAdminResource();
//        if(adminResource != null){
//            passHash = DigestUtils.sha1Hex(pas);
//            System.out.println(passHash);
//        }
//        else throw new Exception ("pass did not generated");

        String QBPassword = null;
        try {
            if(getCredentials().has("qb_password")){
                QBPassword = getCredentials().get("qb_password").getAsString();
                if(QBPassword.length() == 0){
                    String adminResource = getAdminResource();
                    if(adminResource != null){
                        QBPassword = DigestUtils.sha1Hex(adminResource);
                        System.out.println(QBPassword);
                    }
                }
            }else {
                String adminResource = getAdminResource();
                if(adminResource != null){
                    QBPassword = DigestUtils.sha1Hex(adminResource);
                    System.out.println(QBPassword);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }

        return QBPassword;

    }

    private static String getAdminResource(){
        String adminResource = null;

        Pattern z = Pattern.compile("(?<=https?:\\/\\/).*[^\\/]");
        Matcher x = z.matcher(Consts.ADMIN_PANEL);

        if (x.find()) {
            adminResource = x.group(0);
        }
        return adminResource;

    }
}
