package com.utils;

public enum RESPONSE_TYPE {
    GET, PUT, POST, DELETE;
}
