package com.utils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.HashMap;
import java.util.Map;

public enum GlobalParams {

    INSTANCE;

    private Map<String, Object> params;
    private Client client = ClientBuilder.newClient();

    private GlobalParams() {
        params = new HashMap<String, Object>();
    }

    public Object get(String key){
        return params.get(key);
    }

    public void put(String key, Object value){
        params.put(key,value);
    }

    public Client getClient() {
        return client;
    }
}
