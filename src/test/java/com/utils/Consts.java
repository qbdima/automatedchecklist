package com.utils;

public class Consts {

    //PROD mode (run with gradle)
    //public static final String REGISTRATION_CODE = System.getProperty("REGISTRATION_CODE", "UNSET (MAIN)");
    //public static final String ADMIN_PANEL = System.getProperty("ADMIN_PANEL", "UNSET (MAIN)");

    public static final String ADMIN_PANEL = ConfigParser.getAdminURL();
    public static final String REGISTRATION_CODE = ConfigParser.getRegistrationCode();
    public static final String ADMIN_LOGIN = "test"; //quickbloxdima
    public static final String ADMIN_PASSWORD = "11111111";

    public static final String USER1_LOGIN ="test1";
    public static final String USER1_PASSWORD = "11111111";
    public static final String USER1_EMAIL = "test1@gmail.com";

    public static final String USER2_LOGIN ="test2";
    public static final String USER2_PASSWORD = "11111111";
    public static final String USER2_EMAIL = "katia.denisenkoasd@injoit.com";

    public static final String USER_LOGIN ="test-user111";
    public static final String USER_PASSWORD = "11111111";

    public static final String APP_NAME = "test";
    public static final String APP_TYPE = "Fun";

    public static final String STATUSCHECKER_APP = "StatusChecker";
    public static final String STATUSCHECKER_CLASS_NAME = "StatusChecker";
    public static final String STATUSCHECKER_FIELD = "StatusChecker";

    public static final String NEW_APP_NAME = "autotest1";


    public static final String GSM_API_KEY = "AIzaSyAH4OtVXPG1-R8bGujx1lES_7n3v7-yNXg";

    public static final String CLASS_NAME = "Movie";
    public static final String FIELD_NAME1 = "name";
    public static final String FIELD_NAME2 = "description";
    public static final String FIELD_NAME3 = "rating";
    public static final String FIELD_TYPE1 = "String";
    public static final String FIELD_TYPE2 = "String";
    public static final String FIELD_TYPE3 = "Integer";

    public static final String IMAGE_PATH = "./src/test/resources/dog.jpeg";
    public static final String JSON_FILE_PATH = "./src/test/resources/json_file_for_import.json";
    public static final String CERT_FILE_PATH = "./src/test/resources/Certificates_SuperSample_APNS_dev2.p12";

}

