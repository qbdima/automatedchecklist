package com.boshAndWebSocketTests.suite;

import com.boshAndWebSocketTests.modules.BoshModule;
import com.boshAndWebSocketTests.modules.WebSocketModule;
import com.neovisionaries.ws.client.WebSocket;
import com.utils.GlobalParams;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;


public class BoshAndWebSocketSuite {

    final static Logger logger = Logger.getLogger(BoshAndWebSocketSuite.class);
    static GlobalParams params = GlobalParams.INSTANCE;

    final static String password = "11111111";

    static long userID;
    static String chat;

    @AfterGroups(groups = { "PushNotificationTest" })
    public static void setUp(){
        chat = (String)params.get("chat_domain");
        userID = (Long)params.get("userID");
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnGroups = { "PushNotificationTest" }, timeOut = 10000, alwaysRun = true)
    public void checkNotSecureWebSocket() throws JSONException {

        logger.info("Connect to server: " + "ws://" + chat + ":5290");
        WebSocket ws = WebSocketModule.connect(chat, false);
        boolean isConnected;
        if(ws.getState().toString().equals("OPEN")) {
            isConnected = true;
            logger.info("Connection status : connected" + "\n");
        }else {
            isConnected = false;
            logger.info("Connection status : not connected" + "\n");
        }
        Assert.assertTrue(isConnected);
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkNotSecureWebSocket"}, alwaysRun = true, timeOut = 10000)
    public void checkSecureWebSocket() throws JSONException {

        logger.info("Connect to server: " + "wss://" + chat + ":5291");
        WebSocket ws = WebSocketModule.connect(chat, true);
        boolean isConnected;
        if(ws.getState().toString().equals("OPEN")) {
            isConnected = true;
            logger.info("Connection status : connected" + "\n");
        }else {
            isConnected = false;
            logger.info("Connection status : not connected" + "\n");
        }

        Assert.assertTrue(isConnected);
    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkSecureWebSocket"}, alwaysRun = true, timeOut = 20000)
    public void checkNotSecureBosh() throws JSONException {

        logger.info("Connect to server: " + "http://" + chat + ":5280");
        boolean isOk = BoshModule.getConnectionStatus(userID, password, chat, false);

        Assert.assertTrue(isOk);

        if(isOk){
            logger.info("Connection status : connected" + "\n");
        }else {
            logger.info("Connection status : not connected" + "\n");
        }

    }

    @Test(groups = { "BoshAndWebSocketTest" }, dependsOnMethods = {"checkNotSecureBosh"}, alwaysRun = true, timeOut = 20000)
    public void checkSecureBosh() throws JSONException {

        logger.info("Connect to server: " + "https://" + chat + ":5281");
        boolean isOk = BoshModule.getConnectionStatus(userID, password, chat, true);

        Assert.assertTrue(isOk);

        if(isOk){
            logger.info("Connection status : connected" + "\n");
        }else {
            logger.info("Connection status : not connected" + "\n");
        }
    }

}
