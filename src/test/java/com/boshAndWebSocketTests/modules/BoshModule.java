package com.boshAndWebSocketTests.modules;

import com.utils.UtilsMethods;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.IOException;

public class BoshModule {

    public static boolean getConnectionStatus(Long userID, String password, String chatUrl, boolean isSecure) {

        String url;

        if (isSecure) {
            url = "https://" + chatUrl + ":5281";
        } else {
            url = "http://" + chatUrl + ":5280";
        }

        String[] args = {UtilsMethods.createUserJID(userID), password, url};

        String result = null;

        try {
            result = (String) new GroovyShell().parse(new File("./src/test/java/com/boshAndWebSocketTests/modules/BoshGroovyScript.groovy")).invokeMethod("check_bosh", args);
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean isOk = false;

        if (result.equals("SUCCESS")) {
            isOk = true;
        }

        return isOk;
    }

}
