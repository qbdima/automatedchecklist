package com.boshAndWebSocketTests.modules;

import com.neovisionaries.ws.client.*;
import org.apache.log4j.Logger;

public class WebSocketModule {

    final static Logger logger = Logger.getLogger(WebSocketModule.class);

    private static final int TIMEOUT = 5000;

    public static WebSocket connect(String chatUrl, boolean isSecure) {
        String server;

        if (isSecure) {
            server = "wss://" + chatUrl + ":5291";
        } else {
            server = "ws://" + chatUrl + ":5290";
        }

     //   logger.info("Connect to server " + server);

        WebSocketFactory factory = new WebSocketFactory();

        WebSocket webSocket;
        try {
            webSocket = factory
                    .setConnectionTimeout(TIMEOUT)
                    .createSocket(server)
                    .addProtocol("xmpp")
                    .addListener(new WebSocketAdapter() {
                        // A text message arrived from the server.
                        public void onTextMessage(WebSocket websocket, String message) {
                            System.out.println(message);
                        }
                    })
                    .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE);


            webSocket = webSocket.connect();
        } catch (Exception e) {
            logger.info("connection status : not connected");
            throw new RuntimeException("Not connected!!!", e);
        }

        return webSocket;
    }
}
