package com.boshAndWebSocketTests.modules


def check_bosh(String[] args) {

    String userJID = args[0];
    String password = args[1];
    String boshURL = args[2];

    String procCommand = 'node ./src/test/resources/index.js --userJID ' +  userJID + ' --password ' +  password + ' --boshURL ' + boshURL;

    def proc1 = procCommand.execute();
    proc1.waitFor()

    def result;
    if (proc1.exitValue() == 0) {
        result = proc1.in.text;
    } else {
        println "return code: ${proc1.exitValue()}"
        println "stderr: ${proc1.err.text}"
        println "stderr: ${proc1.out.text}"
        println "stdout: ${proc1.in.text}" // *out* from the external program is *in* for groovy

        System.exit(exitValue)
    }
//    println result + "groovy";
//    if(result.trim().equals("SUCCESS")){
//        println "OK"
//    }else {
//        println "ERROR"
//    }
    return result.trim();
}