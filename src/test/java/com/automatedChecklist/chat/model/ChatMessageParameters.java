package com.automatedChecklist.chat.model;

import java.util.List;
import java.util.Map;

public class ChatMessageParameters {

    private int limit;
    private int skip;
    private List<ChatMessage> items;

    public int getLimit() {
        return limit;
    }

    public int getSkip() {
        return skip;
    }

    public List<ChatMessage> getItems() {
        return items;
    }

    public void setItems(List<ChatMessage> items) {
        this.items = items;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public static class ChatMessage {

        private String _id;
        private Object attachments;
        private String chat_dialog_id;
        private String created_at;
        private String date_sent;
        private long[] delivered_ids;
        private String message;
        private long[] read_ids;
        private long recipient_id;
        private long sender_id;
        private String updated_at;
        private int read;
        private Map<String,String> customParameters;
        private int send_to_chat;


        public class Attach
        {
            private long id;
            private String type;
            private String url;

            public long getId() {
                return id;
            }

            public String getType() {
                return type;
            }

            public String getUrl() {
                return url;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public void setId(long id) {
                this.id = id;
            }
        }


        public String getCreated_at() {
            return created_at;
        }

        public int getRead() {
            return read;
        }

        public long getRecipient_id() {
            return recipient_id;
        }

        public long getSender_id() {
            return sender_id;
        }

        public Object getAttachments() {
            return attachments;
        }

        public long[] getDelivered_ids() {
            return delivered_ids;
        }

        public long[] getRead_ids() {
            return read_ids;
        }

        public String get_id() {
            return _id;
        }

        public String getChat_dialog_id() {
            return chat_dialog_id;
        }

        public String getDate_sent() {
            return date_sent;
        }

        public String getMessage() {
            return message;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public Map<String, String> getCustomParameters() {
            return customParameters;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public void setAttachments(List<Attach> attachments) {
            this.attachments = attachments;
        }

        public void setAttachments(Map<String, Attach> attachments) {
            this.attachments = attachments;
        }

        public void setChat_dialog_id(String chat_dialog_id) {
            this.chat_dialog_id = chat_dialog_id;
        }

        public void setDate_sent(String date_sent) {
            this.date_sent = date_sent;
        }

        public void setDelivered_ids(long[] delivered_ids) {
            this.delivered_ids = delivered_ids;
        }

        public void setRead(int read) {
            this.read = read;
        }

        public void setRead_ids(long[] read_ids) {
            this.read_ids = read_ids;
        }

        public void setRecipient_id(long recipient_id) {
            this.recipient_id = recipient_id;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void setSender_id(long sender_id) {
            this.sender_id = sender_id;
        }

        public void setCustomParameters(Map<String, String> customParameters) {
            this.customParameters = customParameters;
        }

        public void setSend_to_chat(int send_to_chat) {
            this.send_to_chat = send_to_chat;
        }

        public int getSend_to_chat() {
            return send_to_chat;
        }
    }
}
