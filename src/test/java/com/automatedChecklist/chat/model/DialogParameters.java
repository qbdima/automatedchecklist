package com.automatedChecklist.chat.model;

import java.util.List;

public class DialogParameters {

    private int limit;
    private int skip;
    private int total_entries;
    private List<Dialog> items;


    public int getLimit() {
        return limit;
    }

    public int getSkip() {
        return skip;
    }

    public int getTotal_entries() {
        return total_entries;
    }

    public List<Dialog> getItems() {
        return items;
    }

    public void setTotal_entries(int total_entries) {
        this.total_entries = total_entries;
    }

    public void setItems(List<Dialog> items) {
        this.items = items;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }


    public static class Dialog {

        private String _id;
        private String xmpp_room_jid;
        private String name;
        private String photo;
        private String created_at;
        private int type;
        private String last_message;
        private String last_message_date_sent;
        private long[] occupants_ids;
        private String user_id;
        private String data;
        private int unread_messages_count;
        private int last_message_user_id;


        public String get_id() {
            return _id;
        }

        public String getPhoto() {
            return photo;
        }

        public String getUser_id() {
            return user_id;
        }

        public int getLast_message_user_id() {
            return last_message_user_id;
        }

        public int getType() {
            return type;
        }

        public int getUnread_messages_count() {
            return unread_messages_count;
        }

        public String getData() {
            return data;
        }

        public String getLast_message() {
            return last_message;
        }

        public String getLast_message_date_sent() {
            return last_message_date_sent;
        }

        public String getName() {
            return name;
        }

        public long[] getOccupants_ids() {
            return occupants_ids;
        }

        public String getXmpp_room_jid() {
            return xmpp_room_jid;
        }

        public String getCreated_at() {
            return created_at;
        }



        public void setType(int type) {
            this.type = type;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public void setData(String data) {
            this.data = data;
        }

        public void setLast_message(String last_message) {
            this.last_message = last_message;
        }

        public void setLast_message_date_sent(String last_message_date_sent) {
            this.last_message_date_sent = last_message_date_sent;
        }

        public void setLast_message_user_id(int last_message_user_id) {
            this.last_message_user_id = last_message_user_id;
        }

        public void setOccupants_ids(long[] occupants_ids) {
            this.occupants_ids = occupants_ids;
        }

        public void setUnread_messages_count(int unread_messages_count) {
            this.unread_messages_count = unread_messages_count;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public void setXmpp_room_jid(String xmpp_room_jid) {
            this.xmpp_room_jid = xmpp_room_jid;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
