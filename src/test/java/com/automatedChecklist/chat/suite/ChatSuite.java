package com.automatedChecklist.chat.suite;

import com.google.gson.Gson;
import com.automatedChecklist.chat.chatmodule.ChatApi;
import com.automatedChecklist.chat.chatmodule.ChatXMPP;
import com.automatedChecklist.chat.chatmodule.ChatXMPP.BooleanCondition;
import com.automatedChecklist.chat.model.ChatMessageParameters;
import com.automatedChecklist.chat.model.DialogParameters;
import com.automatedChecklist.chat.providers.GroupChatRoom;
import com.automatedChecklist.chat.providers.NewDialogData;
import com.automatedChecklist.chat.providers.NewRestApiChatMessageData;
import com.utils.GlobalParams;
import com.utils.UtilsMethods;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.RoomInfo;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ChatSuite {

    private static final String password = "11111111";
    private static final int requestTimeout = 30000;

    private static final String groupChatMessage = "group chat message"+ "\ue415";
    private static final String privateChatMessage = "private chat message"+ "\uD83D\uDE01\uD83D\uDE1A\uD83D\uDE17\uD83D\uDE14";

    final static Logger logger = Logger.getLogger(ChatSuite.class);

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;
    static String appID;

    static long userID;
    static long userID2;

    private static BooleanCondition condition = new BooleanCondition();
    private static BooleanCondition condition2 = new BooleanCondition();

    private static XMPPTCPConnection connection;
    private static XMPPTCPConnection connection2;

    private int count = 0;

    @AfterGroups(groups = { "UserTest" })
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
        appID = (String)params.get("app_id");
        userID = (Long)params.get("userID");
        userID2 = (Long)params.get("userID2");
    }

    @Test(groups = { "ChatTest" }, dependsOnGroups = { "UserTest" }, dataProvider = "newDialogData", dataProviderClass = NewDialogData.class, timeOut = requestTimeout, alwaysRun = true)
    public void createNewDialog(DialogParameters.Dialog dialog) throws JSONException {

        Response response = ChatApi.createNewDialog(params.getClient(), api, token, dialog);

        DialogParameters.Dialog dialog1 = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);
        params.put("dialogID", dialog1.get_id());

        long[] tmp = dialog1.getOccupants_ids();

        Arrays.sort(tmp);

        Assert.assertEquals(tmp.length, 2);
        Assert.assertEquals(tmp[0], userID);
        Assert.assertEquals(tmp[1], userID2);

        Assert.assertEquals(response.getStatus(), 201);

    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"pullOccupants"}, dataProvider = "newDialogData", dataProviderClass = NewDialogData.class,
            timeOut = requestTimeout, alwaysRun = true)
    public void pushOccupants(DialogParameters.Dialog dialog) throws JSONException {

        Response response = ChatApi.pushOccupants(params.getClient(), api, token, (String)params.get("dialogID"), userID2);

        DialogParameters.Dialog dialog1 = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);

        long[] tmp = dialog.getOccupants_ids();
        long[] tmp1 = dialog1.getOccupants_ids();

        tmp[0] = userID;

        Arrays.sort(tmp);
        Arrays.sort(tmp1);

        boolean isEquals = false;

        if(Arrays.equals(tmp,tmp1)){
            isEquals = true;
        }

       Assert.assertTrue(isEquals);

    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"createNewDialog"}, timeOut = requestTimeout,alwaysRun = true)
    public void pullOccupants() throws JSONException {

        Response response = ChatApi.pullOccupants(params.getClient(), api, token,  (String)params.get("dialogID"), userID2);

        DialogParameters.Dialog dialog = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);

        long[] tmp = dialog.getOccupants_ids();

        Assert.assertEquals(tmp.length, 1);
        Assert.assertEquals(tmp[0], userID);

    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"pushOccupants"}, dataProvider = "newRestApiChatMessageData",
            dataProviderClass = NewRestApiChatMessageData.class, timeOut = requestTimeout, alwaysRun = true)
    public void createMessageRestAPI(ChatMessageParameters.ChatMessage chatMessage) throws JSONException {

        Response response = ChatApi.createMessageRestAPI(params.getClient(), api, token, chatMessage);

        ChatMessageParameters.ChatMessage message = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.ChatMessage.class);
        params.put("messageID", message.get_id());
      //  params.put("messageID", message.get_id()+"test");

        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
        Assert.assertEquals(message.getMessage(), chatMessage.getMessage());
        Assert.assertEquals(message.getSender_id(), userID);

    }

//    @Test(groups = { "ChatTest" }, dependsOnMethods = {"createMessageRestAPI"}, retryAnalyzer = Retry.class, timeOut = requestTimeout,alwaysRun = true)
    @Test(groups = { "ChatTest" }, dependsOnMethods = {"createMessageRestAPI"}, timeOut = requestTimeout, alwaysRun = true)
    public void getMessageByID() throws JSONException, InterruptedException {

        Response response;
        ChatMessageParameters chatMessageParameters;
        List<ChatMessageParameters.ChatMessage> messages;
        ChatMessageParameters.ChatMessage message = null;

        while (count < 5) {
            response = ChatApi.getMessageByID(params.getClient(), api, token, (String)params.get("dialogID"), (String)params.get("messageID"));
            chatMessageParameters = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.class);
            messages = chatMessageParameters.getItems();
            if(messages.size() == 0){
                Thread.sleep(1000);
                count++;
            }else {
                message = messages.get(0);
                break;
            }
        }
        count = 0;

        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
        Assert.assertEquals(message.getSender_id(), userID);

//        Response response = ChatApi.getMessageByID(params.getClient(), api, token, (String)params.get("dialogID"), (String)params.get("messageID"));
//
//        ChatMessageParameters chatMessageParameters = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.class);
//        List<ChatMessageParameters.ChatMessage> messages = chatMessageParameters.getItems();
//
//        ChatMessageParameters.ChatMessage message = messages.get(0);
//        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
//        Assert.assertEquals(message.getSender_id(), userID);

    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"getMessageByID"}, timeOut = requestTimeout, alwaysRun = true)
    public void loginUsers() throws XMPPException, IOException, SmackException {

        connection = ChatXMPP.createConnection((String)params.get("chat_domain"));
        connection2 = ChatXMPP.createConnection((String)params.get("chat_domain"));

        PacketListener myListener = new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message) {
                    Message message = (Message) packet;
                    // обработка входящего сообщения
                    //  processMessage(message);
                    logger.info("User1 received message: " + message.getBody() + "\n");
                }
            }
        };

        PacketListener myListener2 = new PacketListener()
        {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message)
                {
                    Message message = (Message) packet;

                    if(message.getType() == Message.Type.chat) {
                        condition.isReceived = true;
                        logger.info("User2 received private message: " + message.getBody() + "\n");
                        if(message.getBody().equals(privateChatMessage)){
                            condition.isEqual = true;
                        }
                    }
                    if(message.getType() == Message.Type.groupchat) {
                        condition2.isReceived = true;
                        logger.info("User2 received group message: " + message.getBody() + "\n");
                    }
                }
            }
        };

        String user1Login = userID + "-" +  appID;
        ChatXMPP.loginToChat(connection, user1Login, password, myListener);

        String user2Login = userID2 + "-" +  appID;
        ChatXMPP.loginToChat(connection2, user2Login, password, myListener2);

        Assert.assertTrue(connection.isConnected());
        Assert.assertTrue(connection2.isConnected());

        Assert.assertTrue(connection.isAuthenticated());
        Assert.assertTrue(connection2.isAuthenticated());
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"loginUsers"}, timeOut = requestTimeout)
    public  void sendPrivateMessageTest() throws SmackException.NotConnectedException {
        ChatXMPP.sendPrivateMessage(connection, UtilsMethods.createUserJID(userID2), privateChatMessage);
    }

    @Test(groups = { "ChatTest" }, timeOut = requestTimeout, dependsOnMethods = {"sendPrivateMessageTest"})
    public  void receivePrivateMessageTest() throws SmackException.NotConnectedException, InterruptedException {
        while (!condition.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition.isReceived);
        Assert.assertTrue(condition.isEqual);
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "receivePrivateMessageTest", timeOut = requestTimeout)
    public  void user2Logout() throws SmackException.NotConnectedException {
        connection2.disconnect(new Presence(Presence.Type.unavailable));
        Assert.assertTrue(!connection2.isConnected());
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "user2Logout", timeOut = requestTimeout, alwaysRun = true)
    public  void sendMessageToOfflineUser() throws SmackException.NotConnectedException {
        condition.isReceived = false;
        condition.isEqual = false;
        ChatXMPP.sendPrivateMessage(connection, UtilsMethods.createUserJID(userID2), privateChatMessage);
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "sendMessageToOfflineUser", timeOut = requestTimeout)
    public  void receiveOfflineMessage() throws SmackException, InterruptedException, IOException, XMPPException {
        connection2.connect();              //логинится не надо
        while (!condition.isReceived && count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        if (!condition.isReceived) {
            logger.error("Offline chat message not received");
        }

        Assert.assertTrue(condition.isReceived);
        Assert.assertTrue(condition.isEqual);
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "receiveOfflineMessage", timeOut = requestTimeout, alwaysRun = true)
    public  void joinRoom() throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        String roomJID = UtilsMethods.createRoomJID((String) params.get("dialogID"));
        RoomInfo info = MultiUserChat.getRoomInfo(connection, roomJID);
        Assert.assertTrue(info.isPersistent());

        MultiUserChat groupChat = ChatXMPP.joinRoom(connection, roomJID, UtilsMethods.createUserJID(userID));
        Assert.assertTrue(groupChat.isJoined());

        MultiUserChat groupChat2 = ChatXMPP.joinRoom(connection2, roomJID, UtilsMethods.createUserJID(userID2));
        Assert.assertTrue(groupChat2.isJoined());
    }


    @Test(groups = { "ChatTest" }, dependsOnMethods = "joinRoom", dataProvider = "groupChatRoomData", dataProviderClass = GroupChatRoom.class)
    public  void sendGroupChatMessage(String roomJID) throws SmackException.NotConnectedException, InterruptedException {
        condition2.isReceived = false;
        ChatXMPP.sendMessageGroup(connection, roomJID, groupChatMessage);

        while (!condition2.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition2.isReceived);

        if(!condition2.isReceived){
            logger.error("Group chat message not received");
        }

        condition2.isReceived = false;
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = {"sendGroupChatMessage"}, dataProvider = "newRestApiChatMessageData",
            dataProviderClass = NewRestApiChatMessageData.class, timeOut = requestTimeout)
    public void createMessageRestAPI2(ChatMessageParameters.ChatMessage chatMessage) throws JSONException, InterruptedException {

        Response response = ChatApi.createMessageRestAPI(params.getClient(), api, token, chatMessage);

        ChatMessageParameters.ChatMessage message = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.ChatMessage.class);

        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
        Assert.assertEquals(message.getMessage(), chatMessage.getMessage());
        Assert.assertEquals(message.getSender_id(), userID);

        while (!condition2.isReceived & count < 15) {
            Thread.sleep(1000);
            count++;
        }
        count = 0;
        Assert.assertTrue(condition2.isReceived);

        if(!condition2.isReceived){
            logger.error("Message from REST API not received");
        }

    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "createMessageRestAPI2", timeOut = requestTimeout, alwaysRun = true)
    public  void user1Logout() throws SmackException.NotConnectedException {
        connection.disconnect(new Presence(Presence.Type.unavailable));
        Assert.assertTrue(!connection.isConnected());
    }

    @Test(groups = { "ChatTest" }, dependsOnMethods = "user1Logout", timeOut = requestTimeout)
    public  void user1LoginToChatUsingToken() throws SmackException.NotConnectedException {
        connection = ChatXMPP.createConnection((String)params.get("chat_domain"));
        String user1Login = userID + "-" +  appID;

        PacketListener myListener = new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (packet instanceof Message) {
                    Message message = (Message) packet;
                    logger.info("User1 received message: " + message.getBody() + "\n");
                }
            }
        };

        try {
            ChatXMPP.loginToChat(connection, user1Login, token, myListener);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        } catch (SmackException e) {
            e.printStackTrace();
        }
        Assert.assertTrue(connection.isAuthenticated(), "User can not login to chat using token");
    }

}
