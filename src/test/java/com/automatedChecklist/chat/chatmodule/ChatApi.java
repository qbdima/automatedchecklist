package com.automatedChecklist.chat.chatmodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import com.google.gson.Gson;
import com.automatedChecklist.chat.model.ChatMessageParameters;
import com.automatedChecklist.chat.model.DialogParameters;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

public class ChatApi {

    public static Response createNewDialog(Client client, String api,
                                           String token, DialogParameters.Dialog dialog) throws JSONException {

        String path = "chat/Dialog.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new Gson().toJson(dialog), MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response pushOccupants(Client client, String api,
                              String token, String dialogID, long userID) throws JSONException {

        String path = "chat/Dialog/" + dialogID + ".json";

        Map map = new HashMap<String,Object>();
        map.put("push_all[occupants_ids][]", userID);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.PUT, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;

    }

    public static Response pullOccupants(Client client, String api,
                              String token, String dialogID, long userID) throws JSONException {

        String path = "chat/Dialog/" + dialogID + ".json";

        Map map = new HashMap<String,Object>();
        map.put("pull_all[occupants_ids][]", userID);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.PUT, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response deleteDialog(Client client, String api,
                                         String token, String dialogID) throws JSONException {

        String path = "chat/Dialog/" + dialogID + ".json";

        Map map = new HashMap<String,Integer>();
        map.put("force", 1);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.DELETE, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response createMessageRestAPI(Client client, String api,
                                     String token, ChatMessageParameters.ChatMessage chatMessage) throws JSONException {

        String path = "chat/Message.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new Gson().toJson(chatMessage), MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response getMessageByID(Client client, String api,
                                          String token, String dialogID, String messageID) throws JSONException {

        String path = "chat/Message.json";

        Map map = new HashMap<String,Object>();
        map.put("chat_dialog_id", dialogID);
        map.put("_id", messageID);

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.GET, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

}
