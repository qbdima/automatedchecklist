package com.automatedChecklist.chat.chatmodule;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.io.IOException;


public class ChatXMPP {

    private final static String ELEMENT_NAME = "extraParams";
    private final static String NAMESPACE = "jabber:client";
    private final static PacketFilter filter = new AndFilter(new PacketTypeFilter(Message.class));

    private final static int port = 5222;

    public static XMPPTCPConnection createConnection(String chat){

        ConnectionConfiguration configuration = new ConnectionConfiguration(chat, port);
        configuration.setReconnectionAllowed(false);
        configuration.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        XMPPTCPConnection connection = new XMPPTCPConnection(configuration);
        return connection;
    }

    public static void loginToChat(XMPPTCPConnection connection, String login, String password, PacketListener myListener)
            throws IOException, XMPPException, SmackException {

        connection.addPacketListener(myListener, filter);
        connection.connect();
        connection.login(login, password);
        connection.sendPacket(new Presence(Presence.Type.available));
    }

    public static MultiUserChat joinRoom(XMPPTCPConnection connection, String roomJID, String login) throws SmackException.NotConnectedException, XMPPException.XMPPErrorException, SmackException.NoResponseException {
        MultiUserChat chat = new MultiUserChat(connection, roomJID);
        DiscussionHistory history = new DiscussionHistory();
        history.setMaxStanzas(0);
        chat.join(login, "12344", history, connection.getPacketReplyTimeout());
        return chat;
    }

    public static void sendPrivateMessage(XMPPTCPConnection con, String to, String textMessage) throws SmackException.NotConnectedException {

        Message message = new Message(textMessage);
        message.setTo(to);
        message.setType(Message.Type.chat);
        message.setBody(textMessage);

        QBChatMessageExtension ex = new QBChatMessageExtension();

        message.addExtension(ex);

        con.sendPacket(message);
    }

    public static void sendMessageGroup(XMPPTCPConnection con, String to, String textMessage) throws SmackException.NotConnectedException {
        if(!textMessage.equals(""))
        {
            Message message = new Message();
            message.setTo(to);
            message.setType(Message.Type.groupchat);
            message.setBody(textMessage);

            QBChatMessageExtension ex = new QBChatMessageExtension();

            message.addExtension(ex);

            con.sendPacket(message);
        }
    }

    public static class BooleanCondition {

        public boolean isReceived = false;
        public boolean isEqual = false;

    }

    public static class QBChatMessageExtension implements PacketExtension {
        @Override
        public String getElementName() {
            return null;
        }

        @Override
        public String getNamespace() {
            return null;
        }

        @Override
        public CharSequence toXML() {
            XmlStringBuilder buf = new XmlStringBuilder();
            buf.halfOpenElement(ELEMENT_NAME).xmlnsAttribute(NAMESPACE).rightAngelBracket();
            buf.element("save_to_history", "1");
            buf.closeElement(ELEMENT_NAME);
            return buf;
        }
    }

}
