package com.automatedChecklist.chat.providers;

import com.utils.GlobalParams;
import org.testng.annotations.DataProvider;


public class GroupChatRoom {

    static GlobalParams params = GlobalParams.INSTANCE;

    @DataProvider(name = "groupChatRoomData")
    public static Object[][] groupChatRoomData() {

        String groupChatRoom = params.get("app_id") + "_" + params.get("dialogID") + "@muc." + params.get("chat_domain");

        return new Object[][]{{groupChatRoom}};
    }

}
