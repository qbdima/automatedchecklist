package com.automatedChecklist.chat.providers;

import com.automatedChecklist.chat.model.DialogParameters;
import com.utils.GlobalParams;
import org.testng.annotations.DataProvider;


public class NewDialogData {

    static GlobalParams params = GlobalParams.INSTANCE;

    @DataProvider(name = "newDialogData")
    public static Object[][] newDialogData() {

        long userID2 = (long)params.get("userID2");

        DialogParameters.Dialog dialog = new DialogParameters.Dialog();
        long[] occupants = {0, userID2};

        dialog.setOccupants_ids(occupants);
        dialog.setName("Rest chat");
        dialog.setType(2);

        return new Object[][]{{dialog}};
    }

}
