package com.automatedChecklist.chat.providers;

import com.automatedChecklist.chat.model.ChatMessageParameters;
import com.utils.GlobalParams;
import org.testng.annotations.DataProvider;

import java.util.HashMap;
import java.util.Map;


public class NewRestApiChatMessageData {

    static GlobalParams params = GlobalParams.INSTANCE;

    @DataProvider(name = "newRestApiChatMessageData")
    public static Object[][] newRestApiChatMessageData() {

        String dialogID = (String)params.get("dialogID");

        if(dialogID == null){
            return null;
        }

        ChatMessageParameters.ChatMessage chatMessage = new ChatMessageParameters.ChatMessage();
        chatMessage.setChat_dialog_id(dialogID);
        chatMessage.setMessage("Rest API message");
        chatMessage.setSend_to_chat(1);

        Map<String, String> params = new HashMap<>();
//        params.put("send_to_chat", "1");
        params.put("my_data", "1");

        chatMessage.setCustomParameters(params);

//        ChatMessageParameters.ChatMessage.Attach attach1 = chatMessage.new Attach();
//        attach1.setId(47863);
//        attach1.setType("image");
//        ChatMessageParameters.ChatMessage.Attach attach2 = chatMessage.new Attach();
//        attach2.setId(1);
//        attach2.setType("image");
//        attach2.setUrl("www");
//
//        Map<String, ChatMessageParameters.ChatMessage.Attach> mess = new HashMap<>();
//        mess.put("0", attach1);
//        mess.put("1", attach2);
//
//
//        chatMessage.setAttachments(mess);

        return new Object[][]{{chatMessage}};
    }
}
