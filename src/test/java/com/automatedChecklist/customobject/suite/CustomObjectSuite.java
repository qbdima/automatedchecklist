package com.automatedChecklist.customobject.suite;

import com.google.gson.Gson;
import com.automatedChecklist.customobject.customobjectmodule.CustomObjectApi;
import com.automatedChecklist.customobject.model.CustomObjectParameters;
import com.automatedChecklist.customobject.providers.NewCustomObjectData;
import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.util.List;


public class CustomObjectSuite {

    private static final String className = "Movie";

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;

    @AfterGroups(groups = { "ContentTest" })
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
    }

    @Test(groups = { "CustomObjectTest" }, dependsOnGroups = { "ContentTest" }, dataProvider = "newCustomObjectData", dataProviderClass = NewCustomObjectData.class,
            timeOut = 10000, alwaysRun = true)
    public void createCO(CustomObjectParameters.CustomObject customObject) throws JSONException {

        Response response = CustomObjectApi.createCO(params.getClient(), api, token, className, customObject);

        CustomObjectParameters.CustomObject customObject1 = new Gson().fromJson(response.readEntity(String.class), CustomObjectParameters.CustomObject.class);
        params.put("customObjectID", customObject1.get_id());

        Assert.assertEquals(customObject1.getName(), customObject.getName());
        Assert.assertEquals(customObject1.getDescription(), customObject.getDescription());
        Assert.assertEquals(customObject1.getRating(), customObject.getRating());
    }

    @Test(groups = { "CustomObjectTest" }, dependsOnMethods = {"createCO"}, timeOut = 10000, alwaysRun = true)
    public void getCOs() throws JSONException {
        Response response = CustomObjectApi.getCOs(params.getClient(), api, token, className);

        CustomObjectParameters customObjectParameters = new Gson().fromJson(response.readEntity(String.class),
                CustomObjectParameters.class);
        List<CustomObjectParameters.CustomObject> customObjects = customObjectParameters.getItems();

        Assert.assertNotEquals(customObjects.size(), 0);
    }

    @Test(groups = { "CustomObjectTest" }, dependsOnMethods = {"createCO"}, timeOut = 10000, alwaysRun = true)
    public void updateCO() throws JSONException {
        Response response = CustomObjectApi.updateCO(params.getClient(), api, token, className, (String)params.get("customObjectID"));

        CustomObjectParameters.CustomObject customObject = new Gson().fromJson(response.readEntity(String.class),
                CustomObjectParameters.CustomObject.class);

        Assert.assertEquals(customObject.getRating(), 100);
    }

    @Test(groups = { "CustomObjectTest" }, dependsOnMethods = {"updateCO"}, timeOut = 10000, alwaysRun = true)
    public void deleteCO() throws JSONException {

        Response response = CustomObjectApi.deleteCO(params.getClient(), api, token, className, (String)params.get("customObjectID"));

        Assert.assertEquals(response.getStatus(), 200);
    }

}
