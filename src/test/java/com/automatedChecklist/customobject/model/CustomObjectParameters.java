package com.automatedChecklist.customobject.model;

import java.util.List;


public class CustomObjectParameters {

    private String class_name;
    private int limit;
    private int skip;
    private List<CustomObject> items;


    public String getClass_name() {
        return class_name;
    }

    public int getLimit() {
        return limit;
    }

    public int getSkip() {
        return skip;
    }

    public List<CustomObject> getItems() {
        return items;
    }

    public void setItems(List<CustomObject> items) {
        this.items = items;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }


    public static class CustomObject {

        private String _id;
        private String _parent_id;
        private String created_at;
        private String description;
        private String name;
        private int rating;
        private String updated_at;
        private String user_id;
        private Permissions permissions;

        public class Permissions{
            private Read read;
            private Update update;
            private Delete delete;

            public Delete getDelete() {
                return delete;
            }

            public Read getRead() {
                return read;
            }

            public Update getUpdate() {
                return update;
            }

            public void setDelete(Delete delete) {
                this.delete = delete;
            }

            public void setRead(Read read) {
                this.read = read;
            }

            public void setUpdate(Update update) {
                this.update = update;
            }
        }

        public class Read{
            private String access;

            public String getAccess() {
                return access;
            }

            public void setAccess(String access) {
                this.access = access;
            }
        }

        public class Update{
            private String access;

            public String getAccess() {
                return access;
            }

            public void setAccess(String access) {
                this.access = access;
            }
        }

        public class Delete{
            private String access;

            public String getAccess() {
                return access;
            }

            public void setAccess(String access) {
                this.access = access;
            }
        }


        public Permissions getPermissions() {
            return permissions;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public String get_id() {
            return _id;
        }

        public String get_parent_id() {
            return _parent_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getDescription() {
            return description;
        }

        public String getName() {
            return name;
        }

        public int getRating() {
            return rating;
        }

        public String getUser_id() {
            return user_id;
        }


        public void set_id(String _id) {
            this._id = _id;
        }

        public void set_parent_id(String _parent_id) {
            this._parent_id = _parent_id;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public void setPermissions(Permissions permissions) {
            this.permissions = permissions;
        }
    }
}
