package com.automatedChecklist.customobject.providers;

import com.automatedChecklist.customobject.model.CustomObjectParameters;
import org.testng.annotations.DataProvider;

public class NewCustomObjectData {

    @DataProvider(name = "newCustomObjectData")
    public static Object[][] newCustomObjectData() {

        CustomObjectParameters.CustomObject customObject = new CustomObjectParameters.CustomObject();
        customObject.setName("Batman");
        customObject.setDescription("my favourite film");
        customObject.setRating(5);

        return new Object[][]{{customObject}};
    }

}
