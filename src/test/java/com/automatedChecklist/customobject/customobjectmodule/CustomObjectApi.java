package com.automatedChecklist.customobject.customobjectmodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import com.google.gson.Gson;
import com.automatedChecklist.customobject.model.CustomObjectParameters;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

public class CustomObjectApi {

    public static Response createCO(Client client, String api,
                                    String token, String className,CustomObjectParameters.CustomObject customObject) throws JSONException {

        String path = "data/" + className + ".json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new Gson().toJson(customObject), MediaType.APPLICATION_JSON));
        return response;
    }

    public static Response getCOs(Client client, String api,
                                  String token, String className) throws JSONException {

        String path = "data/" + className + ".json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.GET, null, null);
        return response;

    }

    public static Response updateCO(Client client, String api,
                                    String token, String className, String customObjectID) throws JSONException {

        String path = "data/" + className + "/" + customObjectID + ".json";

        Map map = new HashMap<String,Integer>();
        map.put("rating", 100);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.PUT, map, Entity.entity("", MediaType.APPLICATION_JSON));
        return response;

    }

    public static Response deleteCO(Client client, String api, String token, String className,
                                    String customObjectID) throws JSONException {

        String path = "data/" + className + "/" + customObjectID + ".json";

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.DELETE, null, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

}
