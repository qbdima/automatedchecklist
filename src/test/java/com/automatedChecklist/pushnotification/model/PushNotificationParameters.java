package com.automatedChecklist.pushnotification.model;


public class PushNotificationParameters {

    private PushToken push_token;
    private Device device;

    public static class PushToken {
        private String environment;
        private String client_identification_sequence;

        public String getEnvironment() {
            return environment;
        }

        public void setEnvironment(String environment) {
            this.environment = environment;
        }

        public String getClient_identification_sequence() {
            return client_identification_sequence;
        }

        public void setClient_identification_sequence(String client_identification_sequence) {
            this.client_identification_sequence = client_identification_sequence;
        }

    }

    public static class Device{
        private String platform;
        private String udid;

        public String getPlatform() {
            return platform;
        }

        public void setPlatform(String platform) {
            this.platform = platform;
        }

        public String getUdid() {
            return udid;
        }

        public void setUdid(String udid) {
            this.udid = udid;
        }
    }

    public PushToken getPush_token() {
        return push_token;
    }

    public void setPush_token(PushToken push_token) {
        this.push_token = push_token;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
