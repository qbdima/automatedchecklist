package com.automatedChecklist.pushnotification.pushnotificationmodule;


import com.automatedChecklist.pushnotification.model.EventParameters;
import com.automatedChecklist.pushnotification.model.PushNotificationParameters;
import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PushNotificationApi {

    public static Response createPushToken(Client client, String api, String token, PushNotificationParameters pushNotificationParameters){

        String path = "push_tokens.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new Gson().toJson(pushNotificationParameters), MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response createSubscription(Client client, String api, String token, String channel) throws JSONException {

        JSONObject notification_channels = new JSONObject();
        notification_channels.put("notification_channels", channel);

        String path = "subscriptions.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(notification_channels.toString(), MediaType.APPLICATION_JSON));

            return response;
    }

    public static Response createEvent(Client client, String api, String token, EventParameters eventParameters) {

        String path = "events.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new GsonBuilder().disableHtmlEscaping().create().toJson(eventParameters), MediaType.APPLICATION_JSON));

        return response;
    }

}
