package com.automatedChecklist.pushnotification.suite;


import com.automatedChecklist.pushnotification.model.EventParameters;
import com.automatedChecklist.pushnotification.model.PushNotificationParameters;
import com.automatedChecklist.pushnotification.providers.NewEventData;
import com.automatedChecklist.pushnotification.providers.NewPushTokenData;
import com.automatedChecklist.pushnotification.providers.NewUniversalEventData;
import com.automatedChecklist.pushnotification.pushnotificationmodule.PushNotificationApi;
import com.utils.GlobalParams;
import com.google.gson.Gson;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;


public class PushNotificationSuite {

    private final String channel = "apns";

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;


    @AfterGroups(groups = { "CustomObjectTest" })
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
    }

    @Test(groups = { "PushNotificationTest" }, dependsOnGroups = { "CustomObjectTest" }, dataProvider = "newPushTokenData",
            dataProviderClass = NewPushTokenData.class, alwaysRun = true)
    public void createPushToken(PushNotificationParameters pushNotificationParameters) throws JSONException {

        Response response = PushNotificationApi.createPushToken(params.getClient(), api, token, pushNotificationParameters);

        PushNotificationParameters pushNotificationParameters1 = new Gson().fromJson(response.readEntity(String.class),PushNotificationParameters.class);

        Assert.assertEquals(pushNotificationParameters1.getPush_token().getClient_identification_sequence(),
                pushNotificationParameters.getPush_token().getClient_identification_sequence());
        Assert.assertEquals(pushNotificationParameters1.getPush_token().getEnvironment(), pushNotificationParameters.getPush_token().getEnvironment());

    }

    @Test(groups = { "PushNotificationTest" }, dependsOnMethods = { "createPushToken" }, alwaysRun = true)
    public void createSubscription() throws JSONException {

        Response response = PushNotificationApi.createSubscription(params.getClient(), api, token, channel);

//        JSONObject jsonObject = new Gson().fromJson(response.readEntity(String.class), JSONObject.class);
//        JSONObject subscription = (JSONObject)jsonObject.get("subscription");

        Assert.assertEquals(response.getStatus(), 201);

    }

    @Test(groups = { "PushNotificationTest" }, dependsOnMethods = { "createSubscription" }, dataProvider = "newEventData", dataProviderClass = NewEventData.class, alwaysRun = true)
    public void createEvent(EventParameters eventParameters) {

        Response response = PushNotificationApi.createEvent(params.getClient(), api, token, eventParameters);

        EventParameters eventParameters1 = new Gson().fromJson(response.readEntity(String.class), EventParameters.class);

        Assert.assertEquals(eventParameters1.getEvent().getMessage(), eventParameters.getEvent().getMessage());
        Assert.assertEquals(eventParameters1.getEvent().getApplication_id(), params.get("app_id"));
        Assert.assertEquals(eventParameters1.getEvent().getEvent_type(), "one_shot");
        Assert.assertEquals(response.getStatus(), 201);

    }

    @Test(groups = { "PushNotificationTest" }, dependsOnMethods = { "createEvent" }, dataProvider = "newUniversalEventData", dataProviderClass = NewUniversalEventData.class,
            alwaysRun = true)
    public void createUniversalEvent(EventParameters eventParameters) throws JSONException {

        Response response = PushNotificationApi.createEvent(params.getClient(), api, token, eventParameters);

        JSONArray events = new JSONArray(response.readEntity(String.class));

        EventParameters eventParameters1 = new Gson().fromJson(events.get(0).toString(), EventParameters.class);

        Assert.assertEquals(eventParameters1.getEvent().getApplication_id(), params.get("app_id"));
        Assert.assertEquals(eventParameters1.getEvent().getEvent_type(), "one_shot");
        Assert.assertEquals(response.getStatus(), 201);

    }
}
