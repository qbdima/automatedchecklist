package com.automatedChecklist.pushnotification.providers;

import com.automatedChecklist.pushnotification.model.PushNotificationParameters;
import org.testng.annotations.DataProvider;


public class NewPushTokenData {

    @DataProvider(name = "newPushTokenData")
    public static Object[][] newPushTokenData() {

        PushNotificationParameters pushNotificationParameters = new PushNotificationParameters();
        PushNotificationParameters.PushToken pushToken = new PushNotificationParameters.PushToken();
        PushNotificationParameters.Device device = new PushNotificationParameters.Device();

        pushToken.setEnvironment("development");
        pushToken.setClient_identification_sequence("df23ee81bad8be0575cf457081c695913b040fca0b2cc4fd89a972fe50c66f55");

        device.setPlatform("ios");
        device.setUdid("5FF7AFC0-7A09-4DCE-9E12-685EF475CEC7");

        pushNotificationParameters.setPush_token(pushToken);

        pushNotificationParameters.setDevice(device);


        return new Object[][]{{pushNotificationParameters}};
    }

}
