package com.automatedChecklist.pushnotification.providers;


import com.automatedChecklist.pushnotification.model.EventParameters;

import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONObject;

import org.testng.annotations.DataProvider;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class NewEventData {

    static GlobalParams params = GlobalParams.INSTANCE;
    private static final String prefix = "payload=";

    @DataProvider(name = "newEventData")
    public static Object[][] newEventData() throws UnsupportedEncodingException {

        EventParameters eventParameters = new EventParameters();
        EventParameters.Event event = new EventParameters.Event();
        EventParameters.User user = new EventParameters.User();

        HashMap<String, Object> pushData = new HashMap<>();

        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");

        HashMap<String, String> aps = new HashMap<>();
        aps.put("alert", "Push notification from autotest send " + format1.format(d));
        aps.put("badge", "9");
        aps.put("sound", "default");

        JSONObject apsObject = new JSONObject(aps);

        pushData.put("aps", apsObject);
        pushData.put("key1", "value1");
        pushData.put("key2", "value2");
        pushData.put("key3", "value3");

        String msgBase64;

        JSONObject object = new JSONObject(pushData);

        msgBase64 = Base64.getEncoder().encodeToString(object.toString().getBytes("utf-8"));
        String message = prefix + msgBase64;

        user.setIds(String.valueOf(params.get("userID")));

        event.setEnvironment("development");
        event.setNotification_type("push");
        event.setPush_type("apns");
        event.setUser(user);
        event.setMessage(message);

        eventParameters.setEvent(event);

        return new Object[][]{{eventParameters}};
    }

}



