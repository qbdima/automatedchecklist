package com.automatedChecklist.pushnotification.providers;


import com.automatedChecklist.pushnotification.model.EventParameters;
import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONObject;
import org.testng.annotations.DataProvider;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;

public class NewUniversalEventData {

    static GlobalParams params = GlobalParams.INSTANCE;

    @DataProvider(name = "newUniversalEventData")
    public static Object[][] newUniversalEventData() throws UnsupportedEncodingException {

        EventParameters eventParameters = new EventParameters();
        EventParameters.Event event = new EventParameters.Event();
        EventParameters.User user = new EventParameters.User();

        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss");

        HashMap<String, String> pushData = new HashMap<>();
        pushData.put("message", "Universal push: " + format1.format(d));
        pushData.put("key_id", "123");

        String msgBase64;

        JSONObject object = new JSONObject(pushData);

        msgBase64 = Base64.getEncoder().encodeToString(object.toString().getBytes("utf-8"));

        user.setIds(String.valueOf(params.get("userID")));

        event.setEnvironment("development");
        event.setNotification_type("push");
        event.setMessage(msgBase64);
        event.setUser(user);

        eventParameters.setEvent(event);

        return new Object[][]{{eventParameters}};
    }

}
