package com.automatedChecklist.content.model;

public class BlobParameters {

    private Blob blob;

    public Blob getBlob() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }


    public class Blob{
        private String id;
        private String udid;
        private String content_type;
        private String name;
        private long size;
        private String created_at;
        private String updated_at;
        private int ref_count;
        private String blob_status;
        private String set_completed_at;
        private String last_read_access_ts;
        private String lifetime;
        private int account_id;
        private BlobObjectAccess blob_object_access;

        public int getAccount_id() {
            return account_id;
        }

        public int getRef_count() {
            return ref_count;
        }

        public long getSize() {
            return size;
        }

        public BlobObjectAccess getBlob_object_access() {
            return blob_object_access;
        }

        public String getBlob_status() {
            return blob_status;
        }

        public String getContent_type() {
            return content_type;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getId() {
            return id;
        }

        public String getLast_read_access_ts() {
            return last_read_access_ts;
        }

        public String getLifetime() {
            return lifetime;
        }

        public String getName() {
            return name;
        }

        public String getSet_completed_at() {
            return set_completed_at;
        }

        public String getUdid() {
            return udid;
        }

        public String getUpdated_at() {
            return updated_at;
        }


        public void setAccount_id(int account_id) {
            this.account_id = account_id;
        }

        public void setBlob_object_access(BlobObjectAccess blob_object_access) {
            this.blob_object_access = blob_object_access;
        }

        public void setBlob_status(String blob_status) {
            this.blob_status = blob_status;
        }

        public void setContent_type(String content_type) {
            this.content_type = content_type;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setLast_read_access_ts(String last_read_access_ts) {
            this.last_read_access_ts = last_read_access_ts;
        }

        public void setLifetime(String lifetime) {
            this.lifetime = lifetime;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setRef_count(int ref_count) {
            this.ref_count = ref_count;
        }

        public void setSet_completed_at(String set_completed_at) {
            this.set_completed_at = set_completed_at;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public void setUdid(String udid) {
            this.udid = udid;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

    }

    public class BlobObjectAccess{
        private int id;
        private int blob_id;
        private String expires;
        private String object_access_type;
        private String params;

        public int getBlob_id() {
            return blob_id;
        }

        public int getId() {
            return id;
        }

        public String getExpires() {
            return expires;
        }

        public String getObject_access_type() {
            return object_access_type;
        }

        public String getParams() {
            return params;
        }


        public void setBlob_id(int blob_id) {
            this.blob_id = blob_id;
        }

        public void setExpires(String expires) {
            this.expires = expires;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setObject_access_type(String object_access_type) {
            this.object_access_type = object_access_type;
        }

        public void setParams(String params) {
            this.params = params;
        }

    }


}
