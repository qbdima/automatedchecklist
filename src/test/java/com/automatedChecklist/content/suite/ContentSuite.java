package com.automatedChecklist.content.suite;

import com.google.gson.Gson;
import com.automatedChecklist.content.contentmodule.ContentApi;
import com.automatedChecklist.content.model.BlobParameters;
//import com.utils.CopyResource;
import com.utils.Consts;
import com.utils.GlobalParams;
import com.utils.UtilsMethods;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class ContentSuite {

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;
    static long file_size;
    static HashMap<String, String> arr;
   // CopyResource copyResource;
   // String filePath = "./src/test/resources/dog.jpeg";
    File file = new File(Consts.IMAGE_PATH);

    @AfterGroups(groups = { "UserTest"})
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
    }

    @Test(groups = { "ContentTest" }, dependsOnGroups = { "ChatTest" }, timeOut = 10000, alwaysRun = true)
    public void createBlobObject() throws JSONException {

//        copyResource = new CopyResource();
//        copyResource.copy("dog.jpeg");

        Response response = ContentApi.createBlobObject(params.getClient(), token, api, file.getAbsolutePath());

        BlobParameters blobParameters = new Gson().fromJson(response.readEntity(String.class), BlobParameters.class);
        String str = blobParameters.getBlob().getBlob_object_access().getParams();

        arr = UtilsMethods.parseBlobObject(str);
        params.put("blobID", blobParameters.getBlob().getId());
        params.put("bucketUrl", UtilsMethods.getBucketUrl(str));

        Assert.assertEquals(response.getStatus(), 201);
    }


    @Test(groups = { "ContentTest" }, dependsOnMethods = {"createBlobObject"}, timeOut = 10000, alwaysRun = true)
    public void uploadFile() throws JSONException, UnsupportedEncodingException {

        file_size = file.length();

        Response response = ContentApi.uploadFile(arr, file, (String)params.get("bucketUrl"));

        Assert.assertEquals(response.getStatus(), 201);
    }


    @Test(groups = { "ContentTest" }, dependsOnMethods = {"uploadFile"}, timeOut = 10000, alwaysRun = true)
    public void declaringFileUploaded() throws JSONException {

        Response response = ContentApi.declaringFileUploaded(params.getClient(), api, (String)params.get("blobID"), file_size, token);

        Assert.assertEquals(response.getStatus(), 200);
    }

}
