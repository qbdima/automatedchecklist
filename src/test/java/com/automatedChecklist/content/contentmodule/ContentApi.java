package com.automatedChecklist.content.contentmodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;

import org.codehaus.jettison.json.JSONException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;


public class ContentApi {

    public static Response createBlobObject(Client client, String token, String api, String blobFilePAth) throws JSONException {

        String[] parts = blobFilePAth.split("/");

        Map map = new HashMap<String,String>();
        map.put("blob[content_type]", "image/jpeg");
        map.put("blob[name]", parts[parts.length - 1]);

        String path = "blobs.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response uploadFile(HashMap<String, String> arr, File file, String bucketUrl) throws JSONException, UnsupportedEncodingException {
        final Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

        final FileDataBodyPart filePart = new FileDataBodyPart("file", file);
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        final FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart;

        for (String key : arr.keySet()) {

            formDataMultiPart
                    .field(key, URLDecoder.decode(arr.get(key), "UTF-8"));

        }
        formDataMultiPart.bodyPart(filePart);
        final WebTarget target = client.target(bucketUrl);
        final Response response = target.request().post(Entity.entity(multipart, multipart.getMediaType()));

        return response;
    }

    public static Response declaringFileUploaded(Client client, String api, String blobID,
                                                 long file_size, String token) throws JSONException {

        String path = "blobs/" + blobID + "/complete.json";

        Map map = new HashMap<String,Object>();
        map.put("blob[size]", file_size);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.PUT, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

}
