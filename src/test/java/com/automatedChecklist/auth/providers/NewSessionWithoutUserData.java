package com.automatedChecklist.auth.providers;


import com.utils.GlobalParams;
import com.utils.UtilsMethods;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.annotations.DataProvider;

import java.security.SignatureException;
import java.util.Random;

public class NewSessionWithoutUserData {

    static GlobalParams params = GlobalParams.INSTANCE;

//    public static String authSecret;
//    public static String appID;
//    public static String authKey;

    /// хорошо бы переписать провайдер, чтобы он возвращал не JSONObject а Session обьект

//    @DataProvider(name = "newSessionWithoutUserData")
//    public static Object[][] newSessionWithoutUserData() throws SignatureException, JSONException {
//
//        Random random = new Random();
//
//        String nonce = Integer.toString(random.nextInt());
//        long time = System.currentTimeMillis() / 1000;
//        String timestamp = Long.toString(time);
//        String signature;
//
//        String srt = "application_id=" + appID + "&" + "auth_key=" + authKey + "&" + "nonce="
//                + nonce + "&" + "timestamp=" + timestamp;
//
//        signature = UtilsMethods.calculateHMAC_SHA(srt, authSecret);
//
//        JSONObject sessionParams = new JSONObject();
//        sessionParams.put("application_id", appID);
//        sessionParams.put("auth_key", authKey);
//        sessionParams.put("nonce", nonce);
//        sessionParams.put("timestamp", timestamp);
//        sessionParams.put("signature", signature);
//
//        return new Object[][]{{sessionParams}};

    @DataProvider(name = "newSessionWithoutUserData")
    public static Object[][] newSessionWithoutUserData() throws SignatureException, JSONException {

        Random random = new Random();

        String nonce = Integer.toString(random.nextInt());
        long time = System.currentTimeMillis() / 1000;
        String timestamp = Long.toString(time);
        String signature;

        String srt = "application_id=" + params.get("app_id") + "&" + "auth_key=" + params.get("auth_key") + "&" + "nonce="
                + nonce + "&" + "timestamp=" + timestamp;

        signature = UtilsMethods.calculateHMAC_SHA(srt, (String)params.get("auth_secret"));

        JSONObject sessionParams = new JSONObject();
        sessionParams.put("application_id", params.get("app_id"));
        sessionParams.put("auth_key", params.get("auth_key"));
        sessionParams.put("nonce", nonce);
        sessionParams.put("timestamp", timestamp);
        sessionParams.put("signature", signature);

        return new Object[][]{{sessionParams}};

    }



}
