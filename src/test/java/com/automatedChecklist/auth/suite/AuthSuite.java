package com.automatedChecklist.auth.suite;

import com.createConfig.MakeConfig;
import com.google.gson.Gson;
import com.automatedChecklist.auth.authmodule.AuthApi;
import com.automatedChecklist.auth.model.SessionParameters;
import com.automatedChecklist.auth.providers.NewSessionWithoutUserData;
import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.security.SignatureException;

public class AuthSuite {

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;

   @AfterGroups(groups = {"config"})
    public void setUp(){
       api = (String)params.get("api_domain");
    }
/*   @BeforeSuite
    public static void setUpClass() {

        MakeConfig makeConfig = new MakeConfig();
        String[] args = new String[0];
        makeConfig.main(args);

//        params.put("app_id", "14879");
//        params.put("auth_key", "kuVm8rzaCZws25-");
//        params.put("auth_secret", "p-ZEfvtbnfgSE48");
//        params.put("api_domain", "https://api.quickblox.com/");
//        params.put("chat_domain", "chat.quickblox.com");

        api = (String)params.get("api_domain");

//        NewSessionWithoutUserData.appID = "14879";
//        NewSessionWithoutUserData.authKey = "kuVm8rzaCZws25-";
//        NewSessionWithoutUserData.authSecret = "p-ZEfvtbnfgSE48";
//        params.put("appID",NewSessionWithoutUserData.appID);
    }*/

    @Test(groups = { "AuthTest" }, dependsOnGroups = {"config"},dataProvider = "newSessionWithoutUserData", dataProviderClass = NewSessionWithoutUserData.class,
            priority = 0, timeOut = 10000, alwaysRun = true)
    public void createSessionWithoutUser(JSONObject sessionParams) throws JSONException, SignatureException {

        Response response = AuthApi.createSessionWithoutUser(params.getClient(), api, sessionParams);

        SessionParameters sessionParameters = new Gson().fromJson(response.readEntity(String.class), SessionParameters.class);
        params.put("token", sessionParameters.getSession().getToken());

        Assert.assertEquals(response.getStatus(), 201);
       // Assert.assertEquals(sessionParameters.getSession().getApplication_id(), NewSessionWithoutUserData.appID);
        Assert.assertNotNull(sessionParameters.getSession().getToken());

    }

    @Test(groups = { "AuthTest" }, dependsOnGroups = {"config"}, dataProvider = "newSessionWithoutUserData", dataProviderClass = NewSessionWithoutUserData.class,
            priority = 0, timeOut = 10000, alwaysRun = true)
    public void createSessionWithoutUser2(JSONObject sessionParams) throws JSONException, SignatureException {

        Response response = AuthApi.createSessionWithoutUser(params.getClient(), api, sessionParams);

        SessionParameters sessionParameters = new Gson().fromJson(response.readEntity(String.class), SessionParameters.class);
        params.put("token2", sessionParameters.getSession().getToken());

        Assert.assertEquals(response.getStatus(), 201);
        Assert.assertNotNull(sessionParameters.getSession().getToken());
    }

}
