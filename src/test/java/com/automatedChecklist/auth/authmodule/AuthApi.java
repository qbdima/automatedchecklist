package com.automatedChecklist.auth.authmodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

public class AuthApi {

    public static Response createSessionWithoutUser(Client client, String api, JSONObject sessionParams) throws JSONException, SignatureException {

        String path = "session.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, null,
                RESPONSE_TYPE.POST, null, Entity.entity(sessionParams.toString(), MediaType.APPLICATION_JSON));

        return response;

    }

    public static Response updateSession(Client client, String api,
                                         String token, String userLogin, String userPassword) throws JSONException {

        String path = "/login.json";

        Map map = new HashMap<String,String>();
        map.put("login", userLogin);
        map.put("password", userPassword);

        Response response = Request.getResponse(Request.getTarget(client, api, path),
                MediaType.APPLICATION_JSON_TYPE, token, RESPONSE_TYPE.POST, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

}
