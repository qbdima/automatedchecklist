package com.automatedChecklist.auth.model;

public class SessionParameters {

    private Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public class Session {
        private String _id;
        private int application_id;
        private String created_at;
        private int device_id;
        private int nonce;
        private String token;
        private String ts;
        private String updated_at;
        private int user_id;
        private int id;


        public String get_id() {
            return _id;
        }

        public int getApplication_id() {
            return application_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public int getDevice_id() {
            return device_id;
        }

        public int getNonce() {
            return nonce;
        }

        public String getToken() {
            return token;
        }

        public String getTs() {
            return ts;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public int getUser_id() {
            return user_id;
        }

        public int getId() {
            return id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public void setApplication_id(int application_id) {
            this.application_id = application_id;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setDevice_id(int device_id) {
            this.device_id = device_id;
        }

        public void setNonce(int nonce) {
            this.nonce = nonce;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }



}
//((JSONObject)json.get("session")).get("token").toString();