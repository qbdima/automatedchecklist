package com.automatedChecklist.user.usermodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import com.google.gson.Gson;
import com.automatedChecklist.user.model.UserParameters;
import org.codehaus.jettison.json.JSONException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

public class UsersApi {

    public static Response createNewApiUser(Client client, String api,
                                 String token, UserParameters userParameters) throws JSONException {

        String path = "users.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(new Gson().toJson(userParameters), MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response resetPasswordForNewApiUser(Client client, String api,
                                           String token, String email) throws JSONException {

        String path = "users/password/reset.json";

        Map map = new HashMap<String,String>();
        map.put("email", email);

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.GET, map, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response getUsers(Client client, String api, String token) throws JSONException {

        String path = "users.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.GET, null, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;

    }

    public static Response deleteUser(Client client, String api, String token, long userID) throws JSONException {

        String path = "users/" + userID + ".json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.DELETE, null, Entity.entity("", MediaType.APPLICATION_JSON));

        return response;

    }
}
