package com.automatedChecklist.user.model;

import java.util.List;

public class UsersArrayParameters {

    private int per_page;
    private int total_entries;
    private int current_page;
    private List<UserParameters> items;

//    class Items{
//        private UserWrapper user;
//
//        public UserWrapper getUser() {
//            return user;
//        }
//
//        public void setUser(UserWrapper user) {
//            this.user = user;
//        }
//    }

    public int getCurrent_page() {
        return current_page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getTotal_entries() {
        return total_entries;
    }

//    public List<Items> getItems() {
//        return items;
//    }


    public List<UserParameters> getItems() {
        return items;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public void setTotal_entries(int total_entries) {
        this.total_entries = total_entries;
    }

//    public void setItems(List<Items> items) {
//        this.items = items;
//    }


    public void setItems(List<UserParameters> items) {
        this.items = items;
    }
}
