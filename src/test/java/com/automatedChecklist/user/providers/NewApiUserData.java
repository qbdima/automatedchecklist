package com.automatedChecklist.user.providers;

import com.automatedChecklist.user.model.UserParameters;
import org.testng.annotations.DataProvider;

import java.util.Random;


public class NewApiUserData {

    @DataProvider(name = "newApiUserData")
    public static Object[][] newApiUserData() {

        Random random = new Random();

        String login = Integer.toString(random.nextInt(Integer.MAX_VALUE) + 1);

        UserParameters userParameters = new UserParameters();
        UserParameters.User us = userParameters.new User();
        us.setLogin(login);
        us.setEmail(login + "@test.com");
        us.setPassword("11111111");
        userParameters.setUser(us);

        return new Object[][]{{userParameters}};
    }


}
