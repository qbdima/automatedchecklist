package com.automatedChecklist.user.suite;

import com.google.gson.Gson;
import com.automatedChecklist.auth.authmodule.AuthApi;
import com.automatedChecklist.user.model.UserParameters;
import com.automatedChecklist.user.model.UsersArrayParameters;
import com.automatedChecklist.user.providers.NewApiUserData;
import com.automatedChecklist.user.usermodule.UsersApi;
import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.security.SignatureException;


public class UserSuite {

    private static final String password = "11111111";
    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;

    @AfterGroups(groups = { "AuthTest" })
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
    }

    @Test(groups = { "UserTest" },dependsOnGroups = { "AuthTest" }, dataProvider = "newApiUserData", dataProviderClass = NewApiUserData.class,
            timeOut = 10000, alwaysRun = true)
    public void createNewApiUser(UserParameters userParameters) throws JSONException {

        params.put("userEmail", userParameters.getUser().getEmail());
        params.put("userLogin", userParameters.getUser().getLogin());

        Response response = UsersApi.createNewApiUser(params.getClient(), api, token, userParameters);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        params.put("userID", user.getId());


        Assert.assertEquals(user.getEmail(), userParameters.getUser().getEmail());
        Assert.assertEquals(user.getLogin(), userParameters.getUser().getLogin());
    }

    @Test(groups = { "UserTest" }, dependsOnMethods = {"createNewApiUser"}, dataProvider = "newApiUserData",
            dataProviderClass = NewApiUserData.class, timeOut = 10000, alwaysRun = true)
    public void createNewApiUser2(UserParameters userParameters) throws JSONException {

        params.put("userEmail2", userParameters.getUser().getEmail());
        params.put("userLogin2", userParameters.getUser().getLogin());

        Response response = UsersApi.createNewApiUser(params.getClient(), api, token, userParameters);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        params.put("userID2", user.getId());

        Assert.assertEquals(user.getEmail(), userParameters.getUser().getEmail());
        Assert.assertEquals(user.getLogin(), userParameters.getUser().getLogin());
    }


    @Test(groups = { "UserTest" }, dependsOnMethods = {"createNewApiUser"}, timeOut = 10000, alwaysRun = true)
    public void updateSession() throws JSONException, SignatureException {

        Response response = AuthApi.updateSession(params.getClient(), api, token, (String) params.get("userLogin"), password);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        Assert.assertEquals(response.getStatus(), 202);
        Assert.assertEquals(user.getId(), (long)params.get("userID"));
        Assert.assertEquals(user.getEmail(), (String)params.get("userEmail"));
        Assert.assertEquals(user.getLogin(), (String)params.get("userLogin"));

    }

    @Test(groups = { "UserTest" }, dependsOnMethods = {"createNewApiUser2"}, timeOut = 10000, alwaysRun = true)
    public void updateSession2() throws JSONException, SignatureException {

        Response response = AuthApi.updateSession(params.getClient(), api, (String)params.get("token2"), (String)params.get("userLogin2"), password);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        Assert.assertEquals(response.getStatus(), 202);
        Assert.assertEquals(user.getId(), (long)params.get("userID2"));
        Assert.assertEquals(user.getEmail(), (String)params.get("userEmail2"));
        Assert.assertEquals(user.getLogin(), (String)params.get("userLogin2"));

    }

    @Test(groups = { "UserTest" }, dependsOnMethods = {"updateSession"}, timeOut = 10000, alwaysRun = true)
    public void getUsers() throws JSONException {

        Response response = UsersApi.getUsers(params.getClient(), api, token);

        UsersArrayParameters users = new Gson().fromJson(response.readEntity(String.class), UsersArrayParameters.class);

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertNotEquals(users.getItems().size(), 0);
    }

}
