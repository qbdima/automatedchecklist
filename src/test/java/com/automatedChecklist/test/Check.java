package com.automatedChecklist.test;

import com.google.gson.Gson;
import com.automatedChecklist.auth.authmodule.AuthApi;
import com.automatedChecklist.auth.model.SessionParameters;
import com.automatedChecklist.auth.providers.NewSessionWithoutUserData;
import com.automatedChecklist.chat.chatmodule.ChatApi;
import com.automatedChecklist.chat.model.ChatMessageParameters;
import com.automatedChecklist.chat.model.DialogParameters;
import com.automatedChecklist.chat.providers.NewDialogData;
import com.automatedChecklist.chat.providers.NewRestApiChatMessageData;
import com.automatedChecklist.content.contentmodule.ContentApi;
import com.automatedChecklist.content.model.BlobParameters;
import com.automatedChecklist.customobject.customobjectmodule.CustomObjectApi;
import com.automatedChecklist.customobject.model.CustomObjectParameters;
import com.automatedChecklist.customobject.providers.NewCustomObjectData;
import com.automatedChecklist.location.locationmodule.LocationApi;
import com.automatedChecklist.location.model.LocationParameters;
import com.automatedChecklist.location.providers.NewLocationData;
import com.automatedChecklist.user.model.UserParameters;
import com.automatedChecklist.user.model.UsersArrayParameters;
import com.automatedChecklist.user.providers.NewApiUserData;
import com.automatedChecklist.user.usermodule.UsersApi;
//import com.utils.CopyResource;
import com.utils.GlobalParams;
import com.utils.UtilsMethods;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.*;


public class Check extends JerseyTest {

    private static final String className = "Movie";

    static GlobalParams params = GlobalParams.INSTANCE;

    static HashMap<String, String> arr;
    static long file_size;

    static Client client;

    static String api = "https://api.quickblox.com/";
//    static String appID = "14879";
//    static String authKey = "kuVm8rzaCZws25-";
//    static String authSecret = "p-ZEfvtbnfgSE48";

    static String blobFilePAth = "/Users/dima_solovyov/Downloads/dog.jpeg";

    static String userPassword = "11111111";

    @Path("https://api.quickblox.com/")
    public static class HttpResource {
        @GET
        public Response getResponse(@Context HttpServletRequest request) {
            return Response.ok(request.getMethod()).build();
        }
    }

    @Override
    public Application configure() {
        ResourceConfig config = new ResourceConfig(HttpResource.class);
        config.register(new AbstractBinder() {
            @Override
            public void configure() {
                bindFactory(HttpServletRequestFactory.class)
                        .to(HttpServletRequest.class);
            }
        });
        return config;
    }

    public static class HttpServletRequestFactory implements Factory<HttpServletRequest> {

        @Override
        public HttpServletRequest provide() {
            //   return new MockHttpServletRequest();
            return null;
        }

        @Override
        public void dispose(HttpServletRequest t) {
        }
    }

    @BeforeClass
    public static void setUpClass() {
        client = ClientBuilder.newClient();

//        NewSessionWithoutUserData.appID = "14879";
//        NewSessionWithoutUserData.authKey = "kuVm8rzaCZws25-";
//        NewSessionWithoutUserData.authSecret = "p-ZEfvtbnfgSE48";
    }


    @Test(dataProvider = "newSessionWithoutUserData", dataProviderClass = NewSessionWithoutUserData.class, priority = 0)
    public void createSessionWithoutUser(JSONObject sessionParams) throws JSONException, SignatureException {

        Response response = AuthApi.createSessionWithoutUser(client, api, sessionParams);

        SessionParameters sessionParameters = new Gson().fromJson(response.readEntity(String.class), SessionParameters.class);
        params.put("token", sessionParameters.getSession().getToken());

        Assert.assertEquals(response.getStatus(), 201);
        Assert.assertNotNull(sessionParameters.getSession().getToken());

    }


    @Test(dependsOnMethods = {"createSessionWithoutUser"}, dataProvider = "newApiUserData", dataProviderClass = NewApiUserData.class)
    public void createNewApiUser(UserParameters userParameters) throws JSONException {

        params.put("userEmail", userParameters.getUser().getEmail());
        params.put("userLogin", userParameters.getUser().getLogin());

        Response response = UsersApi.createNewApiUser(client, api, (String)params.get("token"), userParameters);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        params.put("userID", user.getId());


        Assert.assertEquals(user.getEmail(), userParameters.getUser().getEmail());
        Assert.assertEquals(user.getLogin(), userParameters.getUser().getLogin());
    }

    @Test(dependsOnMethods = {"createNewApiUser"}, dataProvider = "newApiUserData", dataProviderClass = NewApiUserData.class)
    public void createNewApiUser2(UserParameters userParameters) throws JSONException {

        params.put("userEmail2", userParameters.getUser().getEmail());
        params.put("userLogin2", userParameters.getUser().getLogin());

        Response response = UsersApi.createNewApiUser(client, api, (String)params.get("token"), userParameters);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

         params.put("userID2", user.getId());

        Assert.assertEquals(user.getEmail(), userParameters.getUser().getEmail());
        Assert.assertEquals(user.getLogin(), userParameters.getUser().getLogin());
    }


    @Test(dependsOnMethods = {"createNewApiUser"})
    public void updateSession() throws JSONException, SignatureException {

        Response response = AuthApi.updateSession(client, api, (String)params.get("token"), (String)params.get("userLogin"), userPassword);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        Assert.assertEquals(response.getStatus(), 202);
        Assert.assertEquals(user.getId(), (long)params.get("userID"));
        Assert.assertEquals(user.getEmail(), (String)params.get("userEmail"));
        Assert.assertEquals(user.getLogin(), (String)params.get("userLogin"));

    }

//    @Test(enabled = false)
//    public void createSessionWithUser() throws JSONException, SignatureException {
//
//        Random random = new Random();
//
//        String nonce = Integer.toString(random.nextInt());
//        long time = System.currentTimeMillis() / 1000;
//        String timestamp = Long.toString(time);
//        String signature;
//
//        //userLogin = "test" + time;
//        userLogin = "qbtest2";
//
//        String srt = "application_id=" + appID + "&" + "auth_key=" + authKey + "&" + "nonce="
//                + nonce + "&" + "timestamp=" + timestamp + "&" + "user[login]=" + userLogin + "&" + "user[password]=" + userPassword;
//
//        signature = UtilsMethods.calculateHMAC_SHA(srt, authSecret);
//
//        WebTarget target = client
//                .target(api).path("session.json");
//
//        Response response = target
//                .queryParam("application_id", appID)
//                .queryParam("auth_key", authKey)
//                .queryParam("nonce", nonce)
//                .queryParam("timestamp", timestamp)
//                .queryParam("user[login]", userLogin)
//                .queryParam("user[password]", "11111111")
//                .queryParam("signature", signature)
//                .request(MediaType.APPLICATION_JSON)
//                .post(Entity.entity("", MediaType.APPLICATION_JSON), Response.class);
//
//        JSONObject json = new JSONObject(response.readEntity(String.class));
//
//        SessionParameters sessionParameters = new Gson().fromJson(json.toString(), SessionParameters.class);
//    //    token = session.getSession().getToken();
//        System.out.println(json);
//    }


    //Content
    @Test(dependsOnMethods = {"updateSession"})
    public void createBlobObject() throws JSONException {
        Response response = ContentApi.createBlobObject(client, (String)params.get("token"), api, blobFilePAth);

        BlobParameters blobParameters = new Gson().fromJson(response.readEntity(String.class), BlobParameters.class);
        String str = blobParameters.getBlob().getBlob_object_access().getParams();

        arr = UtilsMethods.parseBlobObject(str);
        params.put("blobID", blobParameters.getBlob().getId());

        Assert.assertEquals(response.getStatus(), 201);
    }


    @Test(dependsOnMethods = {"createBlobObject"})
    public void uploadFile() throws JSONException, UnsupportedEncodingException {

        String filePath = "./src/test/resources/dog.jpeg";
        File file = new File(filePath);
//        CopyResource copyResource = new CopyResource();
//        copyResource.copy("dog.jpeg");
//
//        File file = new File(copyResource.getResourcePath());
        file_size = file.length();


//        File file = new File(blobFilePAth);
//        file_size = file.length();

    //    Response response = ContentApi.uploadFile(arr, file);

      //  Assert.assertEquals(response.getStatus(), 201);
    }


    @Test(dependsOnMethods = {"uploadFile"})
    public void declaringFileUploaded() throws JSONException {

        Response response = ContentApi.declaringFileUploaded(client, api, (String)params.get("blobID"), file_size, (String)params.get("token"));

        Assert.assertEquals(response.getStatus(), 200);
    }

    @Test(dependsOnMethods = {"updateSession", "createNewApiUser"})
    public void resetPasswordForNewApiUser() throws JSONException {

        Response response = UsersApi.resetPasswordForNewApiUser(client, api, (String)params.get("token"), (String)params.get("userEmail"));

        Assert.assertEquals(response.getStatus(), 200);

    }


    @Test(dependsOnMethods = {"updateSession"})
    public void getUsers() throws JSONException {

        Response response = UsersApi.getUsers(client, api, (String)params.get("token"));

        UsersArrayParameters users = new Gson().fromJson(response.readEntity(String.class), UsersArrayParameters.class);

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertNotEquals(users.getItems().size(), 0);
    }

    @Test(dependsOnMethods = {"updateSession", "createNewApiUser2"},
            dataProvider = "newDialogData", dataProviderClass = NewDialogData.class)
    public void createNewDialog(DialogParameters.Dialog dialog) throws JSONException {

        Response response = ChatApi.createNewDialog(client, api, (String)params.get("token"), dialog);

        DialogParameters.Dialog dialog1 = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);
        params.put("dialogID", dialog1.get_id());

        long[] tmp = dialog1.getOccupants_ids();

        Arrays.sort(tmp);

        Assert.assertEquals(tmp.length, 2);
        Assert.assertEquals(tmp[0], params.get("userID"));
        Assert.assertEquals(tmp[1], params.get("userID2"));

        Assert.assertEquals(response.getStatus(), 201);

    }

    @Test(dependsOnMethods = {"pullOccupants"}, dataProvider = "newDialogData", dataProviderClass = NewDialogData.class)
    public void pushOccupants(DialogParameters.Dialog dialog) throws JSONException {

        Response response = ChatApi.pushOccupants(client, api, (String)params.get("token"), (String)params.get("dialogID"), (long)params.get("userID2"));

        DialogParameters.Dialog dialog1 = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);

        long[] tmp = dialog.getOccupants_ids();
        long[] tmp1 = dialog1.getOccupants_ids();

        tmp[0] = (long)params.get("userID");

        Arrays.sort(tmp);
        Arrays.sort(tmp1);

        boolean isEquals = false;

        if(Arrays.equals(tmp,tmp1)){
            isEquals = true;
        }

        Assert.assertTrue(isEquals);

    }

    @Test(dependsOnMethods = {"createNewDialog"}, dataProvider = "newDialogData", dataProviderClass = NewDialogData.class)
    public void pullOccupants(DialogParameters.Dialog dialog) throws JSONException {

        Response response = ChatApi.pullOccupants(client, api, (String)params.get("token"),  (String)params.get("dialogID"), (long)params.get("userID2"));

        DialogParameters.Dialog dialog1 = new Gson().fromJson(response.readEntity(String.class), DialogParameters.Dialog.class);

        long[] tmp = dialog1.getOccupants_ids();

        Assert.assertEquals(tmp.length, 1);
        Assert.assertEquals(tmp[0], (long)params.get("userID"));

    }

    @Test(dependsOnMethods = {"pushOccupants"}, dataProvider = "newRestApiChatMessageData",
            dataProviderClass = NewRestApiChatMessageData.class)
    public void createMessageRestAPI(ChatMessageParameters.ChatMessage chatMessage) throws JSONException {

        Response response = ChatApi.createMessageRestAPI(client, api, (String)params.get("token"), chatMessage);

        ChatMessageParameters.ChatMessage message = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.ChatMessage.class);
        params.put("messageID", message.get_id());

        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
        Assert.assertEquals(message.getMessage(), chatMessage.getMessage());
        Assert.assertEquals(message.getSender_id(), params.get("userID"));

    }

    @Test(dependsOnMethods = {"createMessageRestAPI"})
    public void getMessageByID() throws JSONException {

        Response response = ChatApi.getMessageByID(client, api, (String)params.get("token"), (String)params.get("dialogID"), (String)params.get("messageID"));

        ChatMessageParameters chatMessageParameters = new Gson().fromJson(response.readEntity(String.class), ChatMessageParameters.class);
        List<ChatMessageParameters.ChatMessage> messages = chatMessageParameters.getItems();

        ChatMessageParameters.ChatMessage message = messages.get(0);
        Assert.assertEquals(message.getChat_dialog_id(), params.get("dialogID"));
        Assert.assertEquals(message.getSender_id(), params.get("userID"));

    }



    //CustomObject
    @Test(dependsOnMethods = {"updateSession"}, dataProvider = "newCustomObjectData", dataProviderClass = NewCustomObjectData.class)
    public void createCO(CustomObjectParameters.CustomObject customObject) throws JSONException {

        Response response = CustomObjectApi.createCO(client, api, (String)params.get("token"), className,customObject);

        CustomObjectParameters.CustomObject customObject1 = new Gson().fromJson(response.readEntity(String.class), CustomObjectParameters.CustomObject.class);
        params.put("customObjectID", customObject1.get_id());

        Assert.assertEquals(customObject1.getName(), customObject.getName());
        Assert.assertEquals(customObject1.getDescription(), customObject.getDescription());
        Assert.assertEquals(customObject1.getRating(), customObject.getRating());
    }

    @Test(dependsOnMethods = {"updateSession"})
    public void getCOs() throws JSONException {
        Response response = CustomObjectApi.getCOs(client, api, (String)params.get("token"), className);

        CustomObjectParameters customObjectParameters = new Gson().fromJson(response.readEntity(String.class),
                CustomObjectParameters.class);
        List<CustomObjectParameters.CustomObject> customObjects = customObjectParameters.getItems();

        Assert.assertNotEquals(customObjects.size(), 0);
    }

    @Test(dependsOnMethods = {"createCO"})
    public void updateCO() throws JSONException {
        Response response = CustomObjectApi.updateCO(client, api, (String)params.get("token"), className,(String)params.get("customObjectID"));

        CustomObjectParameters.CustomObject customObject = new Gson().fromJson(response.readEntity(String.class),
                CustomObjectParameters.CustomObject.class);

        Assert.assertEquals(customObject.getRating(), 100);
    }

    @Test(dependsOnMethods = {"updateCO"})
    public void deleteCO() throws JSONException {

        Response response = CustomObjectApi.deleteCO(client, api, (String)params.get("token"), className,(String)params.get("customObjectID"));

        Assert.assertEquals(response.getStatus(), 200);
    }


    //Location
    @Test(dependsOnMethods = {"updateSession"}, dataProvider = "newLocationData", dataProviderClass = NewLocationData.class)
    public void createLocation(JSONObject obj) throws JSONException {

        Response response = LocationApi.createLocation(client, api, (String)params.get("token"), obj);

        LocationParameters.Location location = new Gson().fromJson(response.readEntity(String.class), LocationParameters.Location.class);

        Assert.assertEquals(location.getGeo_datum().getLatitude(), obj.getJSONObject("geo_data").get("latitude"));
        Assert.assertEquals(location.getGeo_datum().getLongitude(), obj.getJSONObject("geo_data").get("longitude"));
        Assert.assertEquals(response.getStatus(), 201);
    }

    @Test(dependsOnMethods = {"updateSession"})
    public void getLocations() throws JSONException {
        Response response = LocationApi.getLocations(client, api, (String)params.get("token"));

        LocationParameters locationParameters = new Gson().fromJson(response.readEntity(String.class),
                LocationParameters.class);
        List<LocationParameters.Location> locations = locationParameters.getItems();

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertNotEquals(locations.size(), 0);
    }


    //Remove test data
    @Test(dependsOnMethods = {"getMessageByID"})
    public void DeleteDialog() throws JSONException {
        Response response = ChatApi.deleteDialog(client, api, (String)params.get("token"),  (String)params.get("dialogID"));

        Assert.assertEquals(response.getStatus(), 200);
    }


    @Test(dataProvider = "newSessionWithoutUserData", dataProviderClass = NewSessionWithoutUserData.class, priority = 0)
    public void createSessionWithoutUser2(JSONObject sessionParams) throws JSONException, SignatureException {

        Response response = AuthApi.createSessionWithoutUser(client, api, sessionParams);

        SessionParameters sessionParameters = new Gson().fromJson(response.readEntity(String.class), SessionParameters.class);
        params.put("token2", sessionParameters.getSession().getToken());

        Assert.assertEquals(response.getStatus(), 201);
        Assert.assertNotNull((String)params.get("token2"));
    }


    @Test(dependsOnMethods = {"createNewApiUser2"})
    public void updateSession2() throws JSONException, SignatureException {

        Response response = AuthApi.updateSession(client, api, (String)params.get("token2"), (String)params.get("userLogin2"), userPassword);

        UserParameters userWr = new Gson().fromJson(response.readEntity(String.class), UserParameters.class);
        UserParameters.User user = userWr.getUser();

        Assert.assertEquals(response.getStatus(), 202);
        Assert.assertEquals(user.getId(), (long)params.get("userID2"));
        Assert.assertEquals(user.getEmail(), (String)params.get("userEmail2"));
        Assert.assertEquals(user.getLogin(), (String)params.get("userLogin2"));

    }

    @AfterTest
    public void deleteUser() throws JSONException, SignatureException {

        Response response = UsersApi.deleteUser(client, api, (String)params.get("token"), (long)params.get("userID"));

        Assert.assertEquals(response.getStatus(), 200);

    }

    @AfterTest
    public void deleteUser2() throws JSONException, SignatureException {

        Response response = UsersApi.deleteUser(client, api, (String)params.get("token2"), (long)params.get("userID2"));

        Assert.assertEquals(response.getStatus(), 200);

    }

}
