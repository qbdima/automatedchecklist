package com.automatedChecklist.location.suite;

import com.google.gson.Gson;
import com.automatedChecklist.chat.chatmodule.ChatApi;
import com.automatedChecklist.location.locationmodule.LocationApi;
import com.automatedChecklist.location.model.LocationParameters;
import com.automatedChecklist.location.providers.NewLocationData;
import com.automatedChecklist.user.usermodule.UsersApi;
import com.utils.GlobalParams;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.*;

import javax.ws.rs.core.Response;
import java.security.SignatureException;
import java.util.List;


public class LocationSuite {

    static GlobalParams params = GlobalParams.INSTANCE;
    static String api;
    static String token;

    @AfterGroups(groups = { "BoshAndWebSocketTest" })
    public static void setUp(){
        api = (String)params.get("api_domain");
        token = (String)params.get("token");
    }

    @Test(groups = { "LocationsTest" }, dependsOnGroups = { "BoshAndWebSocketTest" }, dataProvider = "newLocationData",
            dataProviderClass = NewLocationData.class, timeOut = 10000, alwaysRun = true)
    public void createLocation(JSONObject obj) throws JSONException {

        Response response = LocationApi.createLocation(params.getClient(), api, token, obj);

        LocationParameters.Location location = new Gson().fromJson(response.readEntity(String.class), LocationParameters.Location.class);

        Assert.assertEquals(location.getGeo_datum().getLatitude(), obj.getJSONObject("geo_data").get("latitude"));
        Assert.assertEquals(location.getGeo_datum().getLongitude(), obj.getJSONObject("geo_data").get("longitude"));
        Assert.assertEquals(response.getStatus(), 201);
    }

    @Test(groups = { "LocationsTest" }, dependsOnMethods = {"createLocation"}, timeOut = 10000, alwaysRun = true)
    public void getLocations() throws JSONException {
        Response response = LocationApi.getLocations(params.getClient(), api, token);

        LocationParameters locationParameters = new Gson().fromJson(response.readEntity(String.class),
                LocationParameters.class);
        List<LocationParameters.Location> locations = locationParameters.getItems();

        Assert.assertEquals(response.getStatus(), 200);
        Assert.assertNotEquals(locations.size(), 0);
    }

    @Test(groups = { "LocationsTest" }, dependsOnMethods = {"getLocations"}, timeOut = 10000, alwaysRun = true)
    public void deleteDialog() throws JSONException {
        Response response = ChatApi.deleteDialog(params.getClient(), api, token, (String) params.get("dialogID"));

        Assert.assertEquals(response.getStatus(), 200);
    }

//    @Test(groups = { "LocationsTest" }, dependsOnMethods = {"deleteDialog"}, timeOut = 10000)
//    public void deleteUser() throws JSONException, SignatureException {
//
//        Response response = UsersApi.deleteUser(params.getClient(), api, token, (long) params.get("userID"));
//
//        Assert.assertEquals(response.getStatus(), 200);
//
//    }

    @Test(groups = { "LocationsTest" }, dependsOnMethods = {"deleteDialog"}, timeOut = 10000, alwaysRun = true)
    public void deleteUser2() throws JSONException, SignatureException {

        Response response = UsersApi.deleteUser(params.getClient(), api, (String)params.get("token2"), (long)params.get("userID2"));

        Assert.assertEquals(response.getStatus(), 200);

    }

}
