package com.automatedChecklist.location.providers;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.testng.annotations.DataProvider;

public class NewLocationData {

    @DataProvider(name = "newLocationData")
    public static Object[][] newLocationData() throws JSONException {

        JSONObject obj = new JSONObject();
        JSONObject geo = new JSONObject();
        geo.put("latitude", 12.0);
        geo.put("longitude", 45.0);
        geo.put("status", "Hello world");

        obj.put("geo_data", geo);

        return new Object[][]{{obj}};
    }

}
