package com.automatedChecklist.location.locationmodule;

import com.utils.RESPONSE_TYPE;
import com.utils.Request;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class LocationApi {

    public static Response createLocation(Client client, String api, String token, JSONObject obj) throws JSONException {

        String path = "geodata.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.POST, null, Entity.entity(obj.toString(), MediaType.APPLICATION_JSON));

        return response;
    }

    public static Response getLocations(Client client, String api, String token) throws JSONException {

        String path = "geodata/find.json";

        Response response = Request.getResponse(Request.getTarget(client, api, path), MediaType.APPLICATION_JSON_TYPE, token,
                RESPONSE_TYPE.GET, null, null);

        return response;
    }

}
