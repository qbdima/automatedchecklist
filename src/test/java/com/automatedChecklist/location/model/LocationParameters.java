package com.automatedChecklist.location.model;

import java.util.List;

public class LocationParameters {

    private int current_page;
    private int per_page;
    private int total_entries;
    private List<Location> items;

    public int getCurrent_page() {
        return current_page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getTotal_entries() {
        return total_entries;
    }

    public List<Location> getItems() {
        return items;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public void setItems(List<Location> items) {
        this.items = items;
    }

    public void setTotal_entries(int total_entries) {
        this.total_entries = total_entries;
    }


    public static class Location {

        private Geo_datum geo_datum;

        public Geo_datum getGeo_datum() {
            return geo_datum;
        }

        public void setGeo_datum(Geo_datum geo_datum) {
            this.geo_datum = geo_datum;
        }

        public class Geo_datum{
            private int application_id;
            private String created_at;
            private int id;
            private double latitude;
            private double longitude;
            private String status;
            private String updated_at;
            private int user_id;

            public int getApplication_id() {
                return application_id;
            }

            public int getId() {
                return id;
            }

            public int getUser_id() {
                return user_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public double getLatitude() {
                return latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public String getStatus() {
                return status;
            }

            public String getUpdated_at() {
                return updated_at;
            }


            public void setApplication_id(int application_id) {
                this.application_id = application_id;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setLatitude(float latitude) {
                this.latitude = latitude;
            }

            public void setLongitude(float longitude) {
                this.longitude = longitude;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }
        }


    }
}
