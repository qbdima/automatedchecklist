package com.adminPanelTests;


import com.adminPanelPages.CustomPage;
import com.adminPanelPages.OverviewPage;
import com.utils.Consts;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Delete record test")
public class DeleteRecordTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"editRecord"})
    public void setUp() {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        custom = PageFactory.initElements(driver,CustomPage.class);
        custom.sleep();
        custom.chooseClass(Consts.CLASS_NAME);
    }
    @Description("Delete last record in the record list")
    @Test(groups = {"deleteRecord"}, dependsOnGroups = "editRecord", alwaysRun = true)
    public void deleteRecordTest(){
        custom.sleep();
        custom.choose100("100");
        int count = custom.countRecords();
        custom.removeRecord();
        custom.sleep();
        Assert.assertEquals(custom.countRecords(), count - 1);
    }

}