package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Delete chat test")
public class DeleteChatTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"chat"})
    public void setUp() {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickChat();

    }
    @Description("Delete last chat in the chats list")
    @Test(dependsOnGroups = {"chat"}, groups = {"deleteChat"}, alwaysRun = true)
    public void deleteChatTest(){
        chat.choose100("100");
        chat.sleep();
        int count = chat.countChats();
        chat.removeChat();
        chat.sleep();
        Assert.assertEquals(chat.countChats(), count - 1);
    }
}