package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Delete user test")
public class DeleteUserTest extends TestBase{
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"searchUser"})
    public void setUp() {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickUsers();
    }
    @Description("Delete user which was created in the create user test")
    @Test(groups = {"deleteUser"}, dependsOnGroups = {"searchUser"}, alwaysRun = true)
    public void deleteUserTest(){
        int count = user.countUsers();
        user.deleteUser();
        user.clickRemoveSelected();
        user.sleep();
        Assert.assertTrue(user.getSuccessMessage().contains("Success"));
        Assert.assertEquals(user.countUsers(), count - 1);
    }

}


