package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Search users test")
public class SearchUsersTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"editUser"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickUsers();
    }
    @Description("Search users which contains 'test'")
    @Test(groups = {"searchUser"}, dependsOnGroups = {"editUser"}, alwaysRun = true)
    public void searchUserTest(){
        user.choose100("100");
        user.sleep();
        int usersCount = user.searchUsers();
        user.enterText("test");
        user.sleep();
        Assert.assertEquals(user.searchUsers(), usersCount);
    }

}