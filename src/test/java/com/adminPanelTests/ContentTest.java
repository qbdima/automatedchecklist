package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
//import com.utils.CopyResource;
import com.utils.Consts;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

import java.io.File;

@Description("Content test")
public class ContentTest extends TestBase {
    static Params params = Params.INSTANCE;
    File file = new File(Consts.IMAGE_PATH);

    @AfterGroups(groups = {"deleteUser"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickContent();
    }
    @Description("Upload content")
    @Test(dependsOnGroups = {"deleteUser"}, groups = {"content"},alwaysRun = true)
    public void addContentTest(){
        content.uploadFile(file.getAbsolutePath());
        content.sleep();
        Assert.assertTrue(content.getSuccessMessage().contains("Success"));
    }
}
