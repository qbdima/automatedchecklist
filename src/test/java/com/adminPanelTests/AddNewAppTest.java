package com.adminPanelTests;

import com.pagesForConfig.HomePage;
//import com.utils.CopyResource;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.utils.Consts;
import ru.yandex.qatools.allure.annotations.Description;

import java.io.File;

@Description("Tests for add new app")
public class AddNewAppTest extends TestBase {
    static Params params = Params.INSTANCE;
    File file = new File(Consts.IMAGE_PATH);

    @AfterGroups(groups = {"export"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        e_driver.get(Consts.ADMIN_PANEL);
        home = PageFactory.initElements(driver, HomePage.class);
        home.addApp();
        home.sleep();
    }

    @Description("Enter all valid data")
    @Test (dependsOnGroups = {"export"}, groups = {"newApp"}, priority = 5, alwaysRun = true)
    public void createNewApp(){
        newApp.newAppAllFields(file.getAbsolutePath(), Consts.NEW_APP_NAME, "http://grggregergre.com", "ewfefewfew",
                "Fun", "test", "234234534","345454353","456546464", "4654574");
        newApp.sleep();
        Assert.assertTrue(overview.getAppName().contains(Consts.NEW_APP_NAME));
        overview.sleep();
        overview.clickRemoveApp();
        overview.sleep();
        Assert.assertTrue(home.getTitle().contains("Application"));
    }

   /* @AfterClass
    public void exit(){
        driver.close();
        driver.quit();
    }*/
}
