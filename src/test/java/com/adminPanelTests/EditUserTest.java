package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.adminPanelPages.UsersPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Tests for edit user")
public class EditUserTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"createUsers"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickUsers();
        user = PageFactory.initElements(driver, UsersPage.class);
        user.choose100("100");
        user.sleep();
        user.editUser();
    }
    @AfterMethod
    public void after(){
        users.refreshPage();
    }

    @Description("Update user full name")
    @Test( groups = {"editUser"}, dependsOnGroups = {"createUsers"}, priority=1, alwaysRun = true)
    public  void updateUserName(){
        editUser.enterName("autotest1");
        editUser.clickSaveButton();
        editUser.sleep();
        Assert.assertTrue(editUser.getSuccessMessage().contains("Success"));
    }

    @Description("Update user phone")
    @Test (groups = {"editUser"}, dependsOnGroups = {"createUsers"}, priority= 2, alwaysRun = true)
    public  void updateUserPhone(){
        editUser.enterPhone("876666666");
        editUser.clickSaveButton();
        editUser.sleep();
        Assert.assertTrue(editUser.getSuccessMessage().contains("Success"));
    }
    @Description("Update user website")
    @Test ( groups = {"editUser"}, dependsOnGroups = {"createUsers"}, priority= 3, alwaysRun = true)
    public  void updateUserWebsite(){
        editUser.enterWebsite("http://google.com.ua");
        editUser.clickSaveButton();
        editUser.sleep();
        Assert.assertTrue(editUser.getSuccessMessage().contains("Success"));
    }
}