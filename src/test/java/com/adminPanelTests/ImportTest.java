package com.adminPanelTests;

import com.adminPanelPages.CustomPage;
import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
//import com.utils.CopyResource;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import com.utils.Consts;
import ru.yandex.qatools.allure.annotations.Description;

import java.io.File;

@Description("Import tests")
public class ImportTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"deleteRecord"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickCustom();
        custom = PageFactory.initElements(driver, CustomPage.class);
        custom.clickImport();
    }
    @Description("Import file test")
    @Test(dependsOnGroups = {"deleteRecord"}, groups = {"import"}, alwaysRun = true)
    public void importTest(){
        File file = new File(Consts.JSON_FILE_PATH);
        importFile.importFile(Consts.CLASS_NAME, file.getAbsolutePath());
        importFile.sleep();
        Assert.assertTrue(importFile.getClassName().contains(Consts.CLASS_NAME));

    }

}