package com.adminPanelTests;

import org.openqa.selenium.WebDriver;

public enum Params {

    INSTANCE;

    private WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
