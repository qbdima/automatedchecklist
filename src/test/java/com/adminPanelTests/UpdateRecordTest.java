package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Update record test")
public class UpdateRecordTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"addRecord"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickCustom();
    }
    @Description("Update last record in the record list")
    @Test(dependsOnGroups = {"addRecord"}, groups = {"editRecord"}, alwaysRun = true)
    public void updateRecordTest(){
        custom.clickLastRecord();
        updateRecord.sleep();
        updateRecord.updateRecord("test", "test test");
        updateRecord.sleep();
        Assert.assertTrue(custom.getRecordName().contains("test"));
    }

}