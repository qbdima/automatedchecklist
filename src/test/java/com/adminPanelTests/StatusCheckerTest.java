package com.adminPanelTests;


import com.adminPanelPages.ChatPage;
import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.AddClass;
import com.pagesForConfig.HomePage;
import com.pagesForConfig.Users;
import com.utils.StatusChecker;
import com.utils.Consts;
import com.utils.GlobalParams;
import com.utils.WebEventListener;
import org.codehaus.jettison.json.JSONException;

import org.codehaus.jettison.json.JSONObject;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

import java.security.SignatureException;
import java.util.HashMap;

public class StatusCheckerTest extends TestBase{
    static Params params = Params.INSTANCE;
    static GlobalParams checkerParams = GlobalParams.INSTANCE;

    final private static String password = "password1234";

    @AfterGroups(groups = {"newApp"})
    public void setUp () {

        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        home = PageFactory.initElements(driver, HomePage.class);
    }

    @Description("Create StatusChecker app and set credentials to hashmap")
    @Test(dependsOnGroups = {"newApp"},priority = 1, alwaysRun = true, groups = {"checker"})
    public void createCheckerApp(){
        home.checkStatusChecker();
        credentials.setID();
        checkerParams.put("status_app_id", credentials.getID());
        checkerParams.put("status_auth_key", credentials.getAuthKey());
        checkerParams.put("status_auth_secret", credentials.getAuthSecret());
        Assert.assertNotNull(checkerParams.get("status_app_id"));
        Assert.assertNotNull(checkerParams.get("status_auth_key"));
        Assert.assertNotNull(checkerParams.get("status_auth_secret"));
    }
    @Description("Click Setting tab and check checkboxes")
    @Test(dependsOnGroups = {"newApp"}, priority = 2, alwaysRun = true, groups = {"checker"})
    public void checkSettingTab(){
        credentials.checkSettingsTab();
        // Assert.assertTrue();
    }
    @Description("Create StatusChecker class")
    @Test(dependsOnGroups = {"newApp"},priority = 3, alwaysRun = true, groups = {"checker"})
    public void createCustomClass(){
        String  appId = (String)checkerParams.get("status_app_id");
        String customUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/custom";
        e_driver.get(customUrl);
        AddClass customPage= PageFactory.initElements(driver,AddClass.class);
        customPage.statusCheckerClass();
        customPage.sleep();
        Assert.assertEquals(customPage.checkClass(Consts.STATUSCHECKER_CLASS_NAME), true);
    }

    @Description("Create users")
    @Test(dependsOnGroups = {"newApp"},priority = 4, alwaysRun = true, groups = {"checker"})
    public void createUsers(){
        String  appId = (String)checkerParams.get("status_app_id");
        String usersUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/users";
        e_driver.get(usersUrl);
        Users users = PageFactory.initElements(driver,Users.class);
        users.createCheckerUser("XMPPRecipient_", password, password, "XMPPMaster_", password, password, "XMPPSender_", password, password);
        users.sleep();
        checkerParams.put("status_sender", users.getXMPPSender());
        checkerParams.put("status_recipient", users.getXMPPRecipient());
        checkerParams.put("status_master", users.getXMPPMaster());
    }

    @Test(dependsOnGroups = {"newApp"},priority = 5, alwaysRun = true,groups = {"checker"})
    public void print(){
        System.out.println("\"" + "status_app_id" + "\"" + ": " + (String)checkerParams.get("status_app_id") + ",");
        System.out.println("\"" + "status_auth_key" + "\"" + ": " + "\"" + (String)checkerParams.get("status_auth_key") + "\"" + ",");
        System.out.println("\"" + "status_auth_secret" + "\"" + ": " + "\"" + (String)checkerParams.get("status_auth_secret") + "\"" + ",");

        System.out.println("\"" + "XMPPRecipient_id" + "\"" + ": " + ((HashMap)checkerParams.get("status_recipient")).get("jid") + ",");
        System.out.println("\"" + "XMPPRecipient_login" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_recipient")).get("login") + "\"" + ",");
        System.out.println("\"" + "XMPPRecipient_password" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_recipient")).get("password") + "\"" + ",");

        System.out.println("\"" + "XMPPMaster_id" + "\"" + ": " + ((HashMap)checkerParams.get("status_master")).get("jid") + ",");
        System.out.println("\"" + "XMPPMaster_login" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_master")).get("login") + "\"" + ",");
        System.out.println("\"" + "XMPPMaster_password" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_master")).get("password") + "\"" + ",");

        System.out.println("\"" + "XMPPSender_id" + "\"" + ": " + ((HashMap)checkerParams.get("status_sender")).get("jid") + ",");
        System.out.println("\"" + "XMPPSender_login" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_sender")).get("login") + "\"" + ",");
        System.out.println("\"" + "XMPPSender_password" + "\"" + ": " + "\"" + ((HashMap)checkerParams.get("status_sender")).get("password") + "\"" + ",");
    }

    @Description("Send create status request")
    @Test(dependsOnGroups = {"newApp"},priority = 6, alwaysRun = true, groups = {"checker"})
    public void createStatus(){
        JSONObject obj = new JSONObject();
        HashMap<String, String> admin = new HashMap<>();
        try {
            obj.put("name","iegor");
            obj.put("appId",(String)checkerParams.get("status_app_id"));
            obj.put("authSecret",(String)checkerParams.get("status_auth_secret"));
            obj.put("authKey",(String)checkerParams.get("status_auth_key"));
            obj.put("api",(String)checkerParams.get("api_domain"));
            obj.put("chat",(String)checkerParams.get("chat_domain"));
            obj.put("class_name","StatusChecker");
            obj.put("field_name","StatusChecker");
            obj.put("sender", (HashMap)checkerParams.get("status_sender"));
            obj.put("recipient", (HashMap)checkerParams.get("status_recipient"));
            obj.put("master", (HashMap)checkerParams.get("status_master"));
            admin.put("password",(String)checkerParams.get("admin_password"));
            admin.put("url",Consts.ADMIN_PANEL);
            admin.put( "login",(String)checkerParams.get("admin_login"));

            obj.put("admin", admin);

            obj.put("location", "Brazil");
        }catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(StatusChecker.create(obj));
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public void exit(){
        driver.close();
        driver.quit();
        System.exit(0);
    }
}
