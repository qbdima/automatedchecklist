package com.adminPanelTests;

import com.adminPanelPages.ChatPage;
import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;


@Description("Tests for add new app")
public class ChatTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"login"})
    public void setUp () {

        driver = params.getDriver();
       e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickChat();
        chat = PageFactory.initElements(driver, ChatPage.class);
        chat.newChatPopUpOpen();
    }

    @AfterMethod
    public void refresh(){chat.refreshPage();}

    @Description("Create public chat with photo id")
    @Test(dependsOnGroups = {"login"}, groups = {"chat"},alwaysRun = true)
    public void addChatWithPhotoID() {
        chat.CreatePublicChat("Public with photo id","24");
        chat.sleep();
        Assert.assertTrue(chat.getSuccessMessage().contains("Dialog has been successfully"));

    }
    @Description("Create group chat with all fields")
    @Test(dependsOnGroups = {"login"}, groups = {"chat"},alwaysRun = true)
    public void  addGroupChat() {
        chat.CreateGroupChat("Public with photo id","24","27,","28,","14");
        chat.sleep();
        Assert.assertTrue(chat.getSuccessMessage().contains("Dialog has been successfully"));
    }
}
