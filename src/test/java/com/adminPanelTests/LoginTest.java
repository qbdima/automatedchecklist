
package com.adminPanelTests;

import com.utils.GlobalParams;
import com.utils.WebEventListener;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.utils.Consts;
import ru.yandex.qatools.allure.annotations.Description;

import java.util.concurrent.TimeUnit;

@Description("Login tests")
public class LoginTest extends TestBase{

    static Params params = Params.INSTANCE;
    static GlobalParams params1 = GlobalParams.INSTANCE;

    @AfterGroups(groups = { "LocationsTest" })
    public void setUp () {
        driver = new FirefoxDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);
        e_driver.get(Consts.ADMIN_PANEL);
        e_driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Description("Enter correct data")
    @Test (groups = {"login"}, priority = 4, dependsOnGroups ={ "LocationsTest" }, alwaysRun = true)
    public void loginTest() {
        login.loginAdmin((String)params1.get("admin_login"),(String)params1.get("admin_password")); // (String)params.
        login.sleep();
        Assert.assertTrue(home.getTitle().contains("Application"));
        params.setDriver(e_driver);
    }
}