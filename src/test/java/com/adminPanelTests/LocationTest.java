package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Location tests")
public class LocationTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"deleteContent"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickLocation();
    }
    @Description("Check location is present")
    @Test(dependsOnGroups = {"deleteContent"}, groups = {"location"}, alwaysRun = true)
    public void locationTest(){
        //location.locationIsCreated();
        Assert.assertTrue(location.locationIsCreated().contains("world"));
    }
}
