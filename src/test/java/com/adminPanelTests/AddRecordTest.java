package com.adminPanelTests;

import com.adminPanelPages.CustomPage;
import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.Consts;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Tests for add record")
public class AddRecordTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"push"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickCustom();
        custom = PageFactory.initElements(driver, CustomPage.class);
        custom.sleep();
        custom.chooseClass(Consts.CLASS_NAME);
    }
    @Description("Add record test")
    @Test(dependsOnGroups = {"push"}, groups = {"addRecord"}, alwaysRun = true)
    public void addRecordTest(){
        custom.sleep();
        custom.choose100("100");
        int count = custom.countRecords();
        custom.addRecord();
        record.createRecord("111", "test");
        record.sleep();
        Assert.assertEquals(custom.countRecords(), count + 1);
    }
    @Description("Add one more record")
    @Test(dependsOnGroups = {"push"}, groups = {"addRecord"}, alwaysRun = true)
    public void addRecordTest2(){
        custom.sleep();
        custom.choose100("100");
        int count = custom.countRecords();
        custom.addRecord();
        record.createRecord("222", "qwerty");
        record.sleep();
        Assert.assertEquals(custom.countRecords(), count + 1);
    }

}