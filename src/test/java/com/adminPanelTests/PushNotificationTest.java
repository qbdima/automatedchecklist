package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Push notification tests")
public class PushNotificationTest extends TestBase {

    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"location"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickPush();
    }
    @AfterMethod
    public void refresh() {
        push.refreshPage();
    }

    @Description("Send push notification")
    @Test(dependsOnGroups = {"location"}, groups = {"push"}, alwaysRun = true)
    public void sendPushTest(){
        push.sendIosPush("test");
        Assert.assertTrue(push.getSuccesMesage().contains("Success"));
    }

}
