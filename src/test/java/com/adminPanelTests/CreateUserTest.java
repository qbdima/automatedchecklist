package com.adminPanelTests;


import com.adminPanelPages.OverviewPage;
import com.adminPanelPages.UsersPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.utils.Consts;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Tests for create user")
public class CreateUserTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"deleteChat"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver,HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickUsers();
        user = PageFactory.initElements(driver, UsersPage.class);
        user.clickAddUser();
    }

    @Description("Create user with obligatory fields")
    @Test( groups = {"createUsers"}, dependsOnGroups = {"deleteChat"},  priority=1,alwaysRun = true)
    public void createUser(){
        createUser.createUser(" ", Consts.USER_LOGIN, Consts.USER_PASSWORD,Consts.USER_PASSWORD);
        createUser.sleep();
        Assert.assertTrue(createUser.getSuccessMessage().contains("Success"));
    }

}
