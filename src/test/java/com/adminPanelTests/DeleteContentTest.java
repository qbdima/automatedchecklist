package com.adminPanelTests;

import com.adminPanelPages.OverviewPage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Delete content test")
public class DeleteContentTest extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"content"})
    public void setUp() {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickContent();
    }
    @Description("Delete last content in the content list")
    @Test(groups = {"deleteContent"}, dependsOnGroups = {"content"}, alwaysRun = true)
    public void deleteContentTest(){
        content.choose100("100");
        content.sleep();
        int count = content.countContent();
        content.removeContent();
        content.sleep();
        Assert.assertEquals(content.countContent(), count - 1);
    }
}
