package com.adminPanelTests;

import com.adminPanelPages.*;
import com.adminPanelPages.UsersPage;
import com.pagesForConfig.*;
import com.utils.WebEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class TestBase {

    //    static Params params = Params.INSTANCE;
//    WebDriver driver = params.getDriver();
    public WebDriver driver;
    public EventFiringWebDriver e_driver;
    public WebEventListener eventListener;
    public Login login;
    public HomePage home;
    public AddNewAppPage newApp;
    public OverviewPage overview;
    public UsersPage user;
    public CreateUserPage createUser;
    public ContentPage content;
    public EditUserPage editUser;
    public CustomPage custom;
    public ExportPage export;
    public JsonContentPage jsonContent;
    public CsvContentPage csvContent;
    public ImportPage importFile;
    public ChatPage chat;
    public AddRecordPage record;
    public UpdateRecordPage updateRecord;
    public LocationPage location;
    public PushNotificationPage push;
    public Users users;
    public  AlertSettings alert;
    public AddClass customPage;
    AppCredentials credentials;

    @BeforeClass
    public void beforeAllClasses(){

    }
    @BeforeMethod
    public void beforeAllTests() {

        login = PageFactory.initElements(driver, Login.class);
        home = PageFactory.initElements(driver, HomePage.class);
        newApp = PageFactory.initElements(driver, AddNewAppPage.class);
        overview = PageFactory.initElements(driver,OverviewPage.class);
        user = PageFactory.initElements(driver,UsersPage.class);
        createUser = PageFactory.initElements(driver, CreateUserPage.class);
        content = PageFactory.initElements(driver,ContentPage.class);
        editUser = PageFactory.initElements(driver,EditUserPage.class);
        custom = PageFactory.initElements(driver, CustomPage.class);
        export = PageFactory.initElements(driver, ExportPage.class);
        jsonContent = PageFactory.initElements(driver, JsonContentPage.class);
        csvContent = PageFactory.initElements(driver, CsvContentPage.class);
        importFile = PageFactory.initElements(driver, ImportPage.class);
        chat = PageFactory.initElements(driver, ChatPage.class);
        record = PageFactory.initElements(driver, AddRecordPage.class);
        updateRecord = PageFactory.initElements(driver, UpdateRecordPage.class);
        location = PageFactory.initElements(driver, LocationPage.class);
        push = PageFactory.initElements(driver,PushNotificationPage.class);
        users = PageFactory.initElements(driver, Users.class);
        credentials = PageFactory.initElements(driver,AppCredentials.class);

    }



}
