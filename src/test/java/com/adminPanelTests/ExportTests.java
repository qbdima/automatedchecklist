package com.adminPanelTests;

import com.adminPanelPages.CustomPage;
import com.adminPanelPages.ExportPage;
import com.adminPanelPages.OverviewPage;
import com.pagesForConfig.HomePage;
import com.utils.WebEventListener;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.Test;
import com.utils.Consts;
import ru.yandex.qatools.allure.annotations.Description;

@Description("Export tests")
public class ExportTests extends TestBase {
    static Params params = Params.INSTANCE;

    @AfterGroups(groups = {"import"})
    public void setUp () {
        driver = params.getDriver();
        e_driver = new EventFiringWebDriver(driver);
        eventListener = new WebEventListener();
        e_driver.register(eventListener);

        home = PageFactory.initElements(driver, HomePage.class);
        home.chooseApp();
        overview = PageFactory.initElements(driver, OverviewPage.class);
        overview.clickCustom();
        custom = PageFactory.initElements(driver, CustomPage.class);
        custom.clickExport();
        export = PageFactory.initElements(driver, ExportPage.class);
        export.selectClass(Consts.CLASS_NAME);
    }
    @Description("Export to json test")
    @Test(dependsOnGroups = "import", groups = {"export"}, alwaysRun = true)
    public void exportJsonTest(){
        export.clickJson();
        export.sleep();
        export.refreshPage();
        export.clickFile();
        Assert.assertTrue(jsonContent.getJsonContent().contains("created_at"));
        export.back();
    }
    @Description("Export to csv test")
    @Test(dependsOnGroups = "import", groups = {"export"}, enabled = false)
    public void exportCsvTest(){
        export.clickCsv();
        export.sleep();
        export.refreshPage();
        export.clickFile();
        Assert.assertTrue(csvContent.getCsvContent().contains("created_at"));
        export.back();
    }

}