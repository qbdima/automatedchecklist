package com.adminPanelPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.utils.Consts;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

public class UsersPage extends AbstractPage {
    public UsersPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // ищем кнопку add user
    @FindBy(xpath = "//*[@id='list']//div[4]//a")
    private WebElement addUser;
    // ищем ссылку remove selected
    @FindBy(css = "a.remove")
    private WebElement removeSelected;
    // ищем чекбокс select all
    @FindBy(xpath = "//*[@id='check-all']")
    private WebElement selectAll;
    //ищем вкладку settings
    @FindBy(xpath = "//*[@id='list']/div[1]/ul/li[2]/a")
    private WebElement settingsTab;
    //ищем сообщение об успешном удалении
    @FindBy(id = "form-messages")
    private WebElement successMessage;
    @FindBy(xpath = "//*[@id='users_list_filter']//input")
    private WebElement searchField;
    @FindBy (xpath = "//*[@id='users_list_length']/label/select")
    private WebElement recordsNumber;



    //кликаем на кнопку add user
    public void clickAddUser() {
        addUser.click();
    }

    // кликаем на remove selected
    public void clickRemoveSelected() {
        removeSelected.click();
        driver.switchTo().alert().accept();
    }
    // choose 100 records

    // send text to search field
    public void enterText(String text){
        searchField.sendKeys(text);
    }
    // получаем текст success сообщения
    public String getSuccessMessage() {
        return successMessage.getText();
    }

    //choose user for updating
    public void editUser() {
        List<WebElement> usersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getText().equals(Consts.USER_LOGIN)) {
                int k = i + 1;
                WebElement id = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span"));
                id.click();
            }
        }
    }
    //choose user for deleting
    public void deleteUser() {
        List<WebElement> usersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getText().equals(Consts.USER_LOGIN)) {
                int k = i + 1;
                WebElement checkbox = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[1]/input"));
                checkbox.click();
            }

        }
    }
    // count users with ID
    public int countUsers() {
        List<WebElement> listOfUsers = driver.findElements(By.cssSelector("tbody td:nth-child(2)"));
        return listOfUsers.size();
    }
    //search user
    public int searchUsers() {
        int userCount = 0;
        WebElement usersTable = driver.findElement(By.cssSelector("#users_list_wrapper tbody"));
        List<WebElement> allRows = usersTable.findElements(By.tagName("tr"));
        for (WebElement row : allRows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            for (WebElement cell : cells) {
                if (cell.getText().contains("test")) {
                    userCount++;
                }
            }
        }
        return userCount;

    }

    public void choose100(String records){
        Select select = new Select(recordsNumber);
        select.selectByValue(records);
    }

}