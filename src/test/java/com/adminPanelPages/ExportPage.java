package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ExportPage extends AbstractPage{
    public ExportPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "class_name")
    private WebElement chooseClass;
    @FindBy (id = "export_btn_csv")
    private WebElement exportCSV;
    @FindBy (id = "export_btn_json")
    private WebElement exportJSON;
    @FindBy (xpath = "//*[@id='export-table']/tbody/tr[1]/td[4]/a")
    private WebElement findFile;


    public void selectClass(String className){
        Select select = new Select(chooseClass);
        select.selectByVisibleText(className);
    }
    public void exportFiles(){
        exportCSV.click();
        exportJSON.click();
    }
    public void clickCsv(){
        exportCSV.click();
    }
    public void clickJson(){
        exportJSON.click();
    }

    public void clickFile(){
        findFile.click();
    }
}
