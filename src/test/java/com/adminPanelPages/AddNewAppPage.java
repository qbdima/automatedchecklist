package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class AddNewAppPage extends AbstractPage{
        public AddNewAppPage(WebDriver driver) {
            super(driver);
            PageFactory.initElements(driver, this);
        }
        @FindBy(how = How.ID, using = "avatar_file")
        private WebElement appAvatar;
        @FindBy(how = How.ID, using = "application_title")
        private WebElement appTitle;
        @FindBy(how = How.ID, using = "application_url")
        private WebElement appWebsite;
        @FindBy(how = How.ID, using = "application_app_store_link")
        private WebElement appStorelink;
        @FindBy (id = "application_app_type") //реархитектура
        private WebElement appTypeRearch;
        @FindBy(id = "application_type")
        private WebElement appType;
        @FindBy (how = How.ID, using = "application_description")
        private WebElement appDescription;
        @FindBy (how = How.ID, using = "facebook_key")
        private WebElement appFacebookID;
        @FindBy (how = How.ID, using = "facebook_secret")
        private WebElement appFacebookSecret;
        @FindBy (how = How.ID, using = "twitter_key")
        private WebElement appTwitterKey;
        @FindBy (how = How.ID, using = "twitter_secret")
        private WebElement appTwitterSecret;
        @FindBy (how = How.CSS, using = "div.col-md-10 > button[name=\"button\"]")
        private WebElement addButton;
        @FindBy(how = How.CSS, using ="div.btn-toolbar > button[name=\"button\"]" )
        private WebElement addButtonProvider;
        @FindBy (how = How.XPATH, using = "//*[@id='form-errors']")
        private  WebElement errorMessage;

        @FindBy (how = How.XPATH, using = "//*[@id='form-add-app']/div[3]/div/div[2]/div/label")
        private  WebElement titleValidation;
        @FindBy (how = How.XPATH, using = "//*[@id='form-add-app']/div[3]/div/div[3]/div/label")
        private  WebElement websiteValidation;


        public void enterAvatar(String avatar) {
            appAvatar.sendKeys(avatar);
        }
        public void enterTitle(String title) {
            appTitle.sendKeys(title);
        }
        public void enterWebsite(String website){
            appWebsite.sendKeys(website);
        }
        public void enterAppStoreLink (String storeLink){
            appStorelink.sendKeys(storeLink);
        }
        public void chooseType(String type){
            try {
                appType.isDisplayed();
            } catch (Exception e) {
                appType = appTypeRearch;
            }
            Select select = new Select(appType);
            select.selectByVisibleText(type);
        }
        public void enterDescription(String description){
            appDescription.sendKeys(description);
        }
        public void enterFacebookID(String facebookID){
            appFacebookID.sendKeys(facebookID);
        }
        public void enterFacebookSecret(String facebookSecret){
            appFacebookSecret.sendKeys(facebookSecret);
        }
        public void enterTwitterKey(String twitterKey){
            appTwitterKey.sendKeys(twitterKey);
        }
        public void enterTwitterSecret(String twitterSecret){
            appTwitterSecret.sendKeys(twitterSecret);
        }
        public void clickAddButton(){
            try {
                addButton.isDisplayed();
            } catch (Exception e) {
                addButton = addButtonProvider;
            }
            addButton.click();
        }
        public String getErrorMessage(){
            return errorMessage.getText();
        }
        public String getTitleValidation(){
            return  titleValidation.getText();
        }
        public String getWebsiteValidation(){
            return  websiteValidation.getText();
        }
        //fill only obligatory fields
        public void addNewApp(String avatar, String title, String type){
            enterAvatar(avatar);
            enterTitle(title);
            chooseType(type);
            clickAddButton();
        }
        // fill all fields
        public void newAppAllFields(String avatar, String title,String website,String storeLink , String type, String description, String facebookID, String facebookSecret, String twitterKey, String twitterSecret ){
            enterAvatar(avatar);
            enterTitle(title);
            enterWebsite(website);
            enterAppStoreLink(storeLink);
            chooseType(type);
            enterDescription(description);
            enterFacebookID(facebookID);
            enterFacebookSecret(facebookSecret);
            enterTwitterKey(twitterKey);
            enterTwitterSecret(twitterSecret);
            clickAddButton();
        }



}
