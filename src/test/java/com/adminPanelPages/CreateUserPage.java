package com.adminPanelPages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateUserPage extends AbstractPage {
    public CreateUserPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // ищем поле full name
    @FindBy(id = "user_full_name")
    private WebElement userFullName;
    // ищем поле user email
    @FindBy(id = "user_email")
    private WebElement userEmail;
    // ищем поле user login
    @FindBy(id= "user_login")
    private WebElement userLogin;
    // ищем поле user password
    @FindBy(id = "user_password")
    private WebElement userPassword;
    // ищем поле password confirm
    @FindBy(id = "user_password_confirmation")
    private WebElement confirmPassword;
    //  ищем поле  user phone
    @FindBy(id = "user_phone")
    private WebElement userPhone;
    // ищем поле  user website
    @FindBy(id = "user_website")
    private WebElement userWebsite;
    // ищем поле blob id
    @FindBy(id = "user_blob_id")
    private WebElement userBlobId;
    // ищем поле  external id
    @FindBy(id = "user_external_user_id")
    private WebElement userExternalId;
    // ищем поле  facebook id
    @FindBy(id = "user_facebook_id")
    private WebElement userFacebookId;
    //  ищем поле  twitter id
    @FindBy(id = "user_twitter_id")
    private WebElement userTwitterId;
    // ищем кнопку add user
    @FindBy(xpath = "//*[@id='user_submit']")
    private WebElement addUserButton;
    // ищем поле для ввода тегов
    @FindBy(xpath = "//*[@id='tags_tagsinput']")
    private WebElement userTags;
    // ищем Success message
    @FindBy(xpath = "//*[@id='form-messages']")
    private WebElement successMessage;
    // ищем error message
    @FindBy(xpath= "//*[@id='form-errors']")
    private WebElement errorMessage;
    // ищем error сообщения для поля логин
    @FindBy (css = ".col-sm-9 label.error")
    private WebElement loginValidation;
    // ищем error сообщения для поля email
    @FindBy (css = ".col-sm-9 label.error")
    private WebElement emailValidation;
    // ищем error сообщения для поля password
    @FindBy (css = ".col-sm-9 label.error")
    private WebElement passwordValidation;
    //ищем error сообщения для поля password
    @FindBy (css = ".col-sm-9 label.error")
    private WebElement passwordConfirmValidation;

    public void enterName(String name) {
        userFullName.sendKeys(name);
    }

    public void enterEmail(String email) {
        userEmail.clear();
        userEmail.sendKeys(email);
    }

    public void enterLogin(String login) {
        userLogin.clear();
        userLogin.sendKeys(login);
    }

    public void enterPassword(String password) {

        userPassword.clear();
        userPassword.sendKeys(password);
    }

    public void confirmation(String passwordConfirm) {

        confirmPassword.clear();
        confirmPassword.sendKeys(passwordConfirm);
    }

    public void enterPhone(String phone) {
        userPhone.sendKeys(phone);
    }

    public void enterWebsite(String website) {
        userWebsite.sendKeys(website);
    }

    public void enterBlobId(String blobId) {
        userBlobId.sendKeys(blobId);
    }

    public void enterExternalId(String externalId) {
        userExternalId.sendKeys(externalId);
    }

    public void enterFacebookId(String facebookId) {
        userFacebookId.sendKeys(facebookId);
    }

    public void enterTwitterId(String twitterId) {
        userTwitterId.sendKeys(twitterId);
    }
    public void enterTags(String tags){userTags.sendKeys(tags);}
    public void clickAddUserButton(){addUserButton.click();}

    public String getSuccessMessage(){return successMessage.getText();}
    public String getErrorMessage(){return errorMessage.getText();}
    public String getLoginValidation(){return loginValidation.getText();}
    public String getEmailValidation(){return emailValidation.getText();}
    public String getPasswordValidation(){return passwordValidation.getText();}
    public String getPasswordConfirmValidation(){return passwordConfirmValidation.getText();}

    // заполняем только обязательные поля
    public void createUser (String email,String login,  String password, String passwordConfirm){
        enterEmail(email);
        enterLogin(login);
        enterPassword(password);
        confirmation(passwordConfirm);
        clickAddUserButton();
    }
    // создание юзера для апы StatusChecker
    public void createUser2 (String login,  String password, String passwordConfirm){
        enterLogin(login);
        enterPassword(password);
        confirmation(passwordConfirm);
        clickAddUserButton();
    }
    // заполняем все поля
    public void createUser2 (String fullName, String email,String login,  String password, String passwordConfirm, String phone, String website, String blobId, String externalId, String facebookId, String twitterId){
        enterName(fullName);
        enterEmail(email);
        enterLogin(login);
        enterPassword(password);
        confirmation(passwordConfirm);
        enterPhone(phone);
        enterWebsite(website);
        enterBlobId(blobId);
        enterExternalId(externalId);
        enterFacebookId(facebookId);
        enterTwitterId(twitterId);
        clickAddUserButton();
    }
}
