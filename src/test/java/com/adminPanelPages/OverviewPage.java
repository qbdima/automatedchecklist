package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class OverviewPage extends AbstractPage {
    public OverviewPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //ищем имя приложения в breadcrumbs
    @FindBy(css = "ul.breadcrumb li.selected")
    private WebElement selectedAppName;
    // find chat tab
    @FindBy (xpath = "//*[@id='app-list']/a[2]")
    private WebElement chat;
    //find content tab
    @FindBy (xpath = "//*[@id='app-list']/a[3]")
    private WebElement content;
    // find custom tab
    @FindBy (xpath = "//*[@id='app-list']/a[4]")
    private WebElement custom;
    // find location tab
    @FindBy (xpath = "//*[@id='app-list']/a[5]")
    private WebElement location;
    // find messages tab
    @FindBy (xpath = "//*[@id='app-list']/a[6]")
    private WebElement push;
    // find users tab
    @FindBy (xpath = "//*[@id='app-list']/a[7]")
    private WebElement users;
    // ищем кнопку remove
    @FindBy (css = "a.btn.btn-danger.pull-right")
    private WebElement remove;



    // получаем имя приложения
    public String getAppName(){
        return selectedAppName.getText();
    }
    //кликаем по табам
    public void clickChat(){
        chat.click();
    }
    public void clickContent(){
        content.click();
    }
    public void clickCustom(){
        custom.click();
    }
    public void clickLocation(){
        location.click();
    }
    public void clickPush(){
        push.click();
    }
    public void clickUsers(){
        users.click();
    }

    public void clickRemoveApp() {
        remove.click();
        driver.switchTo().alert().accept();
    }
}