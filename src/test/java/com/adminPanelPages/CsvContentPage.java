package com.adminPanelPages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CsvContentPage extends AbstractPage{

    public CsvContentPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(tagName = "pre")
    private WebElement content;

    public String getCsvContent(){
        return content.getText();
    }
}