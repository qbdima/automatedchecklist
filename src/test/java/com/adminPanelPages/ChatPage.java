package com.adminPanelPages;

import com.utils.Consts;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class ChatPage extends AbstractPage {
    public ChatPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath =  "//*[@id='dialogs_list_wrapper']/div[3]/input")
    private WebElement newDialogBut;
    @FindBy(xpath = "//*[@id='chat_dialog_name']")
    private WebElement nameField;
    @FindBy(xpath = "//*[@id='chat_dialog_photo']")
    private WebElement photoField;
    @FindBy(xpath=  "//*[@id='chat_dialog_type_1']")
    private WebElement publicRadioBut;
    @FindBy(xpath= "//*[@id='dialog_submit']")
    private WebElement createButton;
    @FindBy(xpath =  "//*[@id='chat_dialog_type_2']")
    private WebElement groupRadioBut;
    @FindBy(id= "chat_dialog_occupants_ids")
    private WebElement occupantsIDField;
    @FindBy (css = "div.content")
    private WebElement successMessage;
    @FindBy (xpath =  "//*[@id='update_form']/div[1]/div[2]/label")
    private WebElement errorName;
    @FindBy (xpath = "//*[@id='update_form']/div[6]/div[2]/label")
    private WebElement errorOccupants;
    @FindBy (xpath = "//*[@id='dialogs_list_last']/a")
    private WebElement lastButton;
    @FindBy (css = "tbody tr:last-child td:first-child input")
    private WebElement lastChat;
    @FindBy(xpath = "//*[@id='dialogs_list_wrapper']/div[8]/a")
    private WebElement removeSelected;

    @FindBy(xpath = "//*[@id='dialogs_list_length']/label/select")
    private WebElement recordsNumber;

    public void choose100(String records){
        Select select = new Select(recordsNumber);
        select.selectByValue(records);
    }

    public void newChatPopUpOpen() {
        newDialogBut.click();
    }

    public void enterName(String chatName) {
        nameField.sendKeys(chatName);
    }

    public void enterPhoto(String photo) {
        photoField.sendKeys(photo);
    }

    public void selectPublicChat() {
        publicRadioBut.click();
    }

    public void selectGroupChat() {
        groupRadioBut.click();
    }

    public void enterOccupantsIDs(String user1, String user2, String user3) {
        occupantsIDField.sendKeys(user1, user2, user3);
    }

    public void CreateChatBut() {
        createButton.click();
    }
    public String getSuccessMessage(){
        return successMessage.getText();}

    public String getErrorName(){
        return errorName.getText();
    }
    public String getErrorOccupants(){
        return errorOccupants.getText();
    }
    public void OpenLastPage (){
        lastButton.click();
    }


    public void CreatePublicChat(String chatName, String photo) {
        newChatPopUpOpen();
        enterName(chatName);
        enterPhoto(photo);
        selectPublicChat();
        CreateChatBut();
    }

    public void CreateGroupChat(String chatName, String photo, String user1, String user2, String user3) {
        newChatPopUpOpen();
        enterName(chatName);
        enterPhoto(photo);
        selectGroupChat();
        enterOccupantsIDs(user1, user2, user3);
        CreateChatBut();
    }

    public int countChats(){
        List<WebElement> listOfChats= driver.findElements(By.cssSelector("tbody td:nth-child(2)"));
        return listOfChats.size();
    }

    public void removeChat() {
        lastChat.click();
        removeSelected.click();
        driver.switchTo().alert().accept();
    }

    //click alert tab
    public void openAlertTab(String appId) {
        String chat = Consts.ADMIN_PANEL +"apps/" + appId + "/service/chat/dialogs";
        driver.get(chat);
        sleep();
        WebElement alertTab = driver.findElement(By.xpath("//*[@id='history_dialogs']/div[1]//li[2]/a"));
        String getAlertText = alertTab.getText();
        if (getAlertText.equals("Alerts")){
            String alertUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/chat/alerts";
            driver.get(alertUrl);
        }else {
            String alertUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/chat/alert";
            driver.get(alertUrl);
        }
    }

}
