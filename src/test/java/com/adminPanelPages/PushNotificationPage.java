package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PushNotificationPage extends AbstractPage{
    public PushNotificationPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id='channels']//button[1]")
    private WebElement ios;
    @FindBy (xpath = "//*[@id='advanced_send_text']")
    private WebElement textarea;
    @FindBy (xpath = "//*[@id='sendPush']")
    private WebElement sendPushButton;
    @FindBy(xpath = "//*[@id='msg_3']")
    private WebElement successMessage;
    @FindBy (xpath = "//*[@id='msg_2']")
    private WebElement errorMessage;

    public void clickIos(){
        ios.click();
    }
    public void enterText(String text){
        textarea.sendKeys(text);
    }
    public void clickSend(){
        sendPushButton.click();
    }
    public String getSuccesMesage(){
        return successMessage.getText();
    }
    public String getErrorMessage(){
        return errorMessage.getText();
    }
    public void sendIosPush(String push){
        ios.click();
        textarea.sendKeys(push);
        sendPushButton.click();
    }
}
