package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class EditUserPage extends AbstractPage {
    public EditUserPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // ищем поле full name
    @FindBy(how = How.ID, using = "user_full_name")
    private WebElement userFullName;
    // ищем поле user email
    @FindBy(how = How.ID, using = "user_email")
    private WebElement userEmail;
    // ищем поле user login
    @FindBy(how = How.ID, using = "user_login")
    private WebElement userLogin;
    // ищем поле user password
    @FindBy(how = How.ID, using = "user_password")
    private WebElement userPassword;
    // ищем поле password confirm
    @FindBy(how = How.ID, using = "user_password_confirmation")
    private WebElement confirmPassword;
    //  ищем поле  user phone
    @FindBy(how = How.ID, using = "user_phone")
    private WebElement userPhone;
    // ищем поле  user website
    @FindBy(how = How.ID, using = "user_website")
    private WebElement userWebsite;
    // ищем поле blob id
    @FindBy(how = How.ID, using = "user_blob_id")
    private WebElement userBlobId;
    // ищем поле  external id
    @FindBy(how = How.ID, using = "user_external_user_id")
    private WebElement userExternalId;
    // ищем поле  facebook id
    @FindBy(how = How.ID, using = "user_facebook_id")
    private WebElement userFacebookId;
    //  ищем поле  twitter id
    @FindBy(how = How.ID, using = "user_twitter_id")
    private WebElement userTwitterId;
    // ищем кнопку add user
    @FindBy(how = How.XPATH, using = "//*[@id='user_submit']")
    private WebElement saveButton;
    // ищем поле для ввода тегов
    @FindBy(how = How.XPATH, using = "//*[@id='tags_tag']")
    private WebElement userTags;
    // ищем Success message
    @FindBy(how = How.XPATH, using = "//*[@id='form-results-success']")
    private WebElement successMessage;
    // ищем error message
    @FindBy(how = How.XPATH, using = "//*[@id='form-results-errors']")
    private WebElement errorMessage;
    // ищем error сообщения для поля логин
    @FindBy (how = How.CSS, using = ".col-sm-8 label.error")
    private WebElement loginValidation;
    // ищем error сообщения для поля email
    @FindBy (how = How.CSS, using = ".col-sm-8 label.error")
    private WebElement emailValidation;
    // ищем error сообщения для поля Facebook id
    @FindBy (how = How.CSS, using = ".col-sm-8 label.error")
    private WebElement facebookValidation;
    // ищем error сообщения для поля Facebook id
    @FindBy (how = How.CSS, using = ".col-sm-8 label.error")
    private WebElement twitterValidation;

    public void enterName(String name) {
        userFullName.clear();
        userFullName.sendKeys(name);
    }

    public void enterEmail(String email) {
        userEmail.clear();userEmail.sendKeys(email);
    }

    public void enterLogin(String login) {
        userLogin.clear();
        userLogin.sendKeys(login);
    }

    public void enterPassword(String password) {
        userPassword.clear();
        userPassword.sendKeys(password);
    }

    public void confirmation(String passwordConfirm) {
        confirmPassword.clear();
        confirmPassword.sendKeys(passwordConfirm);
    }

    public void enterPhone(String phone) {
        userPhone.clear();
        userPhone.sendKeys(phone);
    }

    public void enterWebsite(String website) {
        userWebsite.clear();
        userWebsite.sendKeys(website);
    }

    public void enterBlobId(String blobId) {
        userBlobId.clear();
        userBlobId.sendKeys(blobId);
    }

    public void enterExternalId(String externalId) {
        userExternalId.clear();
        userExternalId.sendKeys(externalId);
    }

    public void enterFacebookId(String facebookId) {
        userFacebookId.clear();
        userFacebookId.sendKeys(facebookId);
    }

    public void enterTwitterId(String twitterId) {
        userTwitterId.clear();
        userTwitterId.sendKeys(twitterId);
    }
    public void enterTags(String tags){userTags.sendKeys(tags);}
    public void clickSaveButton(){saveButton.click();}

    public String getSuccessMessage(){return successMessage.getText();}
    public String getErrorMessage(){return errorMessage.getText();}
    public String getEmailValidation(){return emailValidation.getText();}
    public String getLoginValidation(){return loginValidation.getText();}
    public String getFacebookValidation(){return facebookValidation.getText();}
    public String getTwitterValidation(){return twitterValidation.getText();}

}
