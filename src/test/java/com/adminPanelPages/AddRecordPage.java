package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class AddRecordPage extends AbstractPage {
    public AddRecordPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id='new_record__parent_id']")
    private WebElement parendIdField;
    @FindBy (xpath = "//*[@id='new_record_name']")
    private WebElement nameField;
    @FindBy(xpath = "//*[@id='new_record_description']")
    private WebElement descriptionField;
    @FindBy (xpath = "//*[@id='new_record_rating']")
    private WebElement ratingField;
    @FindBy(xpath = "//*[@id='record_add']/div[4]/input")
    private WebElement addRecordButton;

    public void enterParentId(String id){
        parendIdField.sendKeys(id);
    }
    public void enterName(String name){
        nameField.sendKeys(name);
    }
    public void enterDescription(String description){
        descriptionField.sendKeys(description);
    }
    public void enterRating(String rating){
        ratingField.sendKeys(rating);
    }
    public void clickAddRecord(){
        addRecordButton.click();
    }
    public void createRecord(String id, String description){
        enterParentId(id);
        enterDescription(description);
        clickAddRecord();
    }
}