package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpdateRecordPage extends AbstractPage {
    public UpdateRecordPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id='record__parent_id']")
    private WebElement parendIdField;
    @FindBy (xpath = "//*[@id='record_name']")
    private WebElement nameField;
    @FindBy(xpath = "//*[@id='record_description']")
    private WebElement descriptionField;
    @FindBy (xpath = "//*[@id='record_rating']")
    private WebElement ratingField;
    @FindBy(xpath = "//*[@id='record_edit']/div[4]/input")
    private WebElement updateButton;

    public void enterParentId(String id){
        parendIdField.sendKeys(id);
    }
    public void enterName(String name){
        nameField.sendKeys(name);
    }
    public void enterDescription(String description){
        descriptionField.sendKeys(description);
    }
    public void enterRating(String rating){
        ratingField.sendKeys(rating);
    }
    public void clickUpdateRecord(){
        updateButton.click();
    }
    public  void updateRecord(String name,String description){
        enterName(name);
        enterDescription(description);
        clickUpdateRecord();
    }
}
