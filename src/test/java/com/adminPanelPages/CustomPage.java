package com.adminPanelPages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.utils.Consts;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class CustomPage extends AbstractPage {
    public CustomPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='add']")
    private WebElement addButton;
    @FindBy(xpath = "//*[@id='add_menu']//li[1]/a")
    private WebElement newClass;
    @FindBy(css= "#add_menu ul.dropdown-menu li:nth-of-type(2)")
    private WebElement newRecord;
    @FindBy (xpath = "//*[@id='class_name']")
    private WebElement selectClass;
    @FindBy (xpath = "//*[@id='custom']/div[1]//li[2]/a")
    private WebElement exportTab;
    @FindBy (xpath = "//*[@id='custom']/div[1]//li[3]/a")
    private WebElement importTab;
    // find checkbox for last record
    @FindBy (css = "tbody tr:last-child td:first-child input")
    private WebElement lastRecordCheckbox;
    //find name for last record
    @FindBy(css = "tbody tr:last-child td:nth-child(5)")
    private WebElement recordName;
    // last  record id
    @FindBy(css = "tbody tr:last-child td:nth-child(2) a" )
    private WebElement lastRecord;
    @FindBy(css = "a.remove")
    private WebElement removeSelectedRecord;
    @FindBy (xpath = "//*[@id='custom_table_length']/label/select")
    private WebElement recordsNumber;

    public void addClass() {
        addButton.click();
        newClass.click();
    }
    public void addRecord(){
        addButton.click();
        newRecord.click();
    }
    public String getRecordName(){
        return  recordName.getText();
    }
    public void clickLastRecord(){
        lastRecord.click();
    }
    public void clickExport(){
        exportTab.click();
    }
    public void clickImport(){
        importTab.click();
    }

    public void classCreated() {
        List<WebElement> listOfClass = driver.findElements(By.cssSelector("#class_name option"));
        Boolean classIsExist = false;
        for (WebElement element : listOfClass) {
            String app = element.getText();
            if (app.equals(Consts.CLASS_NAME)) {
                classIsExist = true;
                System.out.println("Class Movie is exist");
            }
        }
    }
    public int countRecords(){
        List<WebElement> listOfRecords= driver.findElements(By.cssSelector("tbody td:nth-child(2)"));
        return listOfRecords.size();
    }
    public void removeRecord(){
        lastRecordCheckbox.click();
        removeSelectedRecord.click();
        driver.switchTo().alert().accept();
    }
    public void chooseClass(String currentClass){
        Select select = new Select(selectClass);
        select.selectByVisibleText(currentClass);
    }
    public void choose100(String records){
        Select select = new Select(recordsNumber);
        select.selectByValue(records);
    }
}