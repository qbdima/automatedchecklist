package com.adminPanelPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.NoSuchElementException;

public class LocationPage extends AbstractPage {
    public LocationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "tbody tr:first-child td:nth-child(5) span")
    private WebElement statusMessage;


    public String locationIsCreated() {
        String getStatusMessage = "";
        Boolean location = null;
        try {
            driver.findElement(By.cssSelector("tbody tr:first-child td:nth-child(5) span"));
            location = true;
        } catch (NoSuchElementException e) {
            location = false;
        }
        if (location = true) {
            getStatusMessage= statusMessage.getText();
        }
        return getStatusMessage;
    }
}