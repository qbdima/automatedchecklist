package com.adminPanelPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ImportPage extends AbstractPage {
    public ImportPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(id = "class_name")
    private WebElement chooseClass;
    @FindBy (xpath = "//*[@id='file']")
    private WebElement browseButton;
    @FindBy (xpath = "//*[@id='upload_submit']")
    private WebElement clickImport;
    @FindBy (xpath = "//*[@id='import-table']/tbody/tr/td[3]")
    private WebElement className;



    public void selectClass(String className){
        Select select = new Select(chooseClass);
        select.selectByVisibleText(className);
    }
    public void enterPath(String path){
        browseButton.sendKeys(path);
    }

    public void importFile(String chooseFile, String filePath){
        selectClass(chooseFile);
        enterPath(filePath);
        clickImport.click();
    }

    public String getClassName(){
        return className.getText();
    }
}