package com.adminPanelPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class ContentPage extends AbstractPage {
    public ContentPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='blob_item']")
    private WebElement browseButton;
    @FindBy (xpath = "//*[@id='upload_submit']")
    private WebElement uploadButton;
    @FindBy (xpath = "//*[@id='uploading-result']")
    private WebElement successMessage;
    @FindBy(xpath = "//*[@id='blobs_list_wrapper']//div[3]/a")
    private WebElement removeSelected;
    // ищем чекбокс для последней записи
    @FindBy (css = "#blobs_list tbody tr:last-child td:first-child input")
    private WebElement lastContent;
    @FindBy(xpath = "//*[@id='blobs_list_length']/label/select")
    private WebElement recordsNumber;

    public void choose100(String records){
        Select select = new Select(recordsNumber);
        select.selectByValue(records);
    }

    public void enterPath(String path) {
        browseButton.sendKeys(path);
    }
    public void  uploadFile (String path){
        enterPath(path);
        uploadButton.click();
    }
    public String getSuccessMessage(){return successMessage.getText();}
    //удаляем последний контент в таблице
    public void removeContent(){
        lastContent.click();
        removeSelected.click();
        driver.switchTo().alert().accept();
    }

    public int countContent(){
        List<WebElement> listOfContent = driver.findElements(By.cssSelector("tbody td:nth-child(2)"));
        return listOfContent.size();
    }
}
