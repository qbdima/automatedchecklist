
package com.createConfig;


import com.adminPanelPages.*;
import com.adminPanelTests.TestBase;
import com.pagesForConfig.*;
import com.pagesForConfig.AbstractPage;
import com.utils.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class MakeConfigTest{

    static GlobalParams params = GlobalParams.INSTANCE;

    //WebDriver driver = new FirefoxDriver();

    private WebDriver driver;
    private EventFiringWebDriver e_driver;
    private WebEventListener eventListener;


    Login login;
    HomePage home;
    AppCredentials credentials;
    AccountCredentials accCredendials;
    Users users;
    UploadCertificate certificate;
    AlertSettings alert;
    AddClass customPage;
    ChatPage chat;
    UtilsMethods utils;


@BeforeClass
public void before(){

    driver = new FirefoxDriver();

    e_driver = new EventFiringWebDriver(driver);

    eventListener = new WebEventListener();

    e_driver.register(eventListener);

}
    @BeforeMethod
    public void setUp(){

        e_driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        login = PageFactory.initElements(driver, Login.class);
        home = PageFactory.initElements(driver, HomePage.class);
        credentials = PageFactory.initElements(driver,AppCredentials.class);
        accCredendials = PageFactory.initElements(driver,AccountCredentials.class);
        users = PageFactory.initElements(driver, Users.class);
        certificate = PageFactory.initElements(driver,UploadCertificate.class);
        alert = PageFactory.initElements(driver,AlertSettings.class);
        customPage = PageFactory.initElements(driver,AddClass.class);
        chat = PageFactory.initElements(driver, ChatPage.class);
        utils = new UtilsMethods();
    }

    @Test (priority = 1, alwaysRun = true, groups = {"config"})
    public void checkLoginCredentials(){
        e_driver.get(Consts.ADMIN_PANEL);
        login.sleep();
        try {
            login.checkLoginCredentials();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
       Assert.assertTrue(home.getTitle().contains("Application"));
    }

    @Test(priority = 2, alwaysRun = true, groups = {"config"})
    public void getAppCredentials(){
        home.checkingApps();
        credentials.setID();
        params.put("app_id", credentials.getID());
        params.put("auth_key", credentials.getAuthKey());
        params.put("auth_secret", credentials.getAuthSecret());
        Assert.assertNotNull(params.get("app_id"));
        Assert.assertNotNull(params.get("auth_key"));
        Assert.assertNotNull(params.get("auth_secret"));
    }
    @Test(priority = 3, alwaysRun = true, groups = {"config"})
    public void checkSettingTab(){
        credentials.checkSettingsTab();
       // Assert.assertTrue();
    }
    @Test(priority = 4, alwaysRun = true, groups = {"config"})
    public void getAccCredentials(){
        String account = Consts.ADMIN_PANEL +"account/";
        e_driver.get(account);
        accCredendials.clickSettingsTab();
        params.put("api_domain", accCredendials.getApiDomain());
        params.put("chat_domain",accCredendials.getChatDomain());
        params.put("bucket_name",accCredendials.getBucketName());
        params.put("clientName", accCredendials.getClientName());
        Assert.assertNotNull(params.get("api_domain"));
        Assert.assertNotNull(params.get("chat_domain"));
        Assert.assertNotNull(params.get("bucket_name"));
    }



   @Test(priority = 5, alwaysRun = true, groups = {"config"})
        public void createUsers(){
        String  appId = (String)params.get("app_id");
        String usersUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/users";
        e_driver.get(usersUrl);
        users.user1IsCreated(Consts.USER1_LOGIN,Consts.USER1_EMAIL, Consts.USER1_PASSWORD, Consts.USER1_PASSWORD);
        users.sleep();
        params.put("test_user_id1", users.getUserID1());
        params.put("test_user_login1", users.getUserLogin1());
        params.put("test_user_password1", users.getPassword1());

       users.user2IsCreated(Consts.USER2_LOGIN, Consts.USER2_EMAIL, Consts.USER2_PASSWORD, Consts.USER2_PASSWORD);
        users.sleep();
        params.put("test_user_id2", users.getUserID2());
        params.put("test_user_login2", users.getUserLogin2());
        params.put("test_user_password2", users.getPassword2());
        params.put("dialog_id", "");
        params.put("dialog_users_ids_to_update",users.getUserID2());
        params.put("dialog_operation_to_update", "push");
        Assert.assertNotNull(params.get("test_user_id1"));
        Assert.assertNotNull(params.get("test_user_login1"));
        Assert.assertNotNull(params.get("test_user_id2"));
        Assert.assertNotNull(params.get("test_user_login1"));
   }
    @Test(priority = 6, alwaysRun = true, groups = {"config"})
    public void enterKeyCert(){
        users.clickPushNotification();
        certificate.clickSettings();
        certificate.enterGsmKey(Consts.GSM_API_KEY);

        File file = new File(Consts.CERT_FILE_PATH);
        certificate.chooseCert(file.getAbsolutePath());

        certificate.sleep();
        Assert.assertTrue(certificate.getSuccessMessage().contains("Success"));
    }
    @Test(priority = 7, alwaysRun = true, groups = {"config"})
    public void alertSettings(){
        String  appId = (String)params.get("app_id");
        chat.openAlertTab(appId);
        alert.saveTemplate();
        alert.sleep();
        Assert.assertTrue(alert.getSuccessMessage().contains("Success"));
    }
   @Test(priority = 8, alwaysRun = true, groups = {"config"})
    public void checkClass(){
        String  appId = (String)params.get("app_id");
        String customUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/custom";
       e_driver.get(customUrl);
       customPage.classIsCreated();
        customPage.sleep();
        Assert.assertEquals(customPage.checkClass(Consts.CLASS_NAME), true);
    }
    @Test(priority = 9, alwaysRun = true, groups = {"config"})
    public static void print(){
       UtilsMethods.printConfig();
    }





    @AfterClass
    public void finishBrowser(){
        e_driver.close();
        e_driver.quit();
    }
}


