package com.createConfig;

import com.adminPanelPages.ChatPage;
//import com.utils.CopyResource;
import com.pagesForConfig.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import com.utils.Consts;
import com.utils.GlobalParams;


import java.util.concurrent.TimeUnit;

public class MakeConfig {

    //static HashMap<String, String> params;
    static GlobalParams params = GlobalParams.INSTANCE;

    public static void main(String[] args) {
        WebDriver driver = new FirefoxDriver();
        Login loginAdmin = PageFactory.initElements(driver, Login.class);
        HomePage home = PageFactory.initElements(driver,HomePage.class);
        AppCredentials credentials = PageFactory.initElements(driver,AppCredentials.class);
        AccountCredentials accCredendials = PageFactory.initElements(driver,AccountCredentials.class);
        UploadCertificate certificate = PageFactory.initElements(driver,UploadCertificate.class);
        AddClass customPage = PageFactory.initElements(driver,AddClass.class);
        Users users = PageFactory.initElements(driver,Users.class);
        AlertSettings alert = PageFactory.initElements(driver,AlertSettings.class);
        ChatPage chat = PageFactory.initElements(driver, ChatPage.class);
        OverviewSettings overviewSet = PageFactory.initElements(driver, OverviewSettings.class);

        //params = new HashMap<String , String >();

        //Login admin panel
       driver.get(Consts.ADMIN_PANEL);
        //loginAdmin.loginAdmin(Consts.ADMIN_LOGIN, Consts.ADMIN_PASSWORD);

        //проверяем есть ли созданный аккаунт, если есть - логинимся, если нету, то создается аккаунт и потом логинимся
        try {
            loginAdmin.checkLoginCredentials();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //  get app credentials
        home.checkingApps();
        credentials.setID();
        params.put("app_id", credentials.getID());
        params.put("auth_key", credentials.getAuthKey());
        params.put("auth_secret", credentials.getAuthSecret());

        // проверяем есть ли на странице таб Settings, если есть, отмечаем чекбокс Allow to retrieve a list of users
        credentials.checkSettingsTab();

        // get account credentials
        String account = Consts.ADMIN_PANEL +"account/";
        driver.get(account);
        accCredendials.clickSettingsTab();
        params.put("api_domain", accCredendials.getApiDomain());
        params.put("chat_domain",accCredendials.getChatDomain());
        params.put("bucket_name",accCredendials.getBucketName());
        //accCredendials.getClientName();
        params.put("clientName", accCredendials.getClientName());

        // open user page
        String  appId = credentials.getAppID();
        String usersUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/users";
        driver.get(usersUrl);

        //check users and save credentials
        users.user1IsCreated(Consts.USER1_LOGIN,Consts.USER1_EMAIL, Consts.USER1_PASSWORD, Consts.USER1_PASSWORD);
        users.sleep();
        params.put("test_user_id1", users.getUserID1());
        params.put("test_user_login1", users.getUserLogin1());
        params.put("test_user_password1", users.getPassword1());

        users.user2IsCreated(Consts.USER2_LOGIN, Consts.USER2_EMAIL, Consts.USER2_PASSWORD, Consts.USER2_PASSWORD);
        users.sleep();
        params.put("test_user_id2", users.getUserID2());
        params.put("test_user_login2", users.getUserLogin2());
        params.put("test_user_password2", users.getPassword2());
        params.put("dialog_id", "");
        params.put("dialog_users_ids_to_update",users.getUserID2());
        params.put("dialog_operation_to_update", "push");

        //open push notification page
        users.clickPushNotification();
        certificate.clickSettings();
        certificate.enterGsmKey(Consts.GSM_API_KEY);

        String filePath = "./src/test/resources/Certificates_SuperSample_APNS_dev2.p12";
        certificate.chooseCert(filePath);

//        CopyResource copyResource = new CopyResource();
//        copyResource.copy("Certificates_SuperSample_APNS_dev2.p12");
//        certificate.chooseCert(copyResource.getResourcePath());
        //certificate.chooseCert(Consts.CERT_PATH);

        // open alert settings
        chat.openAlertTab(appId);
        alert.saveTemplate();
        // check class is exist or create class
        String customtUrl = Consts.ADMIN_PANEL +"apps/" + appId + "/service/custom";
        driver.get(customtUrl);
        customPage.classIsCreated();
        printConfig();

        driver.close();
        driver.quit();
    }

   // public static HashMap<String, String> getParams() {
   //     return params;
   // }

    public static void printConfig(){

        System.out.println("\"" + (String)params.get("clientName") + "\"" + ": {" );
        System.out.println("\"" + "app_id" + "\"" + ": " + (String)params.get("app_id") + ",");
        System.out.println("\"" + "auth_key" + "\"" + ": " + "\"" + (String)params.get("auth_key") + "\"" + ",");
        System.out.println("\"" + "auth_secret" + "\"" + ": " + "\"" + (String)params.get("auth_secret") + "\"" + ",");
        System.out.println("\"" + "api_domain" + "\"" + ": " + "\"" + (String)params.get("api_domain") + "\"" + ",");
        System.out.println("\"" + "chat_domain" + "\"" + ": " + "\"" + (String)params.get("chat_domain") + "\"" + ",");
        System.out.println("\"" + "bucket_name" + "\"" + ": " + "\"" + (String)params.get("bucket_name") + "\"" + ",");
        System.out.println("\"" + "test_user_id1" + "\"" + ": " + (String)params.get("test_user_id1") + ",");
        System.out.println("\"" + "test_user_login1" + "\"" + ": " + "\"" + (String)params.get("test_user_login1") + "\"" + ",");
        System.out.println("\"" + "test_user_password1" + "\"" + ": " + "\"" + (String)params.get("test_user_password1") + "\"" + ",");
        System.out.println("\"" + "test_user_id2" + "\"" + ": " + (String)params.get("test_user_id2") + ",");
        System.out.println("\"" + "test_user_login2" + "\"" + ": " + "\"" + (String)params.get("test_user_login2") + "\"" + ",");
        System.out.println("\"" + "test_user_password2" + "\"" + ": " + "\"" + (String)params.get("test_user_password2") + "\"" + ",");
        System.out.println("\"" + "dialog_id" + "\"" + ": " + "\"" + " " + "\"" + ",");
        System.out.println("\"" + "dialog_users_ids_to_update" + "\"" + ": " + "[" + (String)params.get("test_user_id2") + "]" + ",");
        System.out.println("\"" + "dialog_operation_to_update" + "\"" + ": " + "\"" + "push" + "\"");
        System.out.println("}," );

    }

}
