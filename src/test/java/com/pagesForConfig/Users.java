package com.pagesForConfig;

import com.adminPanelPages.CreateUserPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Users extends AbstractPage{
    public Users(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id='app-list']/a[7]")
    private WebElement usersTab;
    @FindBy (xpath = "//*[@id='list']//div[4]//a")
    private WebElement addUser;

    String userId1 = "";
    String login1 = "";
    String password1 = "";
    public void user1IsCreated(String userLogin, String userEmail, String userPass, String confirmPass) {
        CreateUserPage users = null;

        List<WebElement> usersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
        Boolean user1Exist = false;
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getText().equals(userLogin)) {
                user1Exist = true;
                int k = i + 1;
                String id = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                userId1 = id;
                login1 = userLogin;
                password1 = userPass;

            }
        }
        if (!user1Exist){
            addUser.click();
            users = new CreateUserPage(driver);
            users.createUser(userEmail,userLogin,userPass, confirmPass);
            usersTab.click();

            List<WebElement> newUsersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
            for (int i = 0; i <  newUsersList.size(); i++) {
                if (newUsersList.get(i).getText().equals(userLogin)) {
                    int k = i + 1;
                    String id = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                    userId1 = id;
                    login1 = userLogin;
                    password1 = userPass;
                }
            }
        }
    }
    public String getUserID1(){
        return userId1;
    }
    public String getUserLogin1(){
        return login1;
    }
    public String getPassword1(){
        return password1;
    }
    String userId2 = "";
    String login2 = "";
    String password2 = "";
    public void user2IsCreated(String userLogin, String userEmail, String userPass, String confirmPass) {
        CreateUserPage users = null;

        List<WebElement> usersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
        Boolean user2Exist = false;
        for (int i = 0; i < usersList.size(); i++) {
            if (usersList.get(i).getText().equals(userLogin)) {
                user2Exist = true;
                int k = i + 1;
                String id = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                userId2 = id;
                login2 = userLogin;
                password2 = userPass;

            }
        }
        if (!user2Exist){
            addUser.click();
            users = new CreateUserPage(driver);
            users.createUser(userEmail,userLogin,userPass, confirmPass);
            usersTab.click();

            List<WebElement> newUsersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
            for (int i = 0; i <  newUsersList.size(); i++) {
                if (newUsersList.get(i).getText().equals(userLogin)) {
                    int k = i + 1;
                    String id = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                    userId2 = id;
                    login2 = userLogin;
                    password2 = userPass;
                }
            }
        }
    }
    public String getUserID2(){
        return userId2;
    }
    public String getUserLogin2(){
        return login2;
    }
    public String getPassword2(){
        return password2;
    }


    static int  randomMax = 1000000;
    static  int randomMin = 10000;

    HashMap XMPPRecipient = new HashMap();
    HashMap XMPPMaster = new HashMap();
    HashMap XMPPSender = new HashMap();

    public static int randInt() {

        Random rand = new Random();

        int randomNum = rand.nextInt((randomMax - randomMin) + 1) + randomMin;

        return randomNum;
    }

    public void createCheckerUser(String userLogin1, String userPass1, String confirmPass1, String userLogin2, String userPass2, String confirmPass2,
                                  String userLogin3, String userPass3, String confirmPass3 ) {
        CreateUserPage users = null;

        String recipient = userLogin1 + randInt();
        String master = userLogin2 + randInt();
        String sender = userLogin3 + randInt();
            addUser.click();
            users = new CreateUserPage(driver);
            users.createUser2(recipient, userPass1, confirmPass1);
            users.createUser2(master, userPass2, confirmPass2);
            users.createUser2(sender, userPass3, confirmPass3);
            usersTab.click();

            List<WebElement> newUsersList = driver.findElements(By.cssSelector("#users_list > tbody > tr > td:nth-child(3)"));
            for (int i = 0; i < newUsersList.size(); i++) {
                if (newUsersList.get(i).getText().contains(userLogin1)) {
                    int k = i + 1;
                    String id1 = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                    XMPPRecipient.put("jid", id1);
                    XMPPRecipient.put("login", recipient);
                    XMPPRecipient.put("password", userPass1);
                }
                if (newUsersList.get(i).getText().contains(userLogin2)) {
                    int k = i + 1;
                    String id2 = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                    XMPPMaster.put("jid", id2);
                    XMPPMaster.put("login", master);
                    XMPPMaster.put("password", userPass2);
                }
                if (newUsersList.get(i).getText().contains(userLogin3)) {
                    int k = i + 1;
                    String id3 = driver.findElement(By.xpath("//table[@id='users_list']/tbody/tr[" + k + "]/td[2]/a/span")).getText();
                    XMPPSender.put("jid", id3);
                    XMPPSender.put("login", sender);
                    XMPPSender.put("password", userPass3);
                }
            }
        }

    public HashMap getXMPPRecipient(){
        return XMPPRecipient;
    }

    public HashMap getXMPPMaster(){
        return XMPPMaster;
    }

    public HashMap getXMPPSender(){
        return XMPPSender;
    }

}


