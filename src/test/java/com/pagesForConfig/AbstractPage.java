package com.pagesForConfig;


import com.utils.WebEventListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;



public abstract class AbstractPage {
    protected WebDriver driver;

    public AbstractPage(WebDriver driver) {

        EventFiringWebDriver e_driver = new EventFiringWebDriver(driver);
        WebEventListener eventListener = new WebEventListener();
        e_driver.register(eventListener);
        this.driver = e_driver;
    }
    // find chat tab
    @FindBy(xpath = "//*[@id='app-list']/a[2]")
    private WebElement chatPage;
    //find content tab
    @FindBy (xpath = "//*[@id='app-list']/a[3]")
    private WebElement content;
    // find custom tab
    @FindBy (xpath = "//*[@id='app-list']/a[4]")
    private WebElement custom;
    // find location tab
    @FindBy (xpath = "//*[@id='app-list']/a[5]")
    private WebElement location;
    // find messages tab
    @FindBy (xpath = "//*[@id='app-list']/a[6]")
    private WebElement push;
    // find users tab
    @FindBy (xpath = "//*[@id='app-list']/a[7]")
    private WebElement users;

    //кликаем по табам
    public void clickChat(){
        chatPage.click();
    }
    public void clickContent(){
        content.click();
    }
    public void clickCustom(){
        custom.click();
    }
    public void clickLocation(){
        location.click();
    }
    public void clickPushNotification(){
        push.click();
    }
    public void clickUsers(){
        users.click();
    }


    protected WebDriver getDriver() {
        return driver;
    }
    public void sleep() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void refreshPage() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().refresh();
    }
    public void back(){
        driver.navigate().back();
    }


}
