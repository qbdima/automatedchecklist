package com.pagesForConfig;

import com.adminPanelPages.AddNewAppPage;
//import com.utils.CopyResource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.utils.Consts;

import java.io.File;
import java.util.List;

public class HomePage extends AbstractPage{

    public HomePage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(className = "panel-title")
    private WebElement homePageTitle;
    @FindBy(css = "img.app-icon.new-app")
    private WebElement newApp;

    @FindBy(xpath = "//*[@id='login_dropdown']/img")
    private WebElement accountIcon;

    @FindBy (css = "li.app")
    private WebElement apps;

    public void   checkingApps() {

        List<WebElement> listOfApp = driver.findElements(By.cssSelector("li.app a"));
        AppCredentials credentials = null;
        AddNewAppPage appNew = null;

        Boolean isExist = false;
        for (WebElement element : listOfApp) {
            String app = element.getText();

            if (app.equals(Consts.APP_NAME)) {
                element.click();
                isExist = true;
                credentials = new AppCredentials(driver);
                return;
            }
        }
        if (!isExist) {
            for (WebElement element : listOfApp) {
                String app = element.getText();

                if (app.equals("New app")) {
                    element.click();
                    appNew = new AddNewAppPage(driver);
             //       String filePath = "./src/test/resources/dog.jpeg";
                    File file = new File(Consts.IMAGE_PATH);
                    appNew.addNewApp(file.getAbsolutePath(), Consts.APP_NAME, Consts.APP_TYPE);
//                    CopyResource copyResource = new CopyResource();
//                    copyResource.copy("dog.jpeg");
//                    appNew.addNewApp(copyResource.getResourcePath(), Consts.APP_NAME, Consts.APP_TYPE);
                }
            }
        }
    }

    public void   checkStatusChecker() {

        List<WebElement> listOfApp = driver.findElements(By.cssSelector("li.app a"));
        AppCredentials credentials = null;
        AddNewAppPage appNew = null;

        Boolean isExist = false;
        for (WebElement element : listOfApp) {
            String app = element.getText();

            if (app.equals(Consts.STATUSCHECKER_APP)) {
                element.click();
                isExist = true;
                credentials = new AppCredentials(driver);
                return;
            }
        }
        if (!isExist) {
            for (WebElement element : listOfApp) {
                String app = element.getText();

                if (app.equals("New app")) {
                    element.click();
                    appNew = new AddNewAppPage(driver);

                    appNew.addNewApp("", Consts.STATUSCHECKER_APP, Consts.APP_TYPE);
                }
            }
        }
    }



    public void   addApp() {
        List<WebElement> listOfApp = driver.findElements(By.cssSelector("li.app a"));

        for (WebElement element : listOfApp) {
            String app = element.getText();

            if (app.equals("New app")) {
                element.click();
                return;
            }
        }
    }

    public void   chooseApp() {
        List<WebElement> listOfApp = driver.findElements(By.cssSelector("li.app a"));

        for (WebElement element : listOfApp) {
            String app = element.getText();

            if (app.equals(Consts.APP_NAME)) {
                element.click();
                return;
            }
        }
    }
    public String getTitle() {
        return homePageTitle.getText();
    }
}
