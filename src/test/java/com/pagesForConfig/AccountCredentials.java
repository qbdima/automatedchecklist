package com.pagesForConfig;


import com.utils.GlobalParams;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccountCredentials extends AbstractPage {

    public AccountCredentials(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);

    }

    static GlobalParams params = GlobalParams.INSTANCE;

    @FindBy (xpath = "//*[@id='workspace']/div[1]/div/ul/li[2]/a") // реархитектура
    private WebElement settingTabRearch;

    @FindBy(xpath = "//*[@id='workspace']/ul/li[2]/a")
    private WebElement settingsTab;

    @FindBy(id = "qb_api")
    private WebElement qbApi;
    @FindBy(id = "chat_api")
    private WebElement qbChat;
    @FindBy(id = "s3_bucket")
    private WebElement bucketName;

    public String getApiDomain() {
        return qbApi.getAttribute("value");
    }

    public String getChatDomain() {
        return qbChat.getAttribute("value");
    }

    public String getBucketName() {
        return bucketName.getAttribute("value");
    }

    public void clickSettingsTab() {
        try {
            settingsTab.isDisplayed();
        } catch (Exception e) {
            settingsTab = settingTabRearch;
        }
        settingsTab.click();
    }

    public String getClientName() {
        String clientName = null;

        Pattern p = Pattern.compile("(?<=https:\\/\\/api)\\w*");
        Matcher m = p.matcher(params.get("api_domain")
                .toString());

        if (m.find()) {
            clientName = m.group(0);
        }
        return clientName;
    }
}


