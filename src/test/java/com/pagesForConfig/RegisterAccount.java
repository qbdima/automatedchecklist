package com.pagesForConfig;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RegisterAccount extends AbstractPage {
    public  RegisterAccount (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "user_full_name")
    private WebElement fullName;
    @FindBy(id = "user_email")
    private WebElement userEmail;
    @FindBy(id = "user_login")
    private WebElement userLogin;
    @FindBy(id = "user_password")
    private WebElement userPassword;
    @FindBy(id = "user_password_confirmation")
    private WebElement confirmPass;
    @FindBy(id = "user_registration_code")
    private WebElement regCode;
    @FindBy(xpath = "//*[@id='signup_terms']")
    private WebElement accept;
    @FindBy(xpath = "//*[@id='signup_submit']")
    private WebElement signupButton;


    public void registerAcc(String name, String email,String login, String pass, String confirmPassword, String code){
        fullName.sendKeys(name);
        userEmail.sendKeys(email);
        userLogin.sendKeys(login);
        userPassword.sendKeys(pass);
        confirmPass.sendKeys(confirmPassword);
        regCode.sendKeys(code);
        accept.click();
        signupButton.click();
    }
}
