package com.pagesForConfig;

import com.google.gson.JsonObject;
import com.utils.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import javax.mail.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Login extends AbstractPage {
    static GlobalParams params1 = GlobalParams.INSTANCE;

    public static final String confirmGegistration = "registration confirmation";
    public static final String confirmSES = "Amazon SES Address Verification Request";

    public static final String awsRegexp = "https:\\/\\/[^\\s\\n]*";
    public static final String qbConfirmRegexp = "https?://[a-zA-Z_0-9\\.\\-]*/users/email/[a-zA-Z_0-9]*";

    public Login (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//*[@id='login-block']/div/div/div/p/a")
    private WebElement registration;
    @FindBy(id = "user_login")
    private WebElement userLogin;
    @FindBy(id = "user_password")
    private WebElement userPassword;
    @FindBy (xpath = "//*[@id='user_remember_me']")
    private WebElement checkbox;
    @FindBy(id = "signin_submit")
    private WebElement signinButton;
    @FindBy (css = "#login-block .alert")
    private WebElement errorMessage;



    public void enterLogin(String login) {
        userLogin.sendKeys(login);
    }

    public void enterPassword(String password) {
        userPassword.sendKeys(password);
    }
    public void clickCheckbox(){checkbox.click();}

    public void clickLoginButton() {
        signinButton.click();
    }

    public String getErrorMessage(){
        return errorMessage.getText();
    }

    public void loginAdmin(String login, String password) {
        enterLogin(login);
        enterPassword(password);
        clickCheckbox();
        clickLoginButton();
    }
//    public String getHashPass(){
//        String adminResource = null;
//
//        Pattern z = Pattern.compile("(?<=https?:\\/\\/).*[^\\/]");
//        Matcher x = z.matcher(Consts.ADMIN_PANEL);
//
//        if (x.find()) {
//            adminResource = x.group(0);
//        }
//        return adminResource;
//
//    }
    public boolean isAdminExists(String login, String password){
        HomePage home = new HomePage(driver);
        boolean isExist = false;
        loginAdmin(login, password);
        sleep();
        if (home.getTitle().contains("Application")){
            isExist = true;
            params1.put("admin_login", login);
            params1.put("admin_password", password);
        }
        return isExist;
    }

    public void checkLoginCredentials() throws Exception {

//        String passHash;
//        RegisterAccount register = new RegisterAccount(driver);
//        String pas = getHashPass();
//        if(pas != null){
//            passHash = DigestUtils.sha1Hex(pas);
//            System.out.println(passHash);
//        }
//        else throw new Exception ("pass did not generated");

        RegisterAccount register = new RegisterAccount(driver);

        String login = ConfigParser.getQBLogin();
        String password = ConfigParser.getQBPassword();

        if(!isAdminExists(login, password)){
            if(!isAdminExists(login, "testtest")){
                String email_for_registration;
                String password_for_email;

                JsonObject credentials = ConfigParser.getCredentials();

                if(credentials != null){
                    try {
                        email_for_registration =  credentials.get("email_for_registration").getAsString();
                        password_for_email = credentials.get("password_for_email").getAsString();
                    }catch (NullPointerException e){
                        throw new Exception ("Can not get email or password");
                    }
                } else throw new Exception ("Can not get email or password");

                sleep();
                confirmSesEmail(email_for_registration, password_for_email);
                registration.click();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                register.registerAcc("test123", email_for_registration, login,password ,password , Consts.REGISTRATION_CODE);

                try{
                    confirmEmail(email_for_registration, password_for_email);
                }catch (Exception e){
                    e.printStackTrace();
                    System.exit(1);
                }

            }else {
                return;
            }
        }else {
            return;
        }
        driver.get(Consts.ADMIN_PANEL);
        if(!isAdminExists(login, password)){
            throw new Exception ("Account did not created");
        }

    }

    public void confirmEmail(String adminEmail, String adminEmailPassword) throws Exception {

        Mailbox.connect(adminEmail, adminEmailPassword);
        Mailbox.setFolder("INBOX");

//        Message messageFromAmazon = Mailbox.getMessageByDateAndSubject(confirmSES); //ищет не прочитанные сообщения с указанной темой, с момента времени текущий день - 1
//
//        if(messageFromAmazon != null){
//            System.out.println(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromAmazon), awsRegexp));
//            if(!Mailbox.followTheLink(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromAmazon), awsRegexp))){
//                System.out.println("Email not confirmed!");
//            }
//        }else {
//            System.out.println("SES verification message not found");
//        }

        Message messageFromQB = Mailbox.getMessageByDateAndSubject(confirmGegistration, false); //ищет не прочитанные сообщения с указанной темой, с момента времени текущий день - 1

        if(messageFromQB != null){
            System.out.println(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromQB), qbConfirmRegexp));
            if(!Mailbox.followTheLink(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromQB), qbConfirmRegexp))){
                throw new Exception("Email not confirmed!");
            }
        }else {
            throw new Exception("QB registration confirmation message not found");
        }

    }

    public void confirmSesEmail(String adminEmail, String adminEmailPassword) throws Exception {

        Mailbox.connect(adminEmail, adminEmailPassword);
        Mailbox.setFolder("INBOX");

        Message messageFromAmazon = Mailbox.getMessageByDateAndSubject(confirmSES, true); //ищет не прочитанные сообщения с указанной темой, с момента времени текущий день - 1

        if(messageFromAmazon != null){
            System.out.println(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromAmazon), awsRegexp));
            if(!Mailbox.followTheLink(Mailbox.getUrlByRegexp(Mailbox.readMessage(messageFromAmazon), awsRegexp))){
                System.out.println("Email not confirmed!");
            }
        }else {
            System.out.println("SES verification message not found");
        }

    }

}
