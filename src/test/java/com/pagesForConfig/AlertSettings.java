package com.pagesForConfig;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AlertSettings extends AbstractPage{
    public AlertSettings (WebDriver driver) {
        super (driver);
        PageFactory.initElements(driver, this);
    }

    // ищем второй радиобатон в пункте templates
    @FindBy(id="chat_alert_type_template_2")
    private WebElement template2;

    //ищем второй радиобатон в пункте badge counter
    @FindBy (id="chat_alert_badge_2")
    private WebElement badge2;

    @FindBy (xpath = "//*[@id='save_btn_chat_alert']/input")
    private WebElement saveButton;
    @FindBy (xpath = "//*[@id='update-result']")
    private WebElement successMessage;

    public String getSuccessMessage(){
        return successMessage.getText();
    }

    public void saveTemplate(){
        template2.click();
        badge2.click();
        saveButton.click();
    }
}
