package com.pagesForConfig;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import com.utils.Consts;

import java.util.List;

public class AddClass  extends AbstractPage {
    public AddClass(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id='add']")
    private WebElement addClass;
    @FindBy(xpath = "//*[@id='add_menu']/ul/li[1]/a")
    private WebElement newClass;
    @FindBy(xpath = "//*[@id='class_name']")
    private WebElement selectClass;

    @FindBy(xpath = "//*[@id='custom_class_name']")
    private WebElement className;
    @FindBy(xpath = "//*[@id='custom_field__name']")
    private WebElement fieldName1;
    @FindBy(xpath = "(//input[@id='custom_field__name'])[2]")
    private WebElement fieldName2;
    @FindBy(xpath = "(//input[@id='custom_field__name'])[3]")
    private WebElement fieldName3;
    @FindBy(xpath = "//*[@id='custom_field__type']")
    private WebElement fieldType1;
    @FindBy(xpath = "(//*[@id='custom_field__type'])[2]")
    private WebElement fieldType2;
    @FindBy(xpath = "(//*[@id='custom_field__type'])[3]")
    private WebElement fieldType3;
    @FindBy(xpath = "//*[@id='add_button'] ")
    private WebElement addField;

    @FindBy(xpath = "//*[@id='new_custom_class_dialog']//div[3]/input")
    private WebElement createClass;


    public void clickAddClass() {
        addClass.click();
    }

    public void clickNewClass() {
        newClass.click();
    }

    public void enterClassName(String name) {
        className.sendKeys(name);
    }

    public void enterFieldName1(String name) {
        fieldName1.sendKeys(name);
    }

    public void enterFieldName2(String name) {
        fieldName2.sendKeys(name);
    }

    public void enterFieldName3(String name) {
        fieldName3.sendKeys(name);
    }

    public void chooseFieldType1(String type) {
        Select select = new Select(fieldType1);
        select.selectByVisibleText(type);
    }

    public void chooseFieldType2(String type) {
        Select select = new Select(fieldType2);
        select.selectByVisibleText(type);
    }

    public void chooseFieldType3(String type) {
        Select select = new Select(fieldType3);
        select.selectByVisibleText(type);
    }

    public void clickAddField() {
        addField.click();
    }

    public void clickCreateClass() {
        createClass.click();
    }


    public void createClass(String name, String fieldName1, String fieldName2, String fieldName3, String type1, String type2, String type3) {
        clickAddClass();
        clickNewClass();
        clickAddField();
        clickAddField();
        enterClassName(name);
        enterFieldName1(fieldName1);
        enterFieldName2(fieldName2);
        enterFieldName3(fieldName3);
        chooseFieldType1(type1);
        chooseFieldType2(type2);
        chooseFieldType3(type3);
        clickCreateClass();
    }

    public void createCheckerClass(String name, String fieldName1, String type3) {
        clickAddClass();
        clickNewClass();
        enterClassName(name);
        enterFieldName1(fieldName1);
        chooseFieldType1(type3);
        clickCreateClass();
    }

    public void classIsCreated() {
        Boolean classCreated = null;
        try {
            driver.findElement(By.xpath("//*[@id='class_name']"));
            classCreated = true;
        } catch (NoSuchElementException e) {
            classCreated = false;
        }
        if (!classCreated) {
            createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
            System.out.println("Class Movie was successfully created");
            return;
        }
        if (classCreated = true) {
            selectClass.click();
            List<WebElement> listOfClass = driver.findElements(By.cssSelector("#class_name option"));
            Boolean movieIsExist = false;
            for (WebElement element : listOfClass) {
                String app = element.getText();
                if (app.equals(Consts.CLASS_NAME)) {
                    movieIsExist = true;
                    System.out.println("Class Movie is exist");
                }
            }
            if (!movieIsExist) {
                createClass(Consts.CLASS_NAME, Consts.FIELD_NAME1, Consts.FIELD_NAME2, Consts.FIELD_NAME3, Consts.FIELD_TYPE1, Consts.FIELD_TYPE2, Consts.FIELD_TYPE3);
                System.out.println("Class Movie was successfully created");
            }

        }
    }

    public void statusCheckerClass() {
        Boolean classCreated = null;
        try {
            driver.findElement(By.xpath("//*[@id='class_name']"));
            classCreated = true;
        } catch (NoSuchElementException e) {
            classCreated = false;
        }
        if (!classCreated) {
            createCheckerClass(Consts.STATUSCHECKER_CLASS_NAME, Consts.STATUSCHECKER_FIELD, Consts.FIELD_TYPE3);
            System.out.println("Class StatusChecker was successfully created");
            return;
        }
        if (classCreated = true) {
            selectClass.click();
            List<WebElement> listOfClass = driver.findElements(By.cssSelector("#class_name option"));
            Boolean checkerClassIsExist = false;
            for (WebElement element : listOfClass) {
                String app = element.getText();
                if (app.equals(Consts.STATUSCHECKER_CLASS_NAME)) {
                    checkerClassIsExist = true;
                    System.out.println("Class StatusChecker is exist");
                }
            }
            if (!checkerClassIsExist) {
                createCheckerClass(Consts.STATUSCHECKER_CLASS_NAME, Consts.STATUSCHECKER_FIELD, Consts.FIELD_TYPE3);
                System.out.println("Class StatusChecker was successfully created");
            }

        }
    }


    public boolean checkClass(String className) {
        selectClass.click();
        List<WebElement> listOfClass = driver.findElements(By.cssSelector("#class_name option"));
        Boolean movieIsExist = false;
        for (WebElement element : listOfClass) {
            String app = element.getText();
            if (app.equals(className)) {
                movieIsExist = true;
            }
        }
        return movieIsExist;
    }
}