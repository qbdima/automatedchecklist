package com.pagesForConfig;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AppCredentials extends AbstractPage{
    public AppCredentials(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    String id = null;

    @FindBy(xpath = "//*[@id='app_id']")
    @CacheLookup
    private WebElement appID;
    @FindBy(id = "app_auth_key")
    private WebElement authKey;
    @FindBy(id = "app_auth_secret")
    private WebElement authSecret;

    @FindBy(xpath = "//*[@id='user']")
    private WebElement accountIcon;
    @FindBy (xpath = "//*[@id='header-menu']//li[1]/a")
    private WebElement name;

    @FindBy (xpath = "//*[@id='workspace']//div[1]/ul/li[2]/a")
    private WebElement overviewSetting;

    public void setID(){
        id = appID.getAttribute("value");
    }


    public String getAppID(){
        // return appID.getAttribute("value");
        return id;
    }

    public String getID(){
        return appID.getAttribute("value");
    }
    public String getAuthKey(){
        return authKey.getAttribute("value");
    }
    public String getAuthSecret(){
        return authSecret.getAttribute("value");
    }


    public void checkSettingsTab(){
        OverviewSettings overviewSet = null;
        Boolean tabPresent = null;
        try {
            driver.findElement(By.xpath("//*[@id='workspace']//div[1]/ul/li[2]/a"));
            tabPresent = true;
        } catch (NoSuchElementException e) {
            tabPresent = false;
        }
        if(!tabPresent){
            return;
        } else {
            overviewSetting.click();
            overviewSet = new OverviewSettings(driver);
            overviewSet.checkCheckbox();
        }
    }

}