package com.pagesForConfig;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UploadCertificate extends AbstractPage {
    public UploadCertificate (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    //@FindBy (css = ".module-navigation-wrap ul li:last-child")
    @FindBy(xpath = "//*[@id='workspace']/div[1]/div[2]/div[1]/ul/li[4]/a")
    private WebElement settings;
    @FindBy (id = "upload_cert")
    private WebElement certForm;
    @FindBy (id = "upload_submit")
    private WebElement uploadButton;

    // open GSM
    @FindBy (xpath = "//*[@id='settings']/div[3]/div[1]/a")
    private WebElement openGsm;

    @FindBy (xpath = "//*[@id='gcm_api_key']")
    private WebElement gsmKeyField;
    @FindBy (xpath = "//*[@id='google_submit']")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@id='flash']")
    private WebElement successMessage;

    public void clickSettings(){settings.click();}

    public void enterPath(String path) {
        certForm.sendKeys(path);
    }

    public void  chooseCert (String path){
        enterPath(path);
        uploadButton.click();
    }
    public void enterGsmKey(String key){
        openGsm.click();
        gsmKeyField.sendKeys(key);
        saveButton.click();
    }


    public String getSuccessMessage() {
        return successMessage.getText();
    }
}
