package com.pagesForConfig;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverviewSettings extends AbstractPage {
    public OverviewSettings (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "application_settings_users_index")
    private WebElement usersCheckbox;

    @FindBy(xpath = "//*[@id='form-add-app']/input")
    private WebElement saveButton;

    public void checkCheckbox(){
           WebElement checkbox = getDriver().findElement(By.id("application_settings_users_index"));
            if(checkbox.isSelected()){
        }else {
            usersCheckbox.click();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            saveButton.click();
        }

    }
}
